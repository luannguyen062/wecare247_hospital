/* eslint-disable @typescript-eslint/no-use-before-define */
import React, { useRef, useState } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import { Button, Card, Tag } from 'antd';
import { FormattedMessage, Link } from 'umi';
import ProTable, { ActionType, ProColumns } from '@ant-design/pro-table';
import { EditOutlined, PlusOutlined } from '@ant-design/icons';
import moment from 'moment';
import { createCarerPayment, getCarerPayments, updateCarerPayment } from '../service';
import CreateForm from '../components/CreateForm';
import UpdateForm from '../components/UpdateForm';

const PatientCarerPaymentView: React.FC<{}> = () => {
  // Columns
  const columns: ProColumns<PAYMENT.ContractPaymentItem>[] = [
    {
      title: <FormattedMessage id="pages.payments.id" defaultMessage="Payment Ref" />,
      dataIndex: 'id',
      search: false,
    },
    {
      title: <FormattedMessage id="pages.payments.contractCode" defaultMessage="Contract" />,
      dataIndex: 'contractCode',
      sorter: true,
      formItemProps: {
        name: 'search',
        rules: [
          {
            required: true,
            message: 'Vui lỏng điền mã HĐDV',
          },
        ],
      },
      render: (_, record) => (
        <Link to={`/contract-view/${record.contractCode}`}>{record.contractCode}</Link>
      ),
    },
    {
      title: <FormattedMessage id="pages.payments.type" defaultMessage="Type" />,
      dataIndex: 'type',
      sorter: true,
      filters: true,
      valueType: 'select',
      valueEnum: {
        30: { text: 'Pay for Carer', type: '30' },
      },
      render: () => {
        return (
          <Tag color="green">
            <FormattedMessage id="pages.payments.type.payForCarer" defaultMessage="Pay for Patient Carer" />
          </Tag>
        );
      },
    },
    {
      title: <FormattedMessage id="pages.payments.amount" defaultMessage="Amount" />,
      dataIndex: 'amount',
      search: false,
      valueType: 'digit'
    },
    {
      title: <FormattedMessage id="pages.payments.patientCarerName" defaultMessage="Pay To" />,
      dataIndex: 'patientCarerName',
      search: false,
    },
    {
      title: <FormattedMessage id="pages.payments.note" defaultMessage="Note" />,
      dataIndex: 'note',
      search: false,
    },
    {
      title: <FormattedMessage id="pages.payments.created" defaultMessage="Date" />,
      dataIndex: 'created',
      render: (_, record) => moment(record.created).format('DD/MM/YYYY LT A'),
      sorter: true,
      search: false,
    },
    {
      title: <FormattedMessage id="pages.payments.createdBy" defaultMessage="Employee" />,
      dataIndex: 'createdBy',
      search: false,
    },
    {
      title: 'Thao Tác',
      dataIndex: 'id',
      search: false,
      render: (_, record) => {
        return (
          <>
            <Button
              type="primary"
              key="update"
              onClick={() => onOpenUpdateForm(record)}
              shape="circle"
            >
              <EditOutlined />
            </Button>
          </>
        );
      },
    },
  ];

  const actionRef = useRef<ActionType>();
  const [paymentList, setPaymentList] = useState<PAYMENT.ContractPaymentItem[]>();
  const [pagination, setPagination] = useState<any>();
  const [createModalVisible, setCreateModalVisible] = useState<boolean>(false);
  const [updateModalVisible, setUpdateModalVisible] = useState<boolean>(false);
  const [currentPayment, setCurrentPayment] = useState<any>(null);

  const onOpenUpdateForm = (record: any) => {
    setUpdateModalVisible(true);
    setCurrentPayment(record);
  };

  const onFetchPayments = async (params: any, sorter: any, queryFilters?: any) => {
    const orderBy = Object.keys(sorter)[0];
    let desc = false;
    if (orderBy) {
      desc = sorter[Object.keys(sorter)[0]] === 'descend';
    }

    const queryParams = {
      page: params.current || 1,
      size: params.pageSize || 10,
      orderBy,
      desc,
      levelId: queryFilters?.levelId,
      preferLocationId: queryFilters?.preferLocationId,
      search: queryFilters?.search,
    };

    const response = await getCarerPayments(queryParams);
    setPaymentList(response.data.items);
    setPagination({
      current: response.data.currentPage,
      total: response.data.total,
      pageSize: response.data.size,
      defaultPageSize: 10,
    });

    return {
      data: response.data.items,
      success: response.success,
    };
  };

  const onCreateCarerPayment = async (values: any) => {
    return createCarerPayment({
      ...values,
    });
  };

  const handleCreateModalVisible = (value: boolean) => {
    setCreateModalVisible(value);
  };

  const onUpdateCarerPayment = async (values: any) => {
    return updateCarerPayment({
      ...values,
    });
  };

  const onUpdateModalVisibleChange = (value: boolean) => {
    if(value === false) {
      setUpdateModalVisible(false);
      setCurrentPayment(null);
    }
  }

  return (
    <PageContainer>
      <Card>
        <ProTable<PAYMENT.ContractPaymentItem>
          actionRef={actionRef}
          rowKey="id"
          toolbar={{
            actions: [
              <Button
                type="primary"
                key="primary"
                onClick={() => {
                  setCreateModalVisible(true);
                }}
              >
                <PlusOutlined />
                <FormattedMessage id="actions.create" defaultMessage="Create New" />
              </Button>,
            ],
          }}
          request={(params, sorter, filter) => onFetchPayments(params, sorter, filter)}
          columns={columns}
          dateFormatter="string"
          dataSource={paymentList}
          pagination={pagination}
          search={{
            labelWidth: 120,
          }}
        />
        <CreateForm
          visible={createModalVisible}
          onVisibleChange={setCreateModalVisible}
          onFinish={onCreateCarerPayment}
          actionRef={actionRef}
          handleCreateModalVisible={handleCreateModalVisible}
        />

        <UpdateForm
          visible={updateModalVisible}
          onVisibleChange={onUpdateModalVisibleChange}
          onFinish={onUpdateCarerPayment}
          actionRef={actionRef}
          currentPayment={currentPayment}
        />
      </Card>
    </PageContainer>
  );
};

export default PatientCarerPaymentView;
