import { apiConfig } from '@/configs/apis';
import request from '../../utils/request';

export async function getCarerPayments(params: any) {
  return request<any>(`${apiConfig.prefixUrl}/api/patient-carer-payments`, { params });
}

export async function getContract(id: string) {
  return request<PAYMENT.ContractPaymentListResponse>(
    `${apiConfig.prefixUrl}/api/contract-payments/${id}`,
  );
}

export async function createCarerPayment(params: PAYMENT.PaymentModel) {
  return request<any>(`${apiConfig.prefixUrl}/api/patient-carer-payments`, {
    data: params,
    method: 'POST',
  });
}

export async function updateContractPayment(id: string, params: PAYMENT.ContractPaymentModel) {
  return request<API.UpdateResponse>(`${apiConfig.prefixUrl}/api/contract-payments/${id}`, {
    data: params,
    method: 'PUT',
  });
}

export async function updateCarerPayment(params: PAYMENT.PaymentModel) {
  return request(`${apiConfig.prefixUrl}/api/patient-carer-payments/${params.id}`, {
    data: params,
    method: 'PUT',
  });
}

export async function getContractCarerSelectList(params: any) {
  return request(
    `${apiConfig.prefixUrl}/api/contracts/${params.contractCode}/patient-carer-select-list`,
  );
}
