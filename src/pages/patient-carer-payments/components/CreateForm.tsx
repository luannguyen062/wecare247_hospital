import React, { useState } from 'react';
import ProForm, {
  ModalForm,
  ProFormText,
  ProFormSelect,
  ProFormDigit,
  ProFormTextArea,
} from '@ant-design/pro-form';
import { useIntl, FormattedMessage } from 'umi';
import { message } from 'antd';
import { getContractCarerSelectList } from '../service';

export interface CarerPaymentFormProps {
  visible: boolean;
  onFinish: (values: any) => Promise<PAYMENT.ContractPaymentListResponse>;
  onVisibleChange?: (visible: boolean) => void;
  handleCreateModalVisible: (visible: boolean) => void;
  actionRef: any;
}

const CreateForm: React.FC<CarerPaymentFormProps> = ({
  visible,
  onFinish,
  onVisibleChange,
  actionRef,
  handleCreateModalVisible,
}) => {
  // Multi lang
  const intl = useIntl();
  const [patientCarers, setPatientCarers] = useState([]);

  const onValuesChange = async (values: any) => {
    if (values.contractCode) {
      const response = await getContractCarerSelectList({ contractCode: values.contractCode });
      setPatientCarers(response.data);
    }
  };

  return (
    <ModalForm
      title={intl.formatMessage({
        id: 'pages.payments.create.title',
        defaultMessage: 'Create New Payment',
      })}
      width="800px"
      visible={visible}
      onVisibleChange={onVisibleChange}
      onFinish={async (values: any) => {
        const response = await onFinish(values);
        if (response.success) {
          handleCreateModalVisible(false);
          if (actionRef.current) {
            actionRef.current.reload();
          }
          message.success(
            intl.formatMessage({
              id: 'api.success',
              defaultMessage: 'Success',
            }),
          );
        }
      }}
      onValuesChange={onValuesChange}
    >
      <ProForm.Group>
        <ProFormText
          width="md"
          label={intl.formatMessage({
            id: 'pages.payments.contractCode',
            defaultMessage: 'Contract',
          })}
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage id="errors.required" defaultMessage="This field is required" />
              ),
            },
          ]}
          name="contractCode"
        />
        <ProFormSelect
          width="md"
          name="patientCarerCode"
          label={intl.formatMessage({
            id: 'pages.payments.type',
            defaultMessage: 'Patient Carer',
          })}
          options={patientCarers}
          rules={[
            {
              required: true,
              message: intl.formatMessage({
                id: 'errors.required',
                defaultMessage: 'This field is required',
              }),
            },
          ]}
        />
        <ProFormDigit
          width="md"
          label={intl.formatMessage({
            id: 'pages.payments.amount',
            defaultMessage: 'Amount',
          })}
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage id="errors.required" defaultMessage="This field is required" />
              ),
            },
          ]}
          name="amount"
        />
        <ProFormTextArea
          width="lg"
          name="note"
          label={intl.formatMessage({
            id: 'pages.payments.note',
            defaultMessage: 'Mô tả khác',
          })}
        />
      </ProForm.Group>
    </ModalForm>
  );
};

export default CreateForm;
