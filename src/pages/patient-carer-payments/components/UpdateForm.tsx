import React, { useEffect, useState } from 'react';
import ProForm, {
  ModalForm,
  ProFormText,
  ProFormSelect,
  ProFormDigit,
  ProFormTextArea,
} from '@ant-design/pro-form';
import { useIntl, FormattedMessage } from 'umi';
import { Form, message } from 'antd';
import { getContractCarerSelectList } from '../service';

export interface UpdateFormProps {
  visible: boolean;
  onFinish: (values: any) => Promise<PAYMENT.ContractPaymentListResponse>;
  onVisibleChange: (visible: boolean) => void;
  currentPayment?: PAYMENT.ContractPaymentModel;
  actionRef: any;
}

const UpdateForm: React.FC<UpdateFormProps> = ({
  visible,
  onFinish,
  onVisibleChange,
  currentPayment,
}) => {
  // Multi lang
  const intl = useIntl();
  const [form] = Form.useForm();
  const [patientCarers, setPatientCarers] = useState([]);

  useEffect(() => {
    if (form && visible) form.resetFields();
  }, [visible]);

  useEffect(() => {
    async function fetchAsync() {
      if (currentPayment?.contractCode) {
        const response = await getContractCarerSelectList({
          contractCode: currentPayment?.contractCode,
        });
        setPatientCarers(response.data);
      } else {
        setPatientCarers([]);
      }
    }

    fetchAsync();
  }, [currentPayment]);

  return (
    <ModalForm
      form={form}
      title={intl.formatMessage({
        id: 'pages.payments.create.title',
        defaultMessage: 'Patient Carer Payment',
      })}
      width="800px"
      visible={visible}
      onVisibleChange={onVisibleChange}
      onFinish={async (values: any) => {
        const response = await onFinish({
          ...values,
          id: currentPayment?.id,
        });
        if (response.success) {
          message.success(
            intl.formatMessage({
              id: 'api.success',
              defaultMessage: 'Success',
            }),
          );

          onVisibleChange(false);
          window.location.reload();
        }
      }}
    >
      <ProForm.Group>
        <ProFormText
          width="md"
          label={intl.formatMessage({
            id: 'pages.payments.contractCode',
            defaultMessage: 'Contract',
          })}
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage id="errors.required" defaultMessage="This field is required" />
              ),
            },
          ]}
          name="contractCode"
          initialValue={currentPayment != null ? currentPayment?.contractCode : undefined}
          disabled
        />
        <ProFormSelect
          width="md"
          name="patientCarerCode"
          label={intl.formatMessage({
            id: 'pages.payments.patientCarerCode',
            defaultMessage: 'Patient Carer',
          })}
          options={patientCarers}
          rules={[
            {
              required: true,
              message: intl.formatMessage({
                id: 'errors.required',
                defaultMessage: 'This field is required',
              }),
            },
          ]}
          initialValue={currentPayment != null ? currentPayment?.patientCarerCode : undefined}
        />
        <ProFormDigit
          width="md"
          label={intl.formatMessage({
            id: 'pages.payments.amount',
            defaultMessage: 'Amount',
          })}
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage id="errors.required" defaultMessage="This field is required" />
              ),
            },
          ]}
          name="amount"
          initialValue={currentPayment != null ? currentPayment?.amount : undefined}
        />
        <ProFormTextArea
          width="lg"
          name="note"
          label={intl.formatMessage({
            id: 'pages.payments.note',
            defaultMessage: 'Note',
          })}
          initialValue={currentPayment != null ? currentPayment?.note : undefined}
        />
      </ProForm.Group>
    </ModalForm>
  );
};

export default UpdateForm;
