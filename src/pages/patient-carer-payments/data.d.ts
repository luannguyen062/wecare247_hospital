declare namespace PAYMENT {
  export interface ContractPaymentItem {
    id: string;
    contractCode: string;
    type: number;
    note: string;
    amount: number;
    created: Date;
  }

  export interface PaymentItem {
    id: string;
    contractCode: string;
    type: number;
    note: string;
    amount: number;
    created: Date;
    patientCarerId: string;
    patientCarerName: string;
  }

  export interface ContractPaymentModel {
    id: string;
    contractCode: string;
    amount: number;
    type: number;
    note: Date;
  }

  export interface PaymentModel {
    id: string;
    contractCode: string;
    patientCarerCode: string;
    amount: number;
    type: number;
    note: Date;
  }

  export interface ContractPaymentListResponse {
    success: boolean;
    error: any;
    data: API.PaginationData<ContractPaymentItem>;
  }

  export interface ContractPaymentQueryParams extends API.QueryParams {
    contractId: string;
    type: number;
  }
}
