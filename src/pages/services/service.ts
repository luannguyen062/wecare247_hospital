import { apiConfig } from '@/configs/apis';
import request from '../../utils/request';

export async function getServices(params: any) {
  return request(`${apiConfig.prefixUrl}/api/services`, { params });
}

export async function createService(params: any) {
  return request(`${apiConfig.prefixUrl}/api/services`, {
    method: 'POST',
    data: params,
  });
}

export async function updateService(params: any) {
  return request(`${apiConfig.prefixUrl}/api/services/${params.id}`, {
    method: 'PUT',
    data: params,
  });
}
