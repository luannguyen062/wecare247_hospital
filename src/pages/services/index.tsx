import { Button, message } from 'antd';
import React, { useState, useRef } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import ProTable, { ActionType, ProColumns } from '@ant-design/pro-table';
import { PlusOutlined } from '@ant-design/icons';
import { useIntl } from 'umi';
import ProForm, {
  ProFormDigit,
  ProFormSwitch,
  ProFormText,
  ProFormTextArea,
} from '@ant-design/pro-form';
import { createService, getServices } from './service';
import { ServiceItem, ServiceModel } from './data';
import CreateForm from './components/CreateForm';

const ServiceView: React.FC<{}> = () => {
  const [createModalVisible, handleModalVisible] = useState<boolean>(false);
  const [priceNegotiation, setPriceNegotiation] = useState<boolean>(false);

  const actionRef = useRef<ActionType>();
  const intl = useIntl();
  const columns: ProColumns<ServiceItem>[] = [
    {
      title: intl.formatMessage({
        id: 'pages.services.id',
        defaultMessage: 'Service Code',
      }),
      dataIndex: 'id',
      sorter: true,
      search: false,
    },
    {
      title: intl.formatMessage({
        id: 'pages.services.name',
        defaultMessage: 'Service Name',
      }),
      dataIndex: 'name',
      formItemProps: {
        name: 'search',
        rules: [
          {
            required: true,
            message: intl.formatMessage({
              id: 'errors.required',
              defaultMessage: 'This field is required',
            }),
          },
        ],
      },
    },
    {
      title: intl.formatMessage({
        id: 'pages.services.description',
        defaultMessage: 'Description',
      }),
      dataIndex: 'description',
      sorter: false,
      search: false,
    },
    {
      title: intl.formatMessage({
        id: 'pages.services.hours',
        defaultMessage: 'Hours',
      }),
      dataIndex: 'hours',
      sorter: true,
      search: false,
    },
    {
      title: intl.formatMessage({
        id: 'pages.services.price',
        defaultMessage: 'Service Price',
      }),
      dataIndex: 'price',
      sorter: true,
      valueType: 'digit',
    },
  ];

  const onFetchServices = async (params: any) => {
    const response = await getServices(params);
    return {
      data: response.data,
      success: response.success,
    };
  };

  const onCreateService = async (fields: ServiceModel) => {
    const hide = message.loading('Loading ...');
    try {
      await createService({ ...fields });
      hide();
      message.success('Success');
      return true;
    } catch (error) {
      hide();
      message.error('Fail!');
      return false;
    }
  };

  const onValuesChange = (values: any) => {
    if (values?.priceNegotiation === true) {
      setPriceNegotiation(true);
    } else {
      setPriceNegotiation(false);
    }
  };

  return (
    <PageContainer>
      <ProTable<ServiceItem>
        actionRef={actionRef}
        rowKey="id"
        search={{
          labelWidth: 120,
        }}
        toolBarRender={() => [
          <Button type="primary" onClick={() => handleModalVisible(true)}>
            <PlusOutlined />{' '}
            {intl.formatMessage({
              id: 'actions.create',
              defaultMessage: 'Create',
            })}
          </Button>,
        ]}
        request={(params) => onFetchServices(params)}
        columns={columns}
      />
      <CreateForm onCancel={() => handleModalVisible(false)} modalVisible={createModalVisible}>
        <ProForm
          onFinish={async (values: any) => {
            const success = await onCreateService(values);
            if (success) {
              handleModalVisible(false);
              if (actionRef.current) {
                actionRef.current.reload();
              }
            }
          }}
          onValuesChange={onValuesChange}
        >
          <ProForm.Group>
            <ProFormText
              width="lg"
              name="id"
              label={intl.formatMessage({
                id: 'pages.services.id',
                defaultMessage: 'ID',
              })}
              rules={[
                {
                  required: true,
                  message: intl.formatMessage({
                    id: 'errors.required',
                    defaultMessage: 'This field is required',
                  }),
                },
              ]}
            />
            <ProFormText
              width="lg"
              name="name"
              label={intl.formatMessage({
                id: 'pages.services.name',
                defaultMessage: 'Service Name',
              })}
              rules={[
                {
                  required: true,
                  message: intl.formatMessage({
                    id: 'errors.required',
                    defaultMessage: 'This field is required',
                  }),
                },
              ]}
            />
            <ProFormTextArea
              width="lg"
              name="description"
              label={intl.formatMessage({
                id: 'pages.services.description',
                defaultMessage: 'Description',
              })}
              rules={[
                {
                  required: true,
                  message: intl.formatMessage({
                    id: 'errors.required',
                    defaultMessage: 'This field is required',
                  }),
                },
              ]}
            />
          </ProForm.Group>
          <ProForm.Group>
            <ProFormDigit
              rules={[
                {
                  required: true,
                  message: intl.formatMessage({
                    id: 'errors.required',
                    defaultMessage: 'This field is required',
                  }),
                },
              ]}
              width="lg"
              name="hours"
              label={intl.formatMessage({
                id: 'pages.services.hours',
                defaultMessage: 'Service Hours',
              })}
              disabled={priceNegotiation}
              initialValue={priceNegotiation === true ? 0 : undefined}
            />
            <ProFormDigit
              rules={[
                {
                  required: true,
                  message: intl.formatMessage({
                    id: 'errors.required',
                    defaultMessage: 'This field is required',
                  }),
                },
              ]}
              width="lg"
              name="price"
              label={intl.formatMessage({
                id: 'pages.services.price',
                defaultMessage: 'Service Price',
              })}
              disabled={priceNegotiation}
              initialValue={priceNegotiation === true ? 0 : undefined}
            />
            <ProFormSwitch
              name="priceNegotiation"
              label={intl.formatMessage({
                id: 'pages.services.priceNegotiation',
                defaultMessage: 'Negotiation Service Price',
              })}
            />
          </ProForm.Group>
        </ProForm>
      </CreateForm>
    </PageContainer>
  );
};

export default ServiceView;
