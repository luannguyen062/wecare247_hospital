export interface ServiceItem {
  id: string;
  name: string;
  description: string;
  price: number;
  pricePerHour: number;
}

export interface ServiceListResponse {
  errors: any;
  success: boolean;
  data: ServiceItem[];
}

export interface ServiceModel {
  id: string;
  name: string;
  description: string;
  price?: number;
  hours?: number;
  priceNegotiation: boolean;
}
