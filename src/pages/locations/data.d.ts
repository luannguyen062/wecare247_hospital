export interface LocationsListItem {
  id: string;
  name: string;
  description: string;
  provinceId: string;
  provinceName: string;
  districtId: string;
  districtName: string;
  wardId: string;
  wardName: string;
  streetAddress: string;
}
