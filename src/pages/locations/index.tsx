import { PlusOutlined } from '@ant-design/icons';
import { Button, Card, Form, message } from 'antd';
import React, { useState, useRef, useEffect } from 'react';
import { FormattedMessage, useIntl } from 'umi';
import { PageContainer } from '@ant-design/pro-layout';
import ProTable, { ProColumns, ActionType, TableDropdown } from '@ant-design/pro-table';
import ProForm, { ProFormText, ProFormSelect, ProFormTextArea } from '@ant-design/pro-form';
import { renderFormValidations } from '@/utils/validation';
import CreateForm from './components/CreateForm';
import { LocationsListItem } from './data.d';
import { getLocations, addLocation, getProvinces, getDistricts, getWards } from './service';
import { mapToSelectList } from '../../utils/utils';
import LocationFilter from './components/LocationFilter';

const LocationView: React.FC<{}> = () => {
  const intl = useIntl();

  const [createModalVisible, handleModalVisible] = useState<boolean>(false);
  const [listProvinces, setListProvinces] = useState([]);
  const [listDistricts, setListDistricts] = useState([]);
  const [listWards, setListWards] = useState([]);
  const actionRef = useRef<ActionType>();

  useEffect(() => {
    async function fetchAsync() {
      const response = await getProvinces();
      setListProvinces(mapToSelectList(response.data));
    }

    fetchAsync();
  }, []);

  const handleAdd = async (fields: LocationsListItem) => {
    const hide = message.loading(
      intl.formatMessage({
        id: 'api.loading',
        defaultMessage: 'Loading ...',
      }),
    );
    try {
      const response = await addLocation({ ...fields });
      if (response.success) {
        hide();
        message.success(
          intl.formatMessage({
            id: 'api.success',
            defaultMessage: 'Success',
          }),
        );
      }

      return response;
    } catch (error) {
      hide();
      message.error(
        intl.formatMessage({
          id: 'api.fail',
          defaultMessage: 'Fail',
        }),
      );
      return undefined;
    }
  };

  const handleChangeProvince = async (value: string) => {
    const response = await getDistricts({ provinceId: value });
    setListDistricts(mapToSelectList(response.data));
    setListWards([]);
  };

  const handleChangeDistrict = async (value: string) => {
    const response = await getWards({ districtId: value });
    setListWards(mapToSelectList(response.data));
  };

  const columns: ProColumns<LocationsListItem>[] = [
    {
      title: intl.formatMessage({
        id: 'pages.locations.id',
        defaultMessage: 'ID',
      }),
      dataIndex: 'id',
      formItemProps: {
        name: 'search',
        label: intl.formatMessage({
          id: 'pages.locations.search',
          defaultMessage: 'Search',
        }),
        rules: [
          {
            required: true,
            message: (
              <FormattedMessage id="errors.required" defaultMessage="This field is required" />
            ),
          },
        ],
      },
    },
    {
      title: intl.formatMessage({
        id: 'pages.locations.name',
        defaultMessage: 'Name',
      }),
      dataIndex: 'name',
      search: false,
    },
    {
      title: intl.formatMessage({
        id: 'pages.locations.description',
        defaultMessage: 'Description',
      }),
      dataIndex: 'description',
      search: false,
    },
    {
      title: intl.formatMessage({
        id: 'pages.locations.streetAddress',
        defaultMessage: 'Address',
      }),
      dataIndex: 'streetAddress',
      search: false,
      render: (_, record) => `${record.streetAddress}, ${record.wardName}, ${record.districtName}, ${record.provinceName}`
    },
    // {
    //   title: '操作',
    //   valueType: 'option',
    //   render: (text, record, _, action) => [
    //     <a
    //       key="editable"
    //       onClick={() => {
    //         action.startEditable?.(record.id);
    //       }}
    //     >
    //       编辑
    //     </a>,
    //     <a href="#" target="_blank" rel="noopener noreferrer" key="view">
    //       查看
    //     </a>,
    //     <TableDropdown
    //       key="actionGroup"
    //       onSelect={(key) => console.log(key)}
    //       menus={[
    //         { key: 'copy', name: '复制' },
    //         { key: 'delete', name: '删除' },
    //       ]}
    //     />,
    //   ],
    // },
  ];

  const [form] = Form.useForm();

  return (
    <PageContainer>
      <ProTable<LocationsListItem>
        actionRef={actionRef}
        rowKey="id"
        search={{
          labelWidth: 120,
        }}
        toolBarRender={() => [
          <Button type="primary" key="primary" onClick={() => handleModalVisible(true)}>
            <PlusOutlined /> <FormattedMessage id="actions.create" defaultMessage="Create New" />
          </Button>,
        ]}
        // tableExtraRender={() => (
        //   <Card>
        //     <LocationFilter
        //       onFinish={(values: any) => console.log(values)}
        //       initialValues={{}}
        //     />
        //   </Card>
        // )}
        request={() => getLocations()}
        columns={columns}
      />
      {/* Create new location form */}
      <CreateForm onCancel={() => handleModalVisible(false)} modalVisible={createModalVisible}>
        <ProForm
          form={form}
          onFinish={async (value: any) => {
            const response = await handleAdd(value);
            if (response) {
              handleModalVisible(false);
              if (actionRef.current) {
                actionRef.current.reload();
              }
            } else {
              renderFormValidations(form, response.errors, form.getFieldsValue());
            }
          }}
        >
          <ProForm.Group>
            <ProFormText
              width="md"
              name="id"
              label={intl.formatMessage({
                id: 'pages.locations.id',
                defaultMessage: 'ID',
              })}
              rules={[
                {
                  required: true,
                  message: intl.formatMessage({
                    id: 'errors.required',
                    defaultMessage: 'This field is required',
                  }),
                },
              ]}
            />
            <ProFormText
              width="md"
              name="name"
              label={intl.formatMessage({
                id: 'pages.locations.name',
                defaultMessage: 'Name',
              })}
              rules={[
                {
                  required: true,
                  message: intl.formatMessage({
                    id: 'errors.required',
                    defaultMessage: 'This field is required',
                  }),
                },
              ]}
            />
          </ProForm.Group>
          <ProForm.Group>
            <ProFormSelect
              width="md"
              options={listProvinces}
              name="provinceId"
              label={intl.formatMessage({
                id: 'pages.locations.province',
                defaultMessage: 'Province',
              })}
              fieldProps={{
                onSelect: handleChangeProvince,
              }}
              rules={[
                {
                  required: true,
                  message: intl.formatMessage({
                    id: 'errors.required',
                    defaultMessage: 'This field is required',
                  }),
                },
              ]}
            />
            <ProFormSelect
              options={listDistricts}
              fieldProps={{
                onSelect: handleChangeDistrict,
              }}
              width="md"
              name="districtId"
              label={intl.formatMessage({
                id: 'pages.locations.district',
                defaultMessage: 'District',
              })}
              rules={[
                {
                  required: true,
                  message: intl.formatMessage({
                    id: 'errors.required',
                    defaultMessage: 'This field is required',
                  }),
                },
              ]}
            />
            <ProFormSelect
              options={listWards}
              rules={[
                {
                  required: true,
                  message: intl.formatMessage({
                    id: 'errors.required',
                    defaultMessage: 'This field is required',
                  }),
                },
              ]}
              width="md"
              name="wardId"
              label={intl.formatMessage({
                id: 'pages.locations.ward',
                defaultMessage: 'Ward',
              })}
            />
          </ProForm.Group>
          <ProForm.Group>
            <ProFormText
              rules={[
                {
                  required: true,
                  message: intl.formatMessage({
                    id: 'errors.required',
                    defaultMessage: 'This field is required',
                  }),
                },
              ]}
              width="lg"
              name="streetAddress"
              label={intl.formatMessage({
                id: 'pages.locations.streetAddress',
                defaultMessage: 'Address',
              })}
            />
          </ProForm.Group>
          <ProForm.Group>
            <ProFormTextArea
              width="lg"
              name="description"
              label={intl.formatMessage({
                id: 'pages.locations.description',
                defaultMessage: 'Description',
              })}
            />
          </ProForm.Group>
        </ProForm>
      </CreateForm>
    </PageContainer>
  );
};

export default LocationView;
