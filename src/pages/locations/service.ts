import { apiConfig } from '@/configs/apis';
import request from '../../utils/request';
import { LocationsListItem } from './data.d';

export async function getLocations() {
  return request(`${apiConfig.prefixUrl}/api/locations`);
}

export async function addLocation(params: LocationsListItem) {
  return request(`${apiConfig.prefixUrl}/api/locations`, {
    method: 'POST',
    data: params,
  });
}

export async function getProvinces() {
  return request(`${apiConfig.prefixUrl}/api/provinces`);
}

export async function getDistricts(params: any) {
  return request(`${apiConfig.prefixUrl}/api/districts`, {
    params,
  });
}

export async function getWards(params: any) {
  return request(`${apiConfig.prefixUrl}/api/wards`, {
    params,
  });
}
