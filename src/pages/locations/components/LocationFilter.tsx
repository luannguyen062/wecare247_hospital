import TableFilter from '@/components/WeCare247/TableFilter';
import ProForm, { ProFormDateRangePicker, ProFormSelect, ProFormText } from '@ant-design/pro-form';
import React from 'react';

type LocationFilterProps = {
  onFinish: any;
  initialValues: any;
};

const LocationFilter: React.FC<LocationFilterProps> = (props) => {
  return (
    <TableFilter key="location-filter" onFinish={props.onFinish} initialValues={props.initialValues}>
      <ProForm.Group>
        <ProFormSelect
          options={[
            {
              value: 'chapter',
              label: '盖章后生效',
            },
          ]}
          width="xs"
          name="useMode"
          label="Province"
          key="useMode"
        />
        <ProFormSelect
          width="xs"
          options={[
            {
              value: 'time',
              label: '履行完终止',
            },
          ]}
          name="unusedMode"
          label="District"
          key="unusedMode"
        />
      </ProForm.Group>
    </TableFilter>
  );
};

export default LocationFilter;
