import { LockTwoTone, UserOutlined } from '@ant-design/icons';
import { Alert, message } from 'antd';
import React, { useState } from 'react';
import ProForm, { ProFormText } from '@ant-design/pro-form';
import { useIntl, Link, history, FormattedMessage, SelectLang, useModel } from 'umi';
import Footer from '@/components/Footer';
import { login, LoginResponse } from '@/services/login';
import styles from './index.less';

const LoginMessage: React.FC<{
  content: string;
}> = ({ content }) => (
  <Alert
    style={{
      marginBottom: 24,
    }}
    message={content}
    type="error"
    showIcon
  />
);

const goto = () => {
  if (!history) return;
  setTimeout(() => {
    const { query } = history.location;
    const { redirect } = query as { redirect: string };
    history.push(redirect || '/');
  }, 10);
};

const Login: React.FC<{}> = () => {
  const [submitting, setSubmitting] = useState(false);
  const [userLoginState, setUserLoginState] = useState<LoginResponse>({});
  const { initialState, setInitialState } = useModel('@@initialState');

  const intl = useIntl();

  const fetchUserInfo = () => {
    const userInfo = initialState?.fetchUserInfo?.();
    if (userInfo) {
      setInitialState({
        ...initialState,
        currentUser: userInfo,
      });
    }
  };
  const handleSubmit = async (values: any) => {
    setSubmitting(true);
    try {
      const msg = await login({ ...values });
      if (msg.success === true) {
        message.success(
          intl.formatMessage({
            id: 'api.login.success',
            defaultMessage: 'Success',
          }),
        );
        // Save token
        localStorage.setItem('logged-user', msg.data?.accessToken || '');
        // await storeData('logged-user', msg.data?.accessToken, null, null);

        // await readData(
        //   'logged-user',
        //   () => {
        //     // Fetch logged info
        //     fetchUserInfo();
        //     // Go to home page
        //     goto();
        //   },
        //   null,
        // );
        setTimeout(() => {
          // Fetch logged info
          fetchUserInfo();
          // Go to home page
          goto();
        }, 1000);
      }

      setUserLoginState(msg);
    } catch (error) {
      message.error(
        intl.formatMessage({
          id: 'api.login.error',
          defaultMessage: 'Login error! ',
        }),
      );
    }
    setSubmitting(false);
  };

  const { success } = userLoginState;

  return (
    <div className={styles.container}>
      <div className={styles.lang}>{SelectLang && <SelectLang />}</div>
      <div className={styles.content}>
        <div className={styles.top}>
          <div className={styles.header}>
            <Link to="/">
              <img alt="logo" className={styles.logo} src="/logo.svg" />
              <span className={styles.title}>WeCare247</span>
            </Link>
          </div>
          <div className={styles.desc}>Vui lòng đăng nhập để sử dụng CRM</div>
        </div>

        <div className={styles.main}>
          <ProForm
            initialValues={{
              autoLogin: true,
            }}
            submitter={{
              searchConfig: {
                submitText: intl.formatMessage({
                  id: 'pages.login.submit',
                  defaultMessage: 'Login',
                }),
              },
              render: (_, dom) => dom.pop(),
              submitButtonProps: {
                loading: submitting,
                size: 'large',
                style: {
                  width: '100%',
                },
              },
            }}
            onFinish={async (values: any) => {
              handleSubmit(values);
            }}
          >
            {success === false && (
              <LoginMessage
                content={intl.formatMessage({
                  id: 'pages.login.accountLogin.errorMessage',
                  defaultMessage: 'Incorrect email/password',
                })}
              />
            )}
            <ProFormText
              name="username"
              fieldProps={{
                size: 'large',
                prefix: <UserOutlined className={styles.prefixIcon} />,
              }}
              placeholder={intl.formatMessage({
                id: 'pages.login.username.placeholder',
                defaultMessage: 'Username',
              })}
              rules={[
                {
                  required: true,
                  message: (
                    <FormattedMessage
                      id="errors.required"
                      defaultMessage="This field is required"
                    />
                  ),
                },
                {
                  pattern: /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/,
                  message: (
                    <FormattedMessage
                      id="pages.login.username.email"
                      defaultMessage="Please input valid email!"
                    />
                  ),
                },
              ]}
            />
            <ProFormText.Password
              name="password"
              fieldProps={{
                size: 'large',
                prefix: <LockTwoTone className={styles.prefixIcon} />,
              }}
              placeholder={intl.formatMessage({
                id: 'pages.login.password.placeholder',
                defaultMessage: 'Password',
              })}
              rules={[
                {
                  required: true,
                  message: (
                    <FormattedMessage
                      id="errors.required"
                      defaultMessage="This field is required"
                    />
                  ),
                },
              ]}
            />
          </ProForm>
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default Login;
