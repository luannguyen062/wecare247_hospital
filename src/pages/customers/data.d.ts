declare namespace CUSTOMER {
  export interface ResCustomerListItem {
    total: number;
    currentPage: number;
    size: number;
    pages: number;
    hasNext: boolean;
    hasPrevious: boolean;
    items: CustomerItem;
  }

  export interface CustomerItem {
    id: string;
    fullName: string;
    contactPhone: string;
    addressNo: string;
    wardId: string;
    wardName: string;
    districtId: string;
    districtName: string;
    provinceId: string;
    provinceName: string;
    street: string;
    avatar: string;
    note: string;
    gender: number;
  }

  export interface CustomerDetailModel {
    id: string;
    fullName: string;
    contactPhone: string;
    addressNo: string;
    ward: WardModel;
    district: DistrictModel;
    province: ProvinceModel;
    street: string;
    avatar: string;
    note: string;
    gender: number;
  }

  export interface ProvinceModel {
    id: string;
    name: string;
  }

  export interface DistrictModel {
    id: string;
    name: string;
    provinceId: string;
  }

  export interface WardModel {
    id: string;
    name: string;
    provinceId: string;
    districtId: string;
  }
}
