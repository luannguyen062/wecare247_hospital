import React, { useEffect, useState } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import { Card, Space, Tag, Rate } from 'antd';
import ProCard from '@ant-design/pro-card';
// import Test from '../components/Test';
import { useIntl, history, FormattedMessage, Link } from 'umi';
import ContractListDetail from '@/components/WeCare247/ContractListDetail';
import { ProColumns } from '@ant-design/pro-table';
import moment from 'moment';
import { HeartFilled } from '@ant-design/icons';
import { isMobile } from 'react-device-detect';
import { getCustomer } from '../service';
import { statusEnum, statusMap } from '../../../services/constants';
import SupportTicketListDetail from '../components/SupportTicketListDetail';
import CustomerInfo from '../components/CustomerInfo';

const statusRender = (status: any, statusValue: any) => {
  if (status === 10) {
    return <Tag color="default">{statusValue || statusMap[status]}</Tag>;
  }

  if (status === 20) {
    return <Tag color="success">{statusValue || statusMap[status]}</Tag>;
  }

  if (status === 0) {
    return <Tag color="warning">{statusValue || statusMap[status]}</Tag>;
  }

  if (status === 100) {
    return <Tag color="cyan">{statusValue || statusMap[status]}</Tag>;
  }

  return <Tag color="error">{statusValue || statusMap[status]}</Tag>;
};

const contractColumns: ProColumns<any>[] = [
  {
    title: <FormattedMessage id="pages.contracts.id" defaultMessage="Code" />,
    dataIndex: 'id',
    sorter: true,
    search: false,
    render: (_, record) => <Link to={`/contract-view/${record.id}`}>{record.id}</Link>,
  },
  {
    title: <FormattedMessage id="pages.contracts.patientName" defaultMessage="Patient Name" />,
    dataIndex: 'patientName',
    sorter: true,
    search: false,
    render: (_, record) => <strong>{record.patientName}</strong>,
  },
  {
    title: <FormattedMessage id="pages.contracts.statusValue" defaultMessage="Status" />,
    dataIndex: 'status',
    formItemProps: {
      name: 'status',
    },
    filters: true,
    onFilter: true,
    valueType: 'select',
    valueEnum: statusEnum,
    render: (_, record) => statusRender(record.status, record.statusValue),
  },
  {
    title: <FormattedMessage id="pages.contracts.contractSourceName" defaultMessage="Location" />,
    dataIndex: 'contractSourceName',
    hideInTable: true,
    search: false,
  },
  {
    title: <FormattedMessage id="pages.contracts.signingDate" defaultMessage="Signing Date" />,
    dataIndex: 'signingDate',
    search: false,
    sorter: true,
    render: (_, record) => {
      if (record.signingDate) {
        return moment(record.signingDate).format('L');
      }

      return 'N/A';
    },
  },
  {
    title: <FormattedMessage id="pages.contracts.effectFrom" defaultMessage="Effect From" />,
    dataIndex: 'effectFrom',
    valueType: 'dateTime',
    formItemProps: {
      name: 'from',
    },
    render: (_, record) => moment(record.effectFrom).format('L'),
  },
  {
    title: <FormattedMessage id="pages.contracts.effectTo" defaultMessage="Effect To" />,
    dataIndex: 'effectTo',
    valueType: 'dateTime',
    formItemProps: {
      name: 'to',
    },
    render: (_, record) => moment(record.effectTo).format('L'),
  },
];

const serviceColumns: ProColumns<any>[] = [
  {
    dataIndex: 'id',
    hideInTable: true,
  },
  {
    title: <FormattedMessage id="pages.contracts.serviceName" defaultMessage="Service Name" />,
    dataIndex: 'serviceId',
    render: (_, innerRecord) => innerRecord.service.name,
  },
  {
    title: <FormattedMessage id="pages.contracts.serviceFrom" defaultMessage="From" />,
    dataIndex: 'from',
    render: (_, innerRecord) => moment(innerRecord.from).format('L'),
  },
  {
    title: <FormattedMessage id="pages.contracts.serviceTo" defaultMessage="To" />,
    dataIndex: 'to',
    render: (_, innerRecord) => moment(innerRecord.to).format('L'),
  },
  {
    title: <FormattedMessage id="pages.contracts.patientCarer" defaultMessage="Patient Carer" />,
    dataIndex: 'patientCarerName',
    render: (_, record) => (
      <Link to={`/patient-carer-view/${record.patientCarerId}`}>
        <strong>{record.patientCarerName}</strong>
      </Link>
    ),
  },
  {
    title: <FormattedMessage id="pages.contracts.serviceHours" defaultMessage="Actual Hours" />,
    dataIndex: 'actualHours',
    valueType: 'digit',
  },
  {
    title: <FormattedMessage id="pages.contracts.servicePrice" defaultMessage="Price" />,
    dataIndex: 'totalPrice',
    valueType: 'digit',
    render: (_, record) => `${record.totalPrice} VND`,
  },
  {
    title: <FormattedMessage id="pages.contracts.serviceStatus" defaultMessage="Status" />,
    dataIndex: 'status',
    formItemProps: {
      name: 'status',
    },
    filters: true,
    onFilter: true,
    valueType: 'select',
    valueEnum: statusEnum,
    render: (_, innerRecord) => statusRender(innerRecord.status, statusMap[innerRecord.status]),
  },
  {
    title: <FormattedMessage id="pages.contracts.serviceRating" defaultMessage="Rating" />,
    dataIndex: 'rating',
    sorter: true,
    render: (_, innerRecord) => {
      if (!innerRecord.rating) {
        return (
          <FormattedMessage
            id="pages.contracts.serviceRating.NotYet"
            defaultMessage="Not Rated Yet"
          />
        );
      }

      return (
        <Rate disabled character={<HeartFilled />} allowHalf defaultValue={innerRecord.rating} />
      );
    },
  },
];

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const CustomerInformationView: React.FC<{}> = (props: any) => {
  const intl = useIntl();
  const [tab, setTab] = useState('info');
  const [customer, setCustomer] = useState<any>(null);

  useEffect(() => {
    const customerId = props.match.params.id;
    const activeTab = props.location.query.tab;
    setTab(activeTab || 'info');

    async function fetchAsync() {
      const response = await getCustomer(customerId);
      setCustomer(response.data);
    }

    fetchAsync();
  }, []);

  console.log(isMobile, 'isMobile');

  return (
    <PageContainer>
      <Card loading={!customer}>
        {customer && (
          <div>
            <Space style={{ marginBottom: 16 }} />
            <ProCard
              tabs={{
                tabPosition: 'top',
                type: 'line',
                activeKey: tab,
                onChange: (key) => {
                  history.push({
                    pathname: history.location.pathname,
                    query: {
                      tab: key,
                    },
                  });

                  setTab(key);
                },
              }}
            >
              <ProCard.TabPane
                key="info"
                tab={intl.formatMessage({
                  id: 'pages.customers.detail.title',
                  defaultMessage: 'Information',
                })}
              >
                <CustomerInfo customer={customer} />
              </ProCard.TabPane>
              <ProCard.TabPane
                key="contracts"
                tab={intl.formatMessage({
                  id: 'pages.customers.detail.contracts.title',
                  defaultMessage: 'Contracts',
                })}
              >
                <ContractListDetail
                  headerTitle={intl.formatMessage({
                    id: 'pages.customers.detail.services.title',
                    defaultMessage: 'Services',
                  })}
                  contracts={customer.contracts}
                  services={customer.contractServices}
                  dateFormatter="string"
                  displayContractSummary
                  contractColumns={contractColumns}
                  serviceColumns={serviceColumns}
                />
              </ProCard.TabPane>
              <ProCard.TabPane
                key="tickets"
                tab={intl.formatMessage({
                  id: 'pages.customers.detail.tickets.title',
                  defaultMessage: 'Support Tickets',
                })}
              >
                <SupportTicketListDetail />
              </ProCard.TabPane>
            </ProCard>
          </div>
        )}
      </Card>
    </PageContainer>
  );
};

export default CustomerInformationView;
