import { apiConfig } from '@/configs/apis';
import request from '../../utils/request';

export async function getCustomers(params?: any) {
  return request(`${apiConfig.prefixUrl}/api/customers`, { params });
}

export async function getCustomer(id: string) {
  return request(`${apiConfig.prefixUrl}/api/customers/${id}`);
}

export async function createCustomer(params: CUSTOMER.CustomerItem) {
  return request(`${apiConfig.prefixUrl}/api/customers`, {
    method: 'POST',
    data: params,
  });
}

export async function updateCustomer(params: CUSTOMER.CustomerItem) {
  return request(`${apiConfig.prefixUrl}/api/customers/${params.id}`, {
    method: 'PUT',
    data: params,
  });
}

export async function getProvinces() {
  return request(`${apiConfig.prefixUrl}/api/provinces`);
}

export async function getDistricts(params: any) {
  return request(`${apiConfig.prefixUrl}/api/districts`, {
    params,
  });
}

export async function getWards(params: any) {
  return request(`${apiConfig.prefixUrl}/api/wards`, {
    params,
  });
}

export async function getCustomerTypeahead(params?: any) {
  return request(`${apiConfig.prefixUrl}/api/typeahead/customers`, { params });
}
