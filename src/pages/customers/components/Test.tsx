import React, { useState } from 'react';
import { Calendar, Badge, Alert, Popover } from 'antd';
import '../style.css';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const Test: React.FC<{}> = (props: any) => {
  const [selectDate, setSelectDate] = useState<any>();
  const onSelect = (value: any) => {
    setSelectDate(value);
  };

  const content = () => (
    <div>
      <p>Content</p>
      <p>Content</p>
    </div>
  );

  function getListData(value: any) {
    let listData;
    switch (value.date()) {
      case 8:
        listData = [{ type: 'error', content: 'This is warning event.' }];
        break;
      case 10:
        listData = [{ type: 'error', content: 'This is warning event.' }];
        break;
      case 15:
        listData = [{ type: 'error', content: 'This is warning event' }];
        break;
      default:
    }
    return listData || [];
  }

  function dateCellRender(value: any) {
    const listData = getListData(value);
    return (
      <ul className="events">
        {listData.map((item: any) => (
          <li key={item.content}>
            {/* <Badge status={item.type} text={item.content} /> */}
            <Popover content={content} title="Title">
              <Badge status={item.type} text={item.content} />
            </Popover>
          </li>
        ))}
      </ul>
    );
  }

  function getMonthData(value: any) {
    if (value.month() === 8) {
      return 1394;
    }

    return 0;
  }

  function monthCellRender(value: any) {
    const num = getMonthData(value);
    return num ? (
      <div className="notes-month">
        <section>{num}</section>
        <span>Backlog number</span>
      </div>
    ) : null;
  }

  // return <Calendar dateCellRender={dateCellRender} monthCellRender={monthCellRender} onPanelChange={onPanelChange}/>;
  return (
    <>
      {/* <Alert message={`You selected date: ${selectDate && selectDate.format('YYYY-MM-DD')}`} /> */}
      <Calendar
        dateCellRender={dateCellRender}
        monthCellRender={monthCellRender}
        onSelect={onSelect}
        mode="month"

      />
    </>
  );
};

export default Test;
