import React from 'react';
import ProCard from '@ant-design/pro-card';
import { useIntl } from 'umi';

export interface SupportTicketListDetailProps {}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const SupportTicketListDetail: React.FC<SupportTicketListDetailProps> = (props) => {
  const intl = useIntl();
  return (
    <>
      <ProCard>
        {intl.formatMessage({
          id: 'actions.comingSoon',
          defaultMessage: 'Coming soon',
        })}
      </ProCard>
    </>
  );
};

export default SupportTicketListDetail;
