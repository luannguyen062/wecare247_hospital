import React from 'react';
import { useIntl } from 'umi';
import ProDescriptions from '@ant-design/pro-descriptions';
import ProTable, { ProColumns } from '@ant-design/pro-table';
import { Space } from 'antd';
import moment from 'moment';

export interface CustomerInfoProps {
  customer: any;
  style?: any;
}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const CustomerInfo: React.FC<CustomerInfoProps> = (props) => {
  const intl = useIntl();
  const { customer, style } = props;
  const patientColumns: ProColumns<any>[] = [
    {
      title: intl.formatMessage({
        id: 'pages.customers.patient.id',
        defaultMessage: 'Patient code',
      }),
      dataIndex: 'id',
      sorter: true,
    },
    {
      title: intl.formatMessage({
        id: 'pages.customers.patient.fullName',
        defaultMessage: 'Name',
      }),
      dataIndex: 'fullName',
      sorter: true,
    },
    {
      title: intl.formatMessage({
        id: 'pages.customers.patient.yearOfBirth',
        defaultMessage: 'Age',
      }),
      dataIndex: 'yearOfBirth',
      sorter: true,
      render: (_, record: any) => moment().year() - record.yearOfBirth,
    },
    {
      title: intl.formatMessage({
        id: 'pages.customers.patient.weight',
        defaultMessage: 'Weight',
      }),
      dataIndex: 'weight',
      sorter: true,
      render: (_, record: any) => `${record.weight} KG`,
    },
  ];

  return (
    <div style={style}>
      <ProDescriptions
        title={intl.formatMessage({
          id: 'pages.customers.title',
          defaultMessage: 'Customer Information',
        })}
      >
        <ProDescriptions.Item
          label={intl.formatMessage({
            id: 'pages.customers.fullName',
            defaultMessage: 'Full Name',
          })}
          valueType="text"
        >
          {customer.fullName}
        </ProDescriptions.Item>
        <ProDescriptions.Item
          label={intl.formatMessage({
            id: 'pages.customers.contactPhone',
            defaultMessage: 'Contact Phone',
          })}
          valueType="text"
        >
          {customer.contactPhone}
        </ProDescriptions.Item>
        <ProDescriptions.Item
          label={intl.formatMessage({
            id: 'pages.customers.secondaryPhone',
            defaultMessage: 'Secondary Phone',
          })}
          valueType="text"
        >
          {customer.secondaryPhone}
        </ProDescriptions.Item>
        <ProDescriptions.Item
          label={intl.formatMessage({
            id: 'pages.customers.gender',
            defaultMessage: 'Gender',
          })}
          valueType="text"
        >
          {customer.gender === 1
            ? intl.formatMessage({
                id: 'pages.customers.gender.male',
                defaultMessage: 'Male',
              })
            : intl.formatMessage({
                id: 'pages.customers.gender.female',
                defaultMessage: 'Female',
              })}
        </ProDescriptions.Item>
        <ProDescriptions.Item
          label={intl.formatMessage({
            id: 'pages.customers.contractAddress',
            defaultMessage: 'Contract Address',
          })}
          valueType="text"
          plain
        >
          {customer.street
            ? `${customer.ward.name}, ${customer.district.name}, ${customer.province.name}`
            : 'N/A'}
        </ProDescriptions.Item>
      </ProDescriptions>
      <Space style={{ marginBottom: 20 }} />
      <ProTable<any>
        columns={patientColumns}
        rowKey="id"
        request={() => {
          return Promise.resolve({
            data: customer.patients,
            success: true,
          });
        }}
        pagination={{
          showQuickJumper: true,
        }}
        search={false}
        options={false}
      />
    </div>
  );
};

export default CustomerInfo;
