import React, { useEffect, useRef, useState } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import { Button, message, Tag } from 'antd';
import ProTable, { ActionType, ProColumns } from '@ant-design/pro-table';
import { EditOutlined } from '@ant-design/icons';
import { Link, useIntl } from 'umi';
import ProForm, { ProFormSelect, ProFormText, ProFormTextArea } from '@ant-design/pro-form';
import { mapToSelectList } from '@/utils/utils';
import {
  getCustomers,
  getProvinces,
  getDistricts,
  getWards,
  createCustomer,
  updateCustomer,
} from '../service';
import CreateForm from '../components/CreateForm';
import UpdateForm from '../components/UpdateForm';

const CustomerView: React.FC<{}> = () => {
  const intl = useIntl();
  const [createModalVisible, handleModalVisible] = useState<boolean>(false);
  const [updateModalVisible, setUpdateModalVisible] = useState<boolean>(false);
  const [currentCustomer, setCurrentCustomer] = useState<CUSTOMER.CustomerItem>();
  const [provinceList, setProvinceList] = useState([]);
  const [districtList, setDistrictList] = useState([]);
  const [wardList, setWardList] = useState([]);

  const onOpenUpdateForm = (record: any) => {
    setCurrentCustomer(record);
    setUpdateModalVisible(true);
  };

  const columns: ProColumns<CUSTOMER.CustomerItem>[] = [
    {
      title: intl.formatMessage({
        id: 'pages.customers.id',
        defaultMessage: 'Customer ID',
      }),
      dataIndex: 'id',
      search: false,
      render: (_, record) => <Link to={`/customer-view/${record.id}`}>{record.id}</Link>,
    },
    {
      title: intl.formatMessage({
        id: 'pages.customers.fullName',
        defaultMessage: 'Full Name',
      }),
      dataIndex: 'fullName',
      formItemProps: {
        name: 'search',
      },
      render: (_, record) => <Link to={`/customer-view/${record.id}`}><strong>{record.fullName}</strong></Link>,
    },
    {
      title: intl.formatMessage({
        id: 'pages.customers.contactPhone',
        defaultMessage: 'Phone',
      }),
      dataIndex: 'contactPhone',
      formItemProps: {
        name: 'search',
      },
      copyable: true,
    },
    {
      title: intl.formatMessage({
        id: 'pages.customers.secondaryPhone',
        defaultMessage: 'Secondary Phone',
      }),
      dataIndex: 'secondaryPhone',
      formItemProps: {
        name: 'search',
      },
      copyable: true,
    },
    {
      title: intl.formatMessage({
        id: 'pages.customers.street',
        defaultMessage: 'Street Address',
      }),
      dataIndex: 'street',
      formItemProps: {
        name: 'search',
      },
      render: (_, record) => {
        return `${record.street}, ${record.wardName}, ${record.districtName}, ${record.provinceName}`;
      },
    },
    {
      title: 'avatar',
      dataIndex: 'avatar',
      valueType: 'avatar',
      hideInTable: true,
      search: false,
    },
    {
      title: intl.formatMessage({
        id: 'pages.customers.gender',
        defaultMessage: 'Gender',
      }),
      dataIndex: 'gender',
      search: false,
      render: (_, record) => {
        if (record.gender === 1) {
          return (
            <Tag color="blue">
              {intl.formatMessage({
                id: 'pages.customers.gender.male',
                defaultMessage: 'Male',
              })}
            </Tag>
          );
        }

        return (
          <Tag color="pink">
            {intl.formatMessage({
              id: 'pages.customers.gender.female',
              defaultMessage: 'Female',
            })}
          </Tag>
        );
      },
    },
    {
      title: intl.formatMessage({
        id: 'pages.customers.note',
        defaultMessage: 'Note',
      }),
      dataIndex: 'note',
      search: false,
    },
    {
      title: intl.formatMessage({
        id: 'actions.title',
        defaultMessage: 'Actions',
      }),
      dataIndex: 'actions',
      search: false,
      render: (_, record) => {
        return (
          <>
            <Button
              type="primary"
              key="update"
              onClick={() => onOpenUpdateForm(record)}
              shape="circle"
            >
              <EditOutlined />
            </Button>
          </>
        );
      },
    },
  ];

  useEffect(() => {
    async function fetchAsync() {
      const provinceResponse = await getProvinces();
      const districtResponse = await getDistricts({});
      const wardResponse = await getWards({});
      setProvinceList(mapToSelectList(provinceResponse.data));
      setDistrictList(mapToSelectList(districtResponse.data));
      setWardList(mapToSelectList(wardResponse.data));
    }
    fetchAsync();
  }, []);

  const onGetCustomers = async (params: any) => {
    const response = await getCustomers({
      page: params.currentPage,
      size: params.pageSize,
      search: params.search,
    });

    if (!response.success) {
      return {
        data: [],
        success: true,
      };
    }

    return {
      data: response.data.items,
      success: true,
    };
  };

  const onCreateCustomer = async (fields: any) => {
    const hide = message.loading(
      intl.formatMessage({
        id: 'api.loading',
        defaultMessage: 'Loading ...',
      }),
    );
    try {
      await createCustomer({ ...fields });
      hide();
      message.success(
        intl.formatMessage({
          id: 'api.success',
          defaultMessage: 'Success!',
        }),
      );
      return true;
    } catch (error) {
      hide();
      message.error(
        intl.formatMessage({
          id: 'api.fail',
          defaultMessage: 'Fail!',
        }),
      );
      return false;
    }
  };

  const onUpdateCustomer = async (fields: any) => {
    const hide = message.loading(
      intl.formatMessage({
        id: 'api.loading',
        defaultMessage: 'Loading ...',
      }),
    );
    try {
      await updateCustomer({ ...fields });
      hide();
      message.success(
        intl.formatMessage({
          id: 'api.success',
          defaultMessage: 'Success!',
        }),
      );
      return true;
    } catch (error) {
      hide();
      message.error(
        intl.formatMessage({
          id: 'api.fail',
          defaultMessage: 'Fail!',
        }),
      );
      return false;
    }
  };

  const onChangeProvince = async (value: string) => {
    const districtResponse = await getDistricts({ provinceId: value });
    setDistrictList(mapToSelectList(districtResponse.data));
    setWardList([]);
  };

  const onChangeDistrict = async (value: string) => {
    const wardResponse = await getWards({ districtId: value });
    setWardList(mapToSelectList(wardResponse.data));
  };

  const actionRef = useRef<ActionType>();

  return (
    <PageContainer>
      <ProTable<CUSTOMER.CustomerItem>
        actionRef={actionRef}
        search={{
          labelWidth: 120,
        }}
        rowKey="id"
        toolBarRender={() => [
          // <Button type="primary" key="primary" onClick={() => handleModalVisible(true)}>
          //   <PlusOutlined />
          //   {intl.formatMessage({
          //     id: 'btn.create',
          //     defaultMessage: 'Create',
          //   })}
          // </Button>,
        ]}
        request={(params) => onGetCustomers(params)}
        columns={columns}
      />
      <CreateForm onCancel={() => handleModalVisible(false)} modalVisible={createModalVisible}>
        <ProForm
          onFinish={async (values) => {
            const success = await onCreateCustomer(values);
            if (success) {
              handleModalVisible(false);
              if (actionRef.current) {
                actionRef.current.reload();
              }
            }
          }}
          title={intl.formatMessage({
            id: 'actions.create',
            defaultMessage: 'Create',
          })}
        >
          <ProForm.Group>
            <ProFormText
              width="sm"
              name="id"
              label={intl.formatMessage({
                id: 'pages.customers.id',
                defaultMessage: 'Customer ID',
              })}
              rules={[
                {
                  required: true,
                  message: intl.formatMessage({
                    id: 'errors.required',
                    defaultMessage: 'This field is required!',
                  }),
                },
              ]}
            />
            <ProFormText
              width="sm"
              name="fullName"
              label={intl.formatMessage({
                id: 'pages.customers.fullName',
                defaultMessage: 'Full Name',
              })}
              rules={[
                {
                  required: true,
                  message: 'Vui lòng nhập tên khách hàng',
                },
              ]}
            />
          </ProForm.Group>
          <ProForm.Group>
            <ProFormText
              width="sm"
              name="contactPhone"
              label={intl.formatMessage({
                id: 'pages.customers.contactPhone',
                defaultMessage: 'Contact Phone',
              })}
              rules={[
                {
                  required: true,
                  message: 'Vui lòng nhập số điện thoại',
                },
              ]}
            />
            <ProFormSelect
              width="sm"
              options={[
                {
                  value: 1,
                  label: intl.formatMessage({
                    id: 'pages.customers.gender.male',
                    defaultMessage: 'Male',
                  }),
                },
                {
                  value: 2,
                  label: intl.formatMessage({
                    id: 'pages.customers.gender.female',
                    defaultMessage: 'Female',
                  }),
                },
              ]}
              name="gender"
              label={intl.formatMessage({
                id: 'pages.customers.gender',
                defaultMessage: 'Gender',
              })}
              rules={[
                {
                  required: true,
                  message: 'Vui lòng chọn giới tính',
                },
              ]}
            />
          </ProForm.Group>
          <ProForm.Group>
            <ProFormSelect
              width="sm"
              options={provinceList}
              name="provinceId"
              label={intl.formatMessage({
                id: 'pages.customers.provinceId',
                defaultMessage: 'Province',
              })}
              fieldProps={{
                onSelect: onChangeProvince,
              }}
              rules={[
                {
                  required: true,
                  message: 'Vui lòng nhập tỉnh/thành phố',
                },
              ]}
            />
            <ProFormSelect
              options={districtList}
              fieldProps={{
                onSelect: onChangeDistrict,
              }}
              width="sm"
              name="districtId"
              label={intl.formatMessage({
                id: 'pages.customers.district',
                defaultMessage: 'District',
              })}
              rules={[
                {
                  required: true,
                  message: 'Vui lòng nhập quận/huyện',
                },
              ]}
            />
            <ProFormSelect
              options={wardList}
              rules={[
                {
                  required: true,
                  message: 'Vui lòng nhập phường/xã',
                },
              ]}
              width="sm"
              name="wardId"
              label={intl.formatMessage({
                id: 'pages.customers.ward',
                defaultMessage: 'Ward',
              })}
            />
          </ProForm.Group>
          <ProForm.Group>
            <ProFormText
              width="md"
              name="street"
              label={intl.formatMessage({
                id: 'pages.customers.street',
                defaultMessage: 'Street Address',
              })}
            />
          </ProForm.Group>
          <ProForm.Group>
            <ProFormTextArea
              width="md"
              name="note"
              label={intl.formatMessage({
                id: 'pages.customers.note',
                defaultMessage: 'Note',
              })}
            />
          </ProForm.Group>
        </ProForm>
      </CreateForm>
      <UpdateForm onCancel={() => setUpdateModalVisible(false)} modalVisible={updateModalVisible}>
        <ProForm
          onFinish={async (values) => {
            const success = await onUpdateCustomer(values);
            if (success) {
              setUpdateModalVisible(false);
              if (actionRef.current) {
                actionRef.current.reload();
              }
            }
          }}
          title={intl.formatMessage({
            id: 'actions.update',
            defaultMessage: 'Update Information',
          })}
        >
          <ProForm.Group>
            <ProFormText
              width="sm"
              name="id"
              label={intl.formatMessage({
                id: 'pages.customers.id',
                defaultMessage: 'Customer ID',
              })}
              rules={[
                {
                  required: true,
                  message: intl.formatMessage({
                    id: 'errors.required',
                    defaultMessage: 'This field is required!',
                  }),
                },
              ]}
              initialValue={currentCustomer?.id}
              disabled
            />
            <ProFormText
              width="sm"
              name="fullName"
              label={intl.formatMessage({
                id: 'pages.customers.fullName',
                defaultMessage: 'Full Name',
              })}
              rules={[
                {
                  required: true,
                  message: intl.formatMessage({
                    id: 'errors.required',
                    defaultMessage: 'This field is required!',
                  }),
                },
              ]}
              initialValue={currentCustomer?.fullName}
            />
          </ProForm.Group>
          <ProForm.Group>
            <ProFormText
              width="sm"
              name="contactPhone"
              label={intl.formatMessage({
                id: 'pages.customers.contactPhone',
                defaultMessage: 'Contact Phone',
              })}
              rules={[
                {
                  required: true,
                  message: intl.formatMessage({
                    id: 'errors.required',
                    defaultMessage: 'This field is required!',
                  }),
                },
              ]}
              initialValue={currentCustomer?.contactPhone}
            />
            <ProFormSelect
              width="sm"
              options={[
                {
                  value: 1,
                  label: intl.formatMessage({
                    id: 'pages.customers.gender.male',
                    defaultMessage: 'Male',
                  }),
                },
                {
                  value: 2,
                  label: intl.formatMessage({
                    id: 'pages.customers.gender.female',
                    defaultMessage: 'Female',
                  }),
                },
              ]}
              name="gender"
              label={intl.formatMessage({
                id: 'pages.customers.gender',
                defaultMessage: 'Gender',
              })}
              rules={[
                {
                  required: true,
                  message: intl.formatMessage({
                    id: 'errors.required',
                    defaultMessage: 'This field is required!',
                  }),
                },
              ]}
              initialValue={currentCustomer?.gender}
            />
          </ProForm.Group>
          <ProForm.Group>
            <ProFormSelect
              width="sm"
              options={provinceList}
              name="provinceId"
              label={intl.formatMessage({
                id: 'pages.customers.provinceId',
                defaultMessage: 'Province',
              })}
              fieldProps={{
                onSelect: onChangeProvince,
              }}
              rules={[
                {
                  required: true,
                  message: intl.formatMessage({
                    id: 'errors.required',
                    defaultMessage: 'This field is required!',
                  }),
                },
              ]}
              initialValue={currentCustomer?.provinceId}
            />
            <ProFormSelect
              options={districtList}
              fieldProps={{
                onSelect: onChangeDistrict,
              }}
              width="sm"
              name="districtId"
              label={intl.formatMessage({
                id: 'pages.customers.district',
                defaultMessage: 'District',
              })}
              rules={[
                {
                  required: true,
                  message: intl.formatMessage({
                    id: 'errors.required',
                    defaultMessage: 'This field is required!',
                  }),
                },
              ]}
              initialValue={currentCustomer?.districtId}
            />
            <ProFormSelect
              options={wardList}
              rules={[
                {
                  required: true,
                  message: intl.formatMessage({
                    id: 'errors.required',
                    defaultMessage: 'This field is required!',
                  }),
                },
              ]}
              width="sm"
              name="wardId"
              label={intl.formatMessage({
                id: 'pages.customers.ward',
                defaultMessage: 'Ward',
              })}
              initialValue={currentCustomer?.wardId}
            />
          </ProForm.Group>
          <ProForm.Group>
            <ProFormText
              width="md"
              name="street"
              label={intl.formatMessage({
                id: 'pages.customers.street',
                defaultMessage: 'Street Address',
              })}
              initialValue={currentCustomer?.street}
            />
          </ProForm.Group>
          <ProForm.Group>
            <ProFormTextArea
              width="md"
              name="note"
              label={intl.formatMessage({
                id: 'pages.customers.note',
                defaultMessage: 'Note',
              })}
              initialValue={currentCustomer?.note}
            />
          </ProForm.Group>
        </ProForm>
      </UpdateForm>
    </PageContainer>
  );
};

export default CustomerView;
