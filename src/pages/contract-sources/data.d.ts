export type TableListItem = {
  id: string;
  name: string;
  description: string;
};
