import { PlusOutlined } from '@ant-design/icons';
import { Button, message } from 'antd';
import React, { useState, useRef } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import type { ProColumns, ActionType } from '@ant-design/pro-table';
import ProTable from '@ant-design/pro-table';
import { useIntl } from 'umi';
import CreateForm from './components/CreateForm';
import type { FormValueType } from './components/UpdateForm';
import type { TableListItem } from './data.d';
import { getContractSources, createContractSource } from './service';

const TableList: React.FC<{}> = () => {
  const intl = useIntl();
  const [createModalVisible, handleModalVisible] = useState<boolean>(false);
  // const [updateModalVisible, handleUpdateModalVisible] = useState<boolean>(false);
  // const [stepFormValues, setStepFormValues] = useState({});
  const actionRef = useRef<ActionType>();
  // const [row, setRow] = useState<TableListItem>();
  // const [selectedRowsState, setSelectedRows] = useState<TableListItem[]>([]);
  const columns: ProColumns<TableListItem>[] = [
    {
      title: 'ID',
      dataIndex: 'id',
      search: false,
      sorter: true,
      hideInForm: true,
      hideInTable: true,
    },
    {
      title: intl.formatMessage({
        id: 'pages.contracts.sources.name',
        defaultMessage: 'Name',
      }),
      dataIndex: 'name',
      search: false,
      sorter: true,
    },
    {
      title: intl.formatMessage({
        id: 'pages.contracts.sources.description',
        defaultMessage: 'Description',
      }),
      dataIndex: 'description',
      search: false,
      sorter: true,
    },
  ];

  const onCreateContractSource = async (fields: FormValueType) => {
    const hide = message.loading(
      intl.formatMessage({
        id: 'apis.loading',
        defaultMessage: 'Loading ...',
      }),
    );
    try {
      await createContractSource({ ...fields });
      hide();
      message.success(
        intl.formatMessage({
          id: 'apis.success',
          defaultMessage: 'Success',
        }),
      );
      return true;
    } catch (error) {
      hide();
      message.error(
        intl.formatMessage({
          id: 'apis.fail',
          defaultMessage: 'Failed',
        }),
      );
      return false;
    }
  };

  return (
    <PageContainer>
      <ProTable<TableListItem>
        actionRef={actionRef}
        rowKey="id"
        search={false}
        toolBarRender={() => [
          <Button type="primary" onClick={() => handleModalVisible(true)}>
            <PlusOutlined />{' '}
            {intl.formatMessage({
              id: 'actions.create',
              defaultMessage: 'Create New',
            })}
          </Button>,
        ]}
        request={() => getContractSources()}
        columns={columns}
      />

      <CreateForm onCancel={() => handleModalVisible(false)} modalVisible={createModalVisible}>
        <ProTable<TableListItem, TableListItem>
          onSubmit={async (values) => {
            const success = await onCreateContractSource(values);
            if (success) {
              handleModalVisible(false);
              if (actionRef.current) {
                actionRef.current.reload();
              }
            }
          }}
          rowKey="id"
          type="form"
          columns={columns}
        />
      </CreateForm>
    </PageContainer>
  );
};

export default TableList;
