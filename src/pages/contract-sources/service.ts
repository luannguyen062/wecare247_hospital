import { apiConfig } from '@/configs/apis';
import request from '../../utils/request';

export async function getContractSourceSelectList(params?: any) {
  return request(`${apiConfig.prefixUrl}/api/select-list/contract-sources`, { params });
}

export async function getContractSources() {
  return request(`${apiConfig.prefixUrl}/api/contract-sources`);
}

export async function createContractSource(params: any) {
  return request(`${apiConfig.prefixUrl}/api/contract-sources`, {
    method: 'POST',
    data: params,
  });
}
