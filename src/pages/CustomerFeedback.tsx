import React, { useEffect, useState } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import { Card, Space, Button, Tag, Form, message, Rate } from 'antd';
import { useIntl, history } from 'umi';
// import styles from './CustomerFeedback.less';
import ProCard from '@ant-design/pro-card';
import { HeartFilled } from '@ant-design/icons';
import ProTable, { ProColumns } from '@ant-design/pro-table';
import moment from 'moment';
import { statusMap } from '@/services/constants';
import ProDescriptions from '@ant-design/pro-descriptions';
import NumberFormat from 'react-number-format';
import ProForm, {
  ModalForm,
  ProFormRate,
  ProFormText,
  ProFormTextArea,
} from '@ant-design/pro-form';
import {
  createCustomerContractFeedback,
  getCustomerContractForFeedback,
} from './contracts/service';

// const CodePreview: React.FC = ({ children }) => (
//   <pre className={styles.pre}>
//     <code>
//       <Typography.Text copyable>{children}</Typography.Text>
//     </code>
//   </pre>
// );

const statusRender = (status: any, statusValue: any) => {
  if (status === 10) {
    return <Tag color="default">{statusValue || statusMap[status]}</Tag>;
  }

  if (status === 20) {
    return <Tag color="success">{statusValue || statusMap[status]}</Tag>;
  }

  if (status === 0) {
    return <Tag color="warning">{statusValue || statusMap[status]}</Tag>;
  }

  if (status === 100) {
    return <Tag color="cyan">{statusValue || statusMap[status]}</Tag>;
  }

  return <Tag color="error">{statusValue || statusMap[status]}</Tag>;
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const CustomerFeedback: React.FC<{}> = (props: any) => {
  const intl = useIntl();
  const [form] = Form.useForm();
  const [contract, setContract] = useState<any>();
  const [modalVisible, setModalVisible] = useState(false);
  const [tab, setTab] = useState('info');
  const [currentService, setCurrentService] = useState<any>();

  const openFeedbackModal = (record: any) => {
    setModalVisible(true);
    setCurrentService(record);
  };

  const serviceColumns: ProColumns<CONTRACT.ContractServiceDetailModel>[] = [
    {
      dataIndex: 'index',
      valueType: 'indexBorder',
      width: 48,
    },
    {
      title: intl.formatMessage({
        id: 'pages.contracts.serviceName',
        defaultMessage: 'Service Name',
      }),
      dataIndex: 'serviceName',
    },
    {
      title: intl.formatMessage({
        id: 'pages.contracts.serviceFrom',
        defaultMessage: 'From',
      }),
      dataIndex: 'from',
      render: (_, record) => moment(record.from).format('L'),
    },
    {
      title: intl.formatMessage({
        id: 'pages.contracts.serviceTo',
        defaultMessage: 'To',
      }),
      dataIndex: 'to',
      render: (_, record) => moment(record.to).format('L'),
    },
    {
      title: intl.formatMessage({
        id: 'pages.contracts.serviceCarer',
        defaultMessage: 'Patient Carer',
      }),
      dataIndex: 'patientCarerName',
      render: (_, record) => {
        if (record.patientCarerId) {
          return <strong>{record.patientCarerName}</strong>;
        }

        return 'N/A';
      },
    },
    {
      title: intl.formatMessage({
        id: 'pages.contracts.serviceHours',
        defaultMessage: 'Actual Hours',
      }),
      dataIndex: 'actualHours',
      valueType: 'digit',
    },
    {
      title: intl.formatMessage({
        id: 'pages.contracts.servicePrice',
        defaultMessage: 'Price',
      }),
      dataIndex: 'totalPrice',
      valueType: 'digit',
    },
    {
      title: intl.formatMessage({
        id: 'pages.contracts.serviceStatus',
        defaultMessage: 'Status',
      }),
      dataIndex: 'status',
      render: (_, record) => statusRender(record.status, null),
    },
    {
      title: intl.formatMessage({
        id: 'pages.contracts.serviceRating',
        defaultMessage: 'Rating',
      }),
      dataIndex: 'rating',
      render: (_, record) => {
        if (record.rating) {
          return <Rate disabled character={<HeartFilled />} allowHalf defaultValue={record.rating} />;
        }

        return 'N/A';
      },
    },
    {
      title: intl.formatMessage({
        id: 'pages.contracts.actions',
        defaultMessage: 'Actions',
      }),
      dataIndex: 'actions',
      render: (_, record) => {
        if (record.status === 100 && record.patientCarerId && !record.rating) {
          return [
            <Button
              type="primary"
              color=""
              key={`view-detail-service-${record.id}`}
              onClick={() => openFeedbackModal(record)}
              shape="round"
              style={{ marginLeft: '10px' }}
            >
              {intl.formatMessage({
                id: 'pages.actions.feedback',
                defaultMessage: 'Feedback',
              })}
            </Button>,
          ];
        }

        return [];
      },
    },
  ];

  const onFetchContract = async (params: any) => {
    const response = await getCustomerContractForFeedback(params);

    if (response.data) {
      setContract(response.data);
    }
  };

  useEffect(() => {
    async function fetchAsync(params: any) {
      await onFetchContract(params);
    }

    const params = {
      contractId: history.location.query.contract,
      accessCode: history.location.query.code,
    };

    fetchAsync(params);

    setTab(history.location.query.tab || 'info');
  }, []);

  useEffect(() => {
    if (form && modalVisible) form.resetFields();
  }, [modalVisible]);

  const renderStatus = (value: any) => {
    if (value === 10) {
      return (
        <Tag color="default">
          {intl.formatMessage({
            id: 'pages.contracts.status.new',
            defaultMessage: 'New',
          })}
        </Tag>
      );
    }

    if (value === 20) {
      return (
        <Tag color="success">
          {intl.formatMessage({
            id: 'pages.contracts.status.active',
            defaultMessage: 'Active',
          })}
        </Tag>
      );
    }

    if (value === 100) {
      return (
        <Tag color="cyan">
          {intl.formatMessage({
            id: 'pages.contracts.status.completed',
            defaultMessage: 'Completed',
          })}
        </Tag>
      );
    }

    if (value === 99) {
      return (
        <Tag color="error">
          {intl.formatMessage({
            id: 'pages.contracts.status.cancelled',
            defaultMessage: 'Cancelled',
          })}
        </Tag>
      );
    }

    return 'N/A';
  };

  const onFeedback = async (values: any) => {
    const params = {
      contractId: history.location.query.contract,
      accessCode: history.location.query.code,
      contractServiceId: currentService.id,
      patientCarerId: currentService.patientCarerId,
      rating: values.rating,
      content: values.content,
      customerId: contract.customer.id,
    };

    const response = await createCustomerContractFeedback(params);
    if (response.data.length > 0) {
      message.success(intl.formatMessage({ id: 'apis.success', defaultMessage: 'Action Success' }));
      setModalVisible(false);
      const fetchParams = {
        contractId: history.location.query.contract,
        accessCode: history.location.query.code,
      };

      await onFetchContract(fetchParams);
    } else {
      const msg =
        response.errors._[0] ||
        intl.formatMessage({ id: 'apis.fail', defaultMessage: 'Action Fail' });

      message.error(msg);
    }
  };

  return (
    <PageContainer>
      <Card loading={!contract}>
        <div>
          <Space style={{ marginBottom: 16 }} />
          <ProCard
            tabs={{
              tabPosition: 'top',
              activeKey: tab,
              onChange: (key) => {
                history.push({
                  pathname: history.location.pathname,
                  query: {
                    ...history.location.query,
                    tab: key,
                  },
                });

                setTab(key);
              },
            }}
          >
            <ProCard.TabPane
              key="info"
              tab={intl.formatMessage({
                id: 'pages.contract.info',
                defaultMessage: 'Detail Information',
              })}
            >
              <ProDescriptions
                title={intl.formatMessage({
                  id: 'pages.customers.title',
                  defaultMessage: 'Customer Information',
                })}
              >
                <ProDescriptions.Item
                  label={intl.formatMessage({
                    id: 'pages.customers.fullName',
                    defaultMessage: 'Full Name',
                  })}
                  valueType="text"
                >
                  <strong>{contract?.customer.fullName}</strong>
                </ProDescriptions.Item>
                <ProDescriptions.Item
                  label={intl.formatMessage({
                    id: 'pages.customers.contactPhone',
                    defaultMessage: 'Phone Number',
                  })}
                  valueType="text"
                >
                  {contract?.customer.contactPhone}
                </ProDescriptions.Item>
                <ProDescriptions.Item
                  label={intl.formatMessage({
                    id: 'pages.customers.gender',
                    defaultMessage: 'Gender',
                  })}
                  valueType="text"
                >
                  {contract?.customer.gender === 1
                    ? intl.formatMessage({
                        id: 'pages.customers.gender.male',
                        defaultMessage: 'Male',
                      })
                    : intl.formatMessage({
                        id: 'pages.customers.gender.female',
                        defaultMessage: 'Female',
                      })}
                </ProDescriptions.Item>
                <ProDescriptions.Item
                  label={intl.formatMessage({
                    id: 'pages.customers.contractAddress',
                    defaultMessage: 'Contract Address',
                  })}
                  valueType="text"
                  plain
                >
                  {contract?.customer.street
                    ? `${contract?.customer.ward.name}, ${contract?.customer.district.name}, ${contract?.customer.province.name}`
                    : 'N/A'}
                </ProDescriptions.Item>
              </ProDescriptions>
              <ProDescriptions
                title={intl.formatMessage({
                  id: 'pages.contracts.title',
                  defaultMessage: 'Contract Information',
                })}
              >
                <ProDescriptions.Item
                  label={intl.formatMessage({
                    id: 'pages.contracts.effectFrom',
                    defaultMessage: 'From',
                  })}
                  valueType="text"
                >
                  {moment(contract?.effectFrom).format('L')}
                </ProDescriptions.Item>
                <ProDescriptions.Item
                  label={intl.formatMessage({
                    id: 'pages.contracts.effectTo',
                    defaultMessage: 'To',
                  })}
                  valueType="text"
                >
                  {moment(contract?.effectTo).format('L')}
                </ProDescriptions.Item>
                <ProDescriptions.Item
                  label={intl.formatMessage({
                    id: 'pages.contracts.signingBy',
                    defaultMessage: 'Signing By',
                  })}
                  valueType="text"
                >
                  {contract?.signingBy}
                </ProDescriptions.Item>
                <ProDescriptions.Item
                  label={intl.formatMessage({
                    id: 'pages.contracts.totalHours',
                    defaultMessage: 'Total Hours',
                  })}
                  valueType="text"
                >
                  {contract?.totalHours}
                </ProDescriptions.Item>
                <ProDescriptions.Item
                  label={intl.formatMessage({
                    id: 'pages.contracts.totalAmount',
                    defaultMessage: 'Total Amount',
                  })}
                  valueType="text"
                >
                  <NumberFormat
                    value={contract?.totalAmount || 0}
                    displayType="text"
                    thousandSeparator=","
                    suffix=" VND"
                  />
                </ProDescriptions.Item>
                <ProDescriptions.Item
                  label={intl.formatMessage({
                    id: 'pages.contracts.totalServices',
                    defaultMessage: 'Total Services',
                  })}
                  valueType="text"
                >
                  {contract?.services.length}
                </ProDescriptions.Item>
                <ProDescriptions.Item
                  label={intl.formatMessage({
                    id: 'pages.contracts.paidAmount',
                    defaultMessage: 'Paid Amount',
                  })}
                  valueType="digit"
                >
                  {/* {contract?.paidAmount || 'N/A'} */}
                  <NumberFormat
                    value={contract?.paidAmount || 0}
                    displayType="text"
                    thousandSeparator=","
                    suffix=" VND"
                  />
                </ProDescriptions.Item>
                <ProDescriptions.Item
                  label={intl.formatMessage({
                    id: 'pages.contracts.remainingAmount',
                    defaultMessage: 'Remaining Amount',
                  })}
                  valueType="digit"
                >
                  {/* {contract?.remainingAmount || 'N/A'} */}
                  <NumberFormat
                    value={contract?.remainingAmount || 0}
                    displayType="text"
                    thousandSeparator=","
                    suffix=" VND"
                  />
                </ProDescriptions.Item>
                <ProDescriptions.Item
                  label={intl.formatMessage({
                    id: 'pages.contracts.status',
                    defaultMessage: 'Status',
                  })}
                  valueType="text"
                >
                  {renderStatus(contract?.status)}
                </ProDescriptions.Item>
                {/* {renderStatusAction(contract?.status)} */}
              </ProDescriptions>
            </ProCard.TabPane>
            <ProCard.TabPane
              key="services"
              tab={intl.formatMessage({
                id: 'pages.services.title',
                defaultMessage: 'Services',
              })}
            >
              <ProTable<CONTRACT.ContractServiceDetailModel>
                columns={serviceColumns}
                rowKey="id"
                search={false}
                pagination={false}
                dateFormatter="string"
                dataSource={contract?.services}
                options={false}
                toolBarRender={false}
              />
            </ProCard.TabPane>
          </ProCard>
        </div>
        <ModalForm
          form={form}
          width="400px"
          title={intl.formatMessage({
            id: 'pages.feedback.title',
            defaultMessage: 'Please give us your feedback',
          })}
          visible={modalVisible}
          modalProps={{
            onCancel: () => setModalVisible(false),
          }}
          onFinish={async (values) => {
            await onFeedback(values);
            return true;
          }}
        >
          <ProForm.Group>
            <ProFormText
              width="md"
              name="patientCarerName"
              label={intl.formatMessage({
                id: 'pages.feedback.patientCarerName',
                defaultMessage: 'Feedback To',
              })}
              disabled
              initialValue={currentService?.patientCarerName}
            />
          </ProForm.Group>
          <ProForm.Group>
            <ProFormRate
              width="md"
              name="rating"
              label={intl.formatMessage({
                id: 'pages.feedback.rating',
                defaultMessage: 'Your Service Rating',
              })}
              fieldProps={{
                character: <HeartFilled />,
              }}
              rules={[
                {
                  required: true,
                  message: intl.formatMessage({
                    id: 'errors.required',
                    defaultMessage: 'This field is required',
                  }),
                },
              ]}
            />
          </ProForm.Group>
          <ProForm.Group>
            <ProFormTextArea
              width="md"
              name="content"
              label={intl.formatMessage({
                id: 'pages.feedback.content',
                defaultMessage: 'Feedback Content',
              })}
              rules={[
                {
                  required: true,
                  message: intl.formatMessage({
                    id: 'errors.required',
                    defaultMessage: 'This field is required',
                  }),
                },
              ]}
            />
          </ProForm.Group>
        </ModalForm>
      </Card>
    </PageContainer>
  );
};

export default CustomerFeedback;
