import React, { useState, useEffect } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import { Card, Col, Row } from 'antd';
import { LightFilter, ProFormDatePicker, ProFormRadio } from '@ant-design/pro-form';
import { useIntl } from 'umi';
import SimpleTable from '@/components/WeCare247/Charts/SimpleTable';
import moment from 'moment';
import { FilterOutlined } from '@ant-design/icons';
import { getContractChart } from '../service';
import LabelLine from '../../../components/WeCare247/Charts/Labelline/index';

const DashboardView: React.FC<{}> = () => {
  const intl = useIntl();
  const [contractChartData, setContractChartData] = useState<any>(null);
  const [contractDataForm, setContractDataForm] = useState<any>({
    type: 20,
  });

  useEffect(() => {
    async function fetchAsync() {
      const contractChart = await getContractChart({ type: contractDataForm.type });
      setContractChartData(contractChart.data);
    }

    fetchAsync();
  }, []);

  const onFetchContracts = async (values: any) => {
    const params = {
      type: values.type,
      fromMonth: moment(values.fromMonth).format('yyyy-MM'),
      toMonth: moment(values.toMonth).format('yyyy-MM'),
    };
    const contractChart = await getContractChart(params);
    setContractChartData(contractChart.data);
    setContractDataForm(params);
  };

  return (
    <PageContainer>
      <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
        <Col className="gutter-row" span={24}>
          <Card
            // title={intl.formatMessage({
            //   id: 'dashboard.contracts',
            //   defaultMessage: 'Contract Data',
            // })}
            loading={!contractChartData}
          >
            {contractChartData && (
              <Row>
                <Col className="gutter-row" span={24}>
                  <LightFilter
                    initialValues={{
                      sex: 'man',
                    }}
                    bordered
                    collapseLabel={<FilterOutlined />}
                    onFinish={onFetchContracts}
                    style={{
                      marginBottom: 30,
                    }}
                  >
                    <ProFormRadio.Group
                      name="type"
                      label={intl.formatMessage({
                        id: 'dashboard.contract.type.title',
                        defaultMessage: 'Filter By',
                      })}
                      radioType="button"
                      fieldProps={{
                        value: contractDataForm.type,
                      }}
                      options={[
                        {
                          label: intl.formatMessage({
                            id: 'dashboard.contract.type.datetime',
                            defaultMessage: 'Month',
                          }),
                          value: 10,
                        },
                        {
                          label: intl.formatMessage({
                            id: 'dashboard.contract.type.contractSources',
                            defaultMessage: 'Contract Sources',
                          }),
                          value: 20,
                        },
                        {
                          label: intl.formatMessage({
                            id: 'dashboard.contract.type.locations',
                            defaultMessage: 'Locations',
                          }),
                          value: 30,
                        },
                        {
                          label: intl.formatMessage({
                            id: 'dashboard.contract.type.department',
                            defaultMessage: 'Hospital Department',
                          }),
                          value: 50,
                        },
                        {
                          label: intl.formatMessage({
                            id: 'dashboard.contract.type.employees',
                            defaultMessage: 'Employees',
                          }),
                          value: 40,
                        },
                      ]}
                      initialValue={20}
                    />
                    <ProFormDatePicker.Month
                      name="fromMonth"
                      placeholder={intl.formatMessage({
                        id: 'dashboard.filter.from',
                        defaultMessage: 'From Month',
                      })}
                    />
                    <ProFormDatePicker.Month
                      name="toMonth"
                      placeholder={intl.formatMessage({
                        id: 'dashboard.filter.to',
                        defaultMessage: 'To Month',
                      })}
                    />
                  </LightFilter>
                </Col>
                <Col xs={24} xl={12} md={12} lg={12}>
                  <SimpleTable
                    data={contractChartData}
                    valueSuffix="HĐDV"
                    title={intl.formatMessage({
                      id: 'dashboard.simple.table.contract.title',
                      defaultMessage: 'Contract Statistic',
                    })}
                  />
                </Col>
                <Col xs={24} xl={12} md={12} lg={12}>
                  <LabelLine
                    data={contractChartData}
                    title={intl.formatMessage({
                      id: 'dashboard.line.title',
                      defaultMessage: 'Line Chart',
                    })}
                  />
                </Col>
              </Row>
            )}
          </Card>
        </Col>
      </Row>
    </PageContainer>
  );
};

export default DashboardView;
