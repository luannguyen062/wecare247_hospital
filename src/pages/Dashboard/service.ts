import { apiConfig } from '@/configs/apis';
import request from '../../utils/request';

export async function getContractChart(params: any) {
  return request<DASHBOARD.ChartResponse>(`${apiConfig.prefixUrl}/api/dashboard/contracts`, {
    params,
  });
}

export async function getPaymentChart(params: any) {
  return request<DASHBOARD.ChartResponse>(`${apiConfig.prefixUrl}/api/dashboard/payments`, {
    params,
  });
}

export async function getCustomerChart(params: any) {
  return request<DASHBOARD.ChartResponse>(`${apiConfig.prefixUrl}/api/dashboard/customers`, {
    params,
  });
}
