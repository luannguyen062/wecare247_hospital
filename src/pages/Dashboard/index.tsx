import React, { useState, useEffect } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import { Card, Col, Row } from 'antd';
import ProForm, { ProFormDatePicker, ProFormRadio } from '@ant-design/pro-form';
import { useIntl } from 'umi';
import SimpleTable from '@/components/WeCare247/Charts/SimpleTable';
import moment from 'moment';
import { getContractChart } from './service';
import LabelLine from '../../components/WeCare247/Charts/Labelline/index';

const DashboardView: React.FC<{}> = () => {
  const intl = useIntl();
  const [contractChartData, setContractChartData] = useState<any>(null);
  const [contractDataForm, setContractDataForm] = useState<any>({
    type: 20,
  });

  useEffect(() => {
    async function fetchAsync() {
      const contractChart = await getContractChart({ type: contractDataForm.type });
      setContractChartData(contractChart.data);
    }

    fetchAsync();
  }, []);

  const onContractValuesChange = async (values: any) => {
    if (values.type) {
      const contractChart = await getContractChart({ type: values.type });
      setContractChartData(contractChart.data);
      setContractDataForm({
        type: values.type,
      });

      return;
    }

    if (values.fromMonth) {
      const contractChart = await getContractChart({
        type: contractDataForm.type,
        fromMonth: moment(values.fromMonth).format('yyyy-MM'),
        toMonth: contractDataForm.toMonth,
      });
      setContractChartData(contractChart.data);
      setContractDataForm({
        ...contractDataForm,
        fromMonth: moment(values.fromMonth).format('yyyy-MM'),
      });

      return;
    }

    if (values.toMonth) {
      const contractChart = await getContractChart({
        type: contractDataForm.type,
        fromMonth: contractDataForm.fromMonth,
        toMonth: moment(values.toMonth).format('yyyy-MM'),
      });
      setContractChartData(contractChart.data);
      setContractDataForm({
        ...contractDataForm,
        toMonth: moment(values.toMonth).format('yyyy-MM'),
      });
    }
  };

  return (
    <PageContainer>
      <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
        <Col className="gutter-row" span={24}>
          <Card
            title={intl.formatMessage({
              id: 'dashboard.contracts',
              defaultMessage: 'Contract Data',
            })}
            loading={!contractChartData}
          >
            {contractChartData && (
              <Row>
                <Col className="gutter-row" span={24}>
                  <ProForm
                    initialValues={{ type: 1 }}
                    onValuesChange={onContractValuesChange}
                    submitter={{
                      // Configure the button text
                      searchConfig: {
                        resetText: 'reset',
                        submitText: 'submit',
                      },
                      // Configure the properties of the button
                      resetButtonProps: {
                        style: {
                          // Hide the reset button
                          display: 'none',
                        },
                      },
                      submitButtonProps: {
                        style: {
                          // Hide the reset button
                          display: 'none',
                        },
                      },
                    }}
                  >
                    <ProFormRadio.Group
                      name="type"
                      label={intl.formatMessage({
                        id: 'dashboard.contract.type.title',
                        defaultMessage: 'Filter By',
                      })}
                      radioType="button"
                      fieldProps={{
                        value: contractDataForm.type,
                      }}
                      options={[
                        {
                          label: intl.formatMessage({
                            id: 'dashboard.contract.type.datetime',
                            defaultMessage: 'Month',
                          }),
                          value: 10,
                        },
                        {
                          label: intl.formatMessage({
                            id: 'dashboard.contract.type.contractSources',
                            defaultMessage: 'Contract Sources',
                          }),
                          value: 20,
                        },
                        {
                          label: intl.formatMessage({
                            id: 'dashboard.contract.type.locations',
                            defaultMessage: 'Locations',
                          }),
                          value: 30,
                        },
                        {
                          label: intl.formatMessage({
                            id: 'dashboard.contract.type.department',
                            defaultMessage: 'Hospital Department',
                          }),
                          value: 50,
                        },
                        {
                          label: intl.formatMessage({
                            id: 'dashboard.contract.type.employees',
                            defaultMessage: 'Employees',
                          }),
                          value: 40,
                        },
                      ]}
                      initialValue={20}
                    />
                    <ProForm.Group>
                      <ProFormDatePicker.Month name="fromMonth" />
                      <ProFormDatePicker.Month name="toMonth" />
                    </ProForm.Group>
                  </ProForm>
                </Col>
                <Col span={12}>
                  <SimpleTable data={contractChartData} valueSuffix="HĐDV" />
                </Col>
                <Col span={12}>
                  <LabelLine data={contractChartData} />
                </Col>
              </Row>
            )}
          </Card>
        </Col>
      </Row>
    </PageContainer>
  );
};

export default DashboardView;
