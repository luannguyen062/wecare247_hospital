import React, { useState, useEffect } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import { Card, Col, Row } from 'antd';
import { LightFilter, ProFormDatePicker, ProFormRadio } from '@ant-design/pro-form';
import { useIntl } from 'umi';
import SimpleTable from '@/components/WeCare247/Charts/SimpleTable';
import moment from 'moment';
import { FilterOutlined } from '@ant-design/icons';
import { getPaymentChart } from '../service';
import LabelLine from '../../../components/WeCare247/Charts/Labelline/index';

const DashboardView: React.FC<{}> = () => {
  const intl = useIntl();
  const [paymentChartData, setPaymentChartData] = useState<any>(null);
  const [paymentDataForm, setPaymentDataForm] = useState<any>({
    type: 20,
  });

  useEffect(() => {
    async function fetchAsync() {
      const contractChart = await getPaymentChart({ type: paymentDataForm.type });
      setPaymentChartData(contractChart.data);
    }

    fetchAsync();
  }, []);

  const onFetchPayments = async (values: any) => {
    const params = {
      type: values.type,
      fromMonth: moment(values.fromMonth).format('yyyy-MM'),
      toMonth: moment(values.toMonth).format('yyyy-MM'),
    };
    const paymentChart = await getPaymentChart(params);
    setPaymentChartData(paymentChart.data);
    setPaymentDataForm(params);
  };

  return (
    <PageContainer>
      <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
        <Col className="gutter-row" span={24}>
          <Card
            title={intl.formatMessage({
              id: 'dashboard.contracts',
              defaultMessage: 'Contract Data',
            })}
            loading={!paymentChartData}
          >
            {paymentChartData && (
              <Row>
                <Col className="gutter-row" span={24}>
                  <LightFilter
                    initialValues={{
                      sex: 'man',
                    }}
                    bordered
                    collapseLabel={<FilterOutlined />}
                    onFinish={onFetchPayments}
                    style={{
                      marginBottom: 30,
                    }}
                  >
                    <ProFormRadio.Group
                      name="type"
                      label={intl.formatMessage({
                        id: 'dashboard.contract.type.title',
                        defaultMessage: 'Filter By',
                      })}
                      radioType="button"
                      fieldProps={{
                        value: paymentDataForm.type,
                      }}
                      options={[
                        {
                          label: intl.formatMessage({
                            id: 'dashboard.contract.type.datetime',
                            defaultMessage: 'Month',
                          }),
                          value: 10,
                        },
                        {
                          label: intl.formatMessage({
                            id: 'dashboard.contract.type.contractSources',
                            defaultMessage: 'Contract Sources',
                          }),
                          value: 20,
                        },
                        {
                          label: intl.formatMessage({
                            id: 'dashboard.contract.type.locations',
                            defaultMessage: 'Locations',
                          }),
                          value: 30,
                        },
                        {
                          label: intl.formatMessage({
                            id: 'dashboard.contract.type.employees',
                            defaultMessage: 'Employees',
                          }),
                          value: 40,
                        },
                      ]}
                      initialValue={20}
                    />
                    <ProFormDatePicker.Month
                      name="fromMonth"
                      placeholder={intl.formatMessage({
                        id: 'dashboard.filter.from',
                        defaultMessage: 'From Month',
                      })}
                    />
                    <ProFormDatePicker.Month
                      name="toMonth"
                      placeholder={intl.formatMessage({
                        id: 'dashboard.filter.to',
                        defaultMessage: 'To Month',
                      })}
                    />
                  </LightFilter>
                </Col>
                <Col xs={24} xl={12} md={12} lg={12}>
                  <SimpleTable
                    data={paymentChartData}
                    valueSuffix="VND"
                    title={intl.formatMessage({
                      id: 'dashboard.simple.table.payment.title',
                      defaultMessage: 'Payment Statistic',
                    })}
                  />
                </Col>
                <Col xs={24} xl={12} md={12} lg={12}>
                  <LabelLine
                    data={paymentChartData}
                    title={intl.formatMessage({
                      id: 'dashboard.line.title',
                      defaultMessage: 'Line Chart',
                    })}
                  />
                </Col>
              </Row>
            )}
          </Card>
        </Col>
      </Row>
    </PageContainer>
  );
};

export default DashboardView;
