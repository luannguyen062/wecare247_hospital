declare namespace DASHBOARD {
  export interface ChartItem {
    text: string;
    value: number;
    extra: any;
  }

  export interface ChartResponse {
    success: boolean;
    errors?: any;
    data: ChartItem[];
  }
}
