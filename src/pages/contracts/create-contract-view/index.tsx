/* eslint-disable @typescript-eslint/no-use-before-define */
import React, { useEffect, useState } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import { Card, Form, message } from 'antd';
import ProForm, {
  ProFormSelect,
  ProFormText,
  ProFormTextArea,
  ProFormDigit,
  ProFormDateRangePicker,
} from '@ant-design/pro-form';
import ProCard from '@ant-design/pro-card';
import { useIntl, history } from 'umi';
import { getLocations } from '@/services/locations';
import { getDistricts } from '@/services/districts';
import { getProvinces } from '@/services/provinces';
import { getWards } from '@/services/wards';
import { getRequireSkills } from '@/services/skills';
import { getServiceSelectList } from '@/services/services';
import { getContractSourceSelectList } from '@/pages/contract-sources/service';
import { getCustomerTypeahead } from '@/pages/customers/service';
import parseCreateContractModel, { createContract, getDepartmentSelectList } from '../service';

const CreateContractView: React.FC<{}> = () => {
  const intl = useIntl();
  const [pageState, setPageState] = useState<CONTRACT.CreateContractState>();
  const [customPrice, setCustomPrice] = useState(false);
  const [typeaheadCustomer, setTypeaheadCustomer] = useState<any>(null);
  const [form] = Form.useForm();

  const onCreateContract = async (values: any) => {
    const response = await createContract(parseCreateContractModel(values));
    if (response.success) {
      message.success(
        intl.formatMessage({
          id: 'api.create.success',
          defaultMessage: 'Success',
        }),
      );

      setTimeout(() => {
        history.push('/contract-view');
      }, 1000);
    } else {
      message.error(
        intl.formatMessage({
          id: 'api.error',
          defaultMessage: 'Fail',
        }),
      );
    }
  };

  const mapToSelectList = (list: any) => list.map((e: any) => ({ label: e.name, value: e.id }));

  useEffect(() => {
    async function fetchListAsync() {
      const lResponse = await getLocations();
      const pResponse = await getProvinces();
      const dResponse = await getDistricts({});
      const wResponse = await getWards({});
      const skillsResponse = await getRequireSkills();
      const serviceResponse = await getServiceSelectList();
      const sourceResponse = await getContractSourceSelectList();
      const departmentResponse = await getDepartmentSelectList();

      setPageState({
        locations: mapToSelectList(lResponse.data || []),
        districts: mapToSelectList(dResponse.data || []),
        promotions: mapToSelectList(lResponse.data || []),
        provinces: mapToSelectList(pResponse.data || []),
        wards: mapToSelectList(wResponse.data || []),
        skills: mapToSelectList(skillsResponse.data || []),
        services: serviceResponse.data || [],
        sources: sourceResponse.data || [],
        departments: departmentResponse.data || [],
      });
    }

    fetchListAsync();
  }, []);

  const onProvinceChange = async (value: string) => {
    const dResponse = await getDistricts({ provinceId: value });
    const newState = {
      ...pageState,
      districts: mapToSelectList(dResponse.data),
    } as CONTRACT.CreateContractState;

    setPageState(newState);
  };

  const onDistrictChange = async (value: string) => {
    const dResponse = await getWards({ districtId: value });
    const newState = {
      ...pageState,
      wards: mapToSelectList(dResponse.data),
    } as CONTRACT.CreateContractState;

    setPageState(newState);
  };

  const onServiceSelect = (value: string) => {
    const service = pageState?.services.filter((e: any) => e.value === value)[0];
    if (service && service.extra.priceNegotiation) {
      setCustomPrice(true);
    } else {
      setCustomPrice(false);
    }
  };

  const setCustomerFields = (customerInfo: any) => {
    if (customerInfo) {
      form.setFields([
        {
          name: 'fullName',
          value: customerInfo.fullName,
        },
        {
          name: 'contactPhone',
          value: customerInfo.contactPhone,
        },
        {
          name: 'secondaryPhone',
          value: customerInfo.secondaryPhone,
        },
        {
          name: 'gender',
          value: customerInfo.gender,
        },
        {
          name: 'street',
          value: customerInfo.street,
        },
        {
          name: 'wardId',
          value: customerInfo.ward.id,
        },
        {
          name: 'districtId',
          value: customerInfo.district.id,
        },
        {
          name: 'provinceId',
          value: customerInfo.province.id,
        },
      ]);
    } else {
      form.resetFields([
        'fullName',
        'gender',
        'street',
        'wardId',
        'districtId',
        'provinceId',
      ]);
    }
  };

  const onValuesChange = async (values: any) => {
    // customer phone / secondary phone
    if (values.contactPhone) {
      const response = await getCustomerTypeahead({
        contactPhone: values.contactPhone,
      });

      setTypeaheadCustomer(response.data);
      setCustomerFields(response.data);
      return;
    }

    if (values.secondaryPhone) {
      const response = await getCustomerTypeahead({
        secondaryPhone: values.secondaryPhone,
      });

      setTypeaheadCustomer(response.data);
      setCustomerFields(response.data);
    }
  };

  return (
    <PageContainer>
      <Card>
        <ProForm
          onFinish={onCreateContract}
          submitter={{
            render: (_, dom) => <div style={{ textAlign: 'right' }}>{dom}</div>,
            submitButtonProps: {
              style: { marginLeft: '10px' },
            },
          }}
          onValuesChange={onValuesChange}
          form={form}
        >
          <ProCard
            title={intl.formatMessage({
              id: 'pages.contracts.create.customerInfo',
              defaultMessage: 'Customer Information',
            })}
            bordered
            headerBordered
            collapsible
            style={{
              marginBottom: 16,
              minWidth: 800,
            }}
          >
            <ProForm.Group size={24}>
              <ProFormText
                name="fullName"
                width="sm"
                label={intl.formatMessage({
                  id: 'pages.contracts.create.customer.fullName',
                  defaultMessage: 'Full Name',
                })}
                rules={[{ required: true }]}
                disabled={typeaheadCustomer != null}
              />
              <ProFormText
                name="contactPhone"
                width="sm"
                label={intl.formatMessage({
                  id: 'pages.contracts.create.customer.contactPhone',
                  defaultMessage: 'Contact Phone',
                })}
                rules={[{ required: true }]}
                // disabled={typeaheadCustomer != null}
              />
              <ProFormText
                name="secondaryPhone"
                width="sm"
                label={intl.formatMessage({
                  id: 'pages.contracts.create.customer.secondaryPhone',
                  defaultMessage: 'Secondary Phone',
                })}
                rules={[{ required: true }]}
                // disabled={typeaheadCustomer != null}
              />
              <ProFormSelect
                width="sm"
                name="gender"
                label={intl.formatMessage({
                  id: 'pages.contracts.create.customer.gender',
                  defaultMessage: 'Gender',
                })}
                options={[
                  {
                    label: intl.formatMessage({
                      id: 'pages.carers.gender.male',
                      defaultMessage: 'Male',
                    }),
                    value: 1,
                  },
                  {
                    label: intl.formatMessage({
                      id: 'pages.carers.gender.female',
                      defaultMessage: 'Female',
                    }),
                    value: 2,
                  },
                ]}
                rules={[{ required: true }]}
                disabled={typeaheadCustomer != null}
              />
            </ProForm.Group>
            <ProForm.Group
              title={intl.formatMessage({
                id: 'pages.contracts.create.customer.contact',
                defaultMessage: 'Contact Address',
              })}
              size={24}
            >
              <ProFormText
                name="street"
                width="sm"
                label={intl.formatMessage({
                  id: 'pages.contracts.create.customer.street',
                  defaultMessage: 'Street',
                })}
                rules={[{ required: true }]}
                disabled={typeaheadCustomer != null}
              />
              <ProFormSelect
                width="sm"
                name="wardId"
                label={intl.formatMessage({
                  id: 'pages.contracts.create.customer.wardId',
                  defaultMessage: 'Ward',
                })}
                options={pageState?.wards}
                rules={[{ required: true }]}
                showSearch
                fieldProps={{
                  optionFilterProp: 'label',
                }}
                disabled={typeaheadCustomer != null}
              />
              <ProFormSelect
                width="sm"
                name="districtId"
                label={intl.formatMessage({
                  id: 'pages.contracts.create.customer.districtId',
                  defaultMessage: 'District',
                })}
                options={pageState?.districts}
                fieldProps={{
                  onSelect: onDistrictChange,
                  optionFilterProp: 'label',
                }}
                rules={[{ required: true }]}
                showSearch
                disabled={typeaheadCustomer != null}
              />
              <ProFormSelect
                width="sm"
                name="provinceId"
                label={intl.formatMessage({
                  id: 'pages.contracts.create.customer.provinceId',
                  defaultMessage: 'Province',
                })}
                options={pageState?.provinces}
                fieldProps={{
                  onSelect: onProvinceChange,
                  optionFilterProp: 'label',
                }}
                rules={[{ required: true }]}
                showSearch
                disabled={typeaheadCustomer != null}
              />
            </ProForm.Group>
            <ProForm.Group
              title={intl.formatMessage({
                id: 'pages.contracts.patient.title',
                defaultMessage: 'Patient Information',
              })}
              size={24}
            >
              <ProFormText
                name="patientFullName"
                width="sm"
                label={intl.formatMessage({
                  id: 'pages.contracts.create.patient.fullName',
                  defaultMessage: 'Full Name',
                })}
                rules={[{ required: true }]}
              />
              <ProFormSelect
                width="sm"
                name="patientGender"
                label={intl.formatMessage({
                  id: 'pages.contracts.create.customer.gender',
                  defaultMessage: 'Gender',
                })}
                valueEnum={{
                  1: intl.formatMessage({
                    id: 'pages.carers.gender.male',
                    defaultMessage: 'Male',
                  }),
                  2: intl.formatMessage({
                    id: 'pages.carers.gender.female',
                    defaultMessage: 'Female',
                  }),
                }}
                rules={[{ required: true }]}
              />
              <ProFormDigit
                name="patientYearOfBirth"
                width="sm"
                label={intl.formatMessage({
                  id: 'pages.contracts.create.patient.patientYearOfBirth',
                  defaultMessage: 'Year Of Birth',
                })}
                rules={[{ required: true }]}
              />
              <ProFormDigit
                name="patientWeight"
                width="sm"
                label={intl.formatMessage({
                  id: 'pages.contracts.create.patient.weight',
                  defaultMessage: 'Weight',
                })}
                rules={[{ required: true }]}
              />
            </ProForm.Group>
          </ProCard>
          <ProCard
            title={intl.formatMessage({
              id: 'pages.contracts.create.info',
              defaultMessage: 'Contract Information',
            })}
            bordered
            headerBordered
            collapsible
            style={{
              minWidth: 800,
              marginBottom: 16,
            }}
          >
            {' '}
            <ProForm.Group
              title={intl.formatMessage({
                id: 'pages.contracts.create.required-skills',
                defaultMessage: 'Work Skills',
              })}
              size={24}
            >
              <ProFormSelect
                name="skills"
                label={intl.formatMessage({
                  id: 'pages.contracts.create.skills',
                  defaultMessage: 'Required Skills',
                })}
                fieldProps={{
                  mode: 'multiple',
                  optionFilterProp: 'label',
                }}
                width="md"
                options={pageState?.skills}
                rules={[{ required: true }]}
                showSearch
              />
              <ProFormSelect
                name="contractLocationId"
                label={intl.formatMessage({
                  id: 'pages.contracts.create.contractLocationId',
                  defaultMessage: 'Contract Location',
                })}
                width="sm"
                options={pageState?.locations}
                rules={[{ required: true }]}
                showSearch
                fieldProps={{
                  optionFilterProp: 'label',
                }}
              />
              <ProFormSelect
                name="locationDepartmentId"
                label={intl.formatMessage({
                  id: 'pages.contracts.create.locationDepartmentId',
                  defaultMessage: 'Department',
                })}
                width="sm"
                options={pageState?.departments}
                rules={[{ required: true }]}
                showSearch
                fieldProps={{
                  optionFilterProp: 'label',
                }}
              />
              <ProFormSelect
                name="contractSourceId"
                label={intl.formatMessage({
                  id: 'pages.contracts.create.contractSourceId',
                  defaultMessage: 'Contract Sources',
                })}
                width="sm"
                options={pageState?.sources}
                rules={[{ required: true }]}
                showSearch
                fieldProps={{
                  optionFilterProp: 'label',
                }}
              />
            </ProForm.Group>
            <ProForm.Group
              title={intl.formatMessage({
                id: 'pages.contracts.create.services',
                defaultMessage: 'Contact Services',
              })}
              size={24}
            >
              <ProFormSelect
                width="md"
                name="serviceId"
                label={intl.formatMessage({
                  id: 'pages.contracts.create.serviceId',
                  defaultMessage: 'Service',
                })}
                options={pageState?.services}
                rules={[{ required: true }]}
                fieldProps={{
                  onSelect: onServiceSelect,
                }}
              />
              <ProFormDigit
                width="sm"
                name="servicePrice"
                label={intl.formatMessage({
                  id: 'pages.contracts.create.servicePrice',
                  defaultMessage: 'Price',
                })}
                disabled={!customPrice}
              />
              <ProFormDateRangePicker
                width="lg"
                name="fromTo"
                label={intl.formatMessage({
                  id: 'pages.contracts.create.fromTo',
                  defaultMessage: 'From - To',
                })}
                rules={[{ required: true }]}
              />
            </ProForm.Group>
            <ProForm.Group>
              <ProFormTextArea
                name="note"
                width="lg"
                label={intl.formatMessage({
                  id: 'pages.contracts.create.note',
                  defaultMessage: 'Note',
                })}
              />
            </ProForm.Group>
          </ProCard>
        </ProForm>
      </Card>
    </PageContainer>
  );
};

export default CreateContractView;
