import React, { useState, useEffect } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import { Button, Card, message, Popconfirm, Space, Tag } from 'antd';
import ProCard from '@ant-design/pro-card';
import ProDescriptions from '@ant-design/pro-descriptions';
import '../style.css';
import ProTable from '@ant-design/pro-table';
import type { ProColumns } from '@ant-design/pro-table';
import moment from 'moment';
import {
  EditOutlined,
  PlusOutlined,
  DeleteFilled,
  CheckCircleFilled,
  // CheckCircleOutlined,
  // ExclamationCircleOutlined,
} from '@ant-design/icons';
import { useIntl, history, FormattedMessage, Link } from 'umi';
import { createCarerPayment, updateCarerPayment } from '@/pages/patient-carer-payments/service';
import { createContractPayment, updateContractPayment } from '@/pages/payments/service';
import { paymentTypeMap, statusMap } from '@/services/constants';
import NumberFormat from 'react-number-format';
import {
  closeContract,
  createContractService,
  getContract,
  updateContractService,
  cancelContract,
  // closeService,
} from '../service';
import CreateServiceForm from '../components/CreateServiceForm';
import UpdateServiceForm from '../components/UpdateServiceForm';
import PaymentForm from '../components/PaymentForm';
import CarerPaymentForm from '../components/PatientCarerPayment';

// const { confirm } = Modal;
const statusRender = (status: any, statusValue: any) => {
  if (status === 10) {
    return <Tag color="default">{statusValue || statusMap[status]}</Tag>;
  }

  if (status === 20) {
    return <Tag color="success">{statusValue || statusMap[status]}</Tag>;
  }

  if (status === 0) {
    return <Tag color="warning">{statusValue || statusMap[status]}</Tag>;
  }

  if (status === 100) {
    return <Tag color="cyan">{statusValue || statusMap[status]}</Tag>;
  }

  return <Tag color="error">{statusValue || statusMap[status]}</Tag>;
};

const ContractInformationView: React.FC<{}> = (props: any) => {
  const intl = useIntl();
  const [tab, setTab] = useState('info');
  const [contract, setContract] = useState<CONTRACT.ContractDetailModel>();
  const [serviceFormVisible, setServiceFormVisible] = useState<boolean>(false);
  const [serviceUpdateFormVisible, setServiceUpdateFormVisible] = useState<boolean>(false);
  const [currentService, setCurrentService] = useState<any>();
  const [currentPayment, setCurrentPayment] = useState<any>(null);
  const [paymentModalVisible, setPaymentModalVisible] = useState(false);
  const [currentCarerPayment, setCurrentCarerPayment] = useState<any>();
  const [carerPaymentModalVisible, setCarerPaymentModalVisible] = useState(false);
  const [closePopupVisible, setClosePopupVisible] = useState(false);
  const [cancelPopupVisible, setCancelPopupVisible] = useState(false);
  const [closeConfirmLoading, setCloseConfirmLoading] = useState(false);
  const [cancelConfirmLoading, setCancelConfirmLoading] = useState(false);

  const onOpenCreateServiceModal = () => {
    setServiceFormVisible(true);
  };

  const onOpenUpdateServiceModal = (record: any) => {
    setCurrentService(record);
    setServiceUpdateFormVisible(true);
  };

  const onOpenCreatePaymentModal = () => {
    setCurrentPayment(null);
    setPaymentModalVisible(true);
  };

  const onOpenUpdatePaymentModal = (record: any) => {
    setCurrentPayment(record);
    setPaymentModalVisible(true);
  };

  const onOpenCarerPaymentModal = (record: any) => {
    if (record) {
      setCurrentCarerPayment(record);
    } else {
      setCurrentCarerPayment(null);
    }
    setCarerPaymentModalVisible(true);
  };

  const serviceColumns: ProColumns<CONTRACT.ContractServiceDetailModel>[] = [
    {
      dataIndex: 'index',
      valueType: 'indexBorder',
      width: 48,
    },
    {
      title: intl.formatMessage({
        id: 'pages.contracts.serviceName',
        defaultMessage: 'Service Name',
      }),
      dataIndex: 'serviceName',
    },
    {
      title: intl.formatMessage({
        id: 'pages.contracts.serviceFrom',
        defaultMessage: 'From',
      }),
      dataIndex: 'from',
      render: (_, record) => moment(record.from).format('L'),
    },
    {
      title: intl.formatMessage({
        id: 'pages.contracts.serviceTo',
        defaultMessage: 'To',
      }),
      dataIndex: 'to',
      render: (_, record) => moment(record.to).format('L'),
    },
    {
      title: intl.formatMessage({
        id: 'pages.contracts.serviceCarer',
        defaultMessage: 'Patient Carer',
      }),
      dataIndex: 'patientCarerName',
      render: (_, record) => {
        if (record.patientCarerId) {
          return (
            <Link to={`/patient-carer-view/${record.patientCarerId}`}>
              <strong>{record.patientCarerName}</strong>
            </Link>
          );
        }

        return 'N/A';
      },
    },
    {
      title: intl.formatMessage({
        id: 'pages.contracts.serviceHours',
        defaultMessage: 'Actual Hours',
      }),
      dataIndex: 'actualHours',
      valueType: 'digit',
    },
    {
      title: intl.formatMessage({
        id: 'pages.contracts.servicePrice',
        defaultMessage: 'Price',
      }),
      dataIndex: 'totalPrice',
      valueType: 'digit',
    },
    {
      title: intl.formatMessage({
        id: 'pages.contracts.serviceStatus',
        defaultMessage: 'Status',
      }),
      dataIndex: 'status',
      // render: (_, record) => {
      //   if (moment(record.from) > moment()) {
      //     return <Tag color="warning">Chưa Thực Hiện</Tag>;
      //   }

      //   if (moment(record.to) < moment()) {
      //     return <Tag color="geekblue">Đã Thực Hiện</Tag>;
      //   }

      //   return <Tag color="green">Đang Thực Hiện</Tag>;
      // },
      render: (_, record) => statusRender(record.status, null),
    },
    {
      title: intl.formatMessage({
        id: 'pages.contracts.actions',
        defaultMessage: 'Actions',
      }),
      dataIndex: 'actions',
      render: (_, record) => [
        <Button
          type="primary"
          color=""
          key={`view-detail-service-${record.id}`}
          onClick={() => onOpenUpdateServiceModal(record)}
          shape="circle"
          style={{ marginLeft: '10px' }}
          disabled={record.status === 100}
        >
          <EditOutlined />
        </Button>,
      ],
    },
  ];

  const scheduleColumns: ProColumns<SCHEDULE.ContractScheduleModel>[] = [
    {
      dataIndex: 'id',
      hideInTable: true,
    },
    {
      title: intl.formatMessage({
        id: 'pages.contracts.serviceName',
        defaultMessage: 'Service Name',
      }),
      dataIndex: 'serviceName',
    },
    {
      title: intl.formatMessage({
        id: 'pages.contracts.serviceFrom',
        defaultMessage: 'From',
      }),
      dataIndex: 'from',
      render: (_, record) => moment(record.from).format('L'),
    },
    {
      title: intl.formatMessage({
        id: 'pages.contracts.serviceTo',
        defaultMessage: 'To',
      }),
      dataIndex: 'to',
      render: (_, record) => moment(record.to).format('L'),
    },
    {
      title: intl.formatMessage({
        id: 'pages.contracts.serviceCarer',
        defaultMessage: 'Patient Carer',
      }),
      dataIndex: 'patientCarerName',
      render: (_, record) => {
        if (record.patientCarerName) {
          return (
            <Link to={`/patient-carer-view/${record.patientCarerId}`}>
              <strong>{record.patientCarerName}</strong>
            </Link>
          );
        }

        return 'N/A';
      },
    },
    {
      title: intl.formatMessage({
        id: 'pages.contracts.serviceStatus',
        defaultMessage: 'Status',
      }),
      dataIndex: 'status',
      // render: (_, record) => {
      //   if (record.status === 20) {
      //     return <Tag color="success">Đang Hiệu Lực</Tag>;
      //   }

      //   return <Tag color="blue">Đã Hiệu Lực</Tag>;
      // },
      render: (_, record) => statusRender(record.status, null),
    },
    {
      title: intl.formatMessage({
        id: 'pages.contracts.actions',
        defaultMessage: 'Actions',
      }),
      dataIndex: 'actions',
      render: (_, record) => {
        if (moment(record.from) < moment()) {
          return (
            <Button
              type="primary"
              color=""
              key="view-detail-schedule"
              // onClick={() => onServiceForm(record)}
              shape="circle"
              style={{ marginLeft: '10px' }}
              disabled
            >
              <EditOutlined />
            </Button>
          );
        }

        return null;
      },
    },
  ];

  const paymentColumns: ProColumns<PAYMENT.PaymentItem>[] = [
    {
      title: intl.formatMessage({
        id: 'pages.payments.paymentRef',
        defaultMessage: 'Payment Ref',
      }),
      dataIndex: 'id',
    },
    {
      title: intl.formatMessage({
        id: 'pages.payments.amount',
        defaultMessage: 'Amount',
      }),
      dataIndex: 'amount',
      valueType: 'digit',
    },
    {
      title: intl.formatMessage({
        id: 'pages.payments.type',
        defaultMessage: 'Type',
      }),
      dataIndex: 'type',
      render: (_, record) => {
        if (record.type === 20) {
          return <Tag color="warning">{paymentTypeMap[record.type]}</Tag>;
        }

        return <Tag color="success">{paymentTypeMap[record.type]}</Tag>;
      },
    },
    {
      title: intl.formatMessage({
        id: 'actions.title',
        defaultMessage: 'Actions',
      }),
      dataIndex: 'actions',
      render: (_, record) => {
        return (
          <Button
            type="primary"
            color=""
            key="view-detail-payment"
            onClick={() => onOpenUpdatePaymentModal(record)}
            shape="circle"
            style={{ marginLeft: '10px' }}
          >
            <EditOutlined />
          </Button>
        );
      },
    },
  ];

  const carerPaymentColumns: ProColumns<PAYMENT.PaymentItem>[] = [
    {
      title: 'Payment Ref',
      dataIndex: 'id',
    },
    {
      title: 'Amount',
      dataIndex: 'amount',
      valueType: 'digit',
    },
    {
      title: 'To Carer',
      dataIndex: 'patientCarerName',
    },
    {
      title: 'Type',
      dataIndex: 'type',
      render: (_, record) => {
        return <Tag color="success">{paymentTypeMap[record.type]}</Tag>;
      },
    },
    {
      title: 'Actions',
      dataIndex: 'actions',
      render: (_, record) => {
        return (
          <Button
            type="primary"
            color=""
            key="view-detail-carer-payment"
            onClick={() => onOpenCarerPaymentModal(record)}
            shape="circle"
            style={{ marginLeft: '10px' }}
          >
            <EditOutlined />
          </Button>
        );
      },
    },
  ];

  const onServiceFormVisibleChange = () => {
    setServiceFormVisible(!serviceFormVisible);
  };

  const onFetchContract = async (id: any) => {
    const response = await getContract(id);
    if (!response.success) {
      message.error(
        intl.formatMessage({
          id: 'api.error',
          defaultMessage: 'Fail',
        }),
      );
    }

    setContract(response.data);
  };

  useEffect(() => {
    const contractId = props.match.params.id;
    const activeTab = props.location.query.tab;
    setTab(activeTab || 'info');

    async function fetchAsync() {
      await onFetchContract(contractId);
    }

    fetchAsync();
  }, []);

  const renderStatus = (value: any) => {
    if (value === 10) {
      return (
        <Tag color="default">
          {intl.formatMessage({
            id: 'pages.contracts.status.new',
            defaultMessage: 'New',
          })}
        </Tag>
      );
    }

    if (value === 20) {
      return (
        <Tag color="success">
          {intl.formatMessage({
            id: 'pages.contracts.status.active',
            defaultMessage: 'Active',
          })}
        </Tag>
      );
    }

    if (value === 100) {
      return (
        <Tag color="cyan">
          {intl.formatMessage({
            id: 'pages.contracts.status.completed',
            defaultMessage: 'Completed',
          })}
        </Tag>
      );
    }

    if (value === 99) {
      return (
        <Tag color="error">
          {intl.formatMessage({
            id: 'pages.contracts.status.cancelled',
            defaultMessage: 'Cancelled',
          })}
        </Tag>
      );
    }

    return 'N/A';
  };

  const showClosePopconfirm = () => {
    setClosePopupVisible(true);
  };

  const showCancelPopconfirm = () => {
    setCancelPopupVisible(true);
  };

  const onCloseContract = async () => {
    setCloseConfirmLoading(true);
    const response = await closeContract(contract?.id);
    if (response.success === true) {
      setTimeout(() => {
        setClosePopupVisible(false);
        setCloseConfirmLoading(false);
      }, 1500);

      await onFetchContract(contract?.id);
    }
  };

  const onCancelContract = async () => {
    setCancelConfirmLoading(true);
    const response = await cancelContract(contract?.id);
    if (response.success === true) {
      setTimeout(() => {
        setCancelPopupVisible(false);
        setCancelConfirmLoading(false);
      }, 1500);

      await onFetchContract(contract?.id);
    }
  };

  const onCloseContractCancel = () => {
    setClosePopupVisible(false);
  };

  const onCancelContractCancel = () => {
    setCancelPopupVisible(false);
  };

  const renderStatusAction = (value: any) => {
    if (value === 10 || value === 20) {
      return (
        <ProDescriptions.Item>
          <Popconfirm
            title={intl.formatMessage({
              id: 'pages.contract.actions.complete.confirm',
              defaultMessage: 'Please confirm to CLOSE contract?',
            })}
            visible={closePopupVisible}
            onConfirm={onCloseContract}
            okButtonProps={{ loading: closeConfirmLoading }}
            onCancel={onCloseContractCancel}
          >
            <Button
              type="primary"
              key="complete-contract"
              onClick={showClosePopconfirm}
              shape="round"
            >
              <CheckCircleFilled />{' '}
              {intl.formatMessage({
                id: 'pages.contract.actions.complete',
                defaultMessage: 'Close',
              })}
            </Button>
          </Popconfirm>
          <Popconfirm
            title={intl.formatMessage({
              id: 'pages.contract.actions.cancel.confirm',
              defaultMessage: 'Please confirm to CANCEL contract?',
            })}
            visible={cancelPopupVisible}
            onConfirm={onCancelContract}
            okButtonProps={{ loading: cancelConfirmLoading }}
            onCancel={onCancelContractCancel}
          >
            <Button
              type="primary"
              key="cancel-contract"
              onClick={showCancelPopconfirm}
              style={{
                marginLeft: '20px',
              }}
              danger
              shape="round"
            >
              <DeleteFilled />{' '}
              {intl.formatMessage({
                id: 'pages.contract.actions.cancel',
                defaultMessage: 'Cancel',
              })}
            </Button>
          </Popconfirm>
        </ProDescriptions.Item>
      );
    }

    return <></>;
  };

  const onCreateService = (values: any) => {
    return createContractService({
      ...values,
      contractId: contract?.id,
      from: moment.utc(values.serviceTime[0]).toDate(),
      to: moment.utc(values.serviceTime[1]).toDate(),
    });
  };

  const onUpdateService = (values: any) => {
    return updateContractService({
      id: currentService?.id,
      contractId: contract?.id,
      from: moment.utc(values.serviceTime[0]).toDate(),
      to: moment.utc(values.serviceTime[1]).toDate(),
      serviceId: values.serviceId,
      patientCarerId: values.patientCarerId,
      price: values.price,
    });
  };

  const onCreatePayment = (values: any) => {
    return createContractPayment({
      ...values,
      type: Number(values.type),
    });
  };

  const onUpdatePayment = (values: any) => {
    return updateContractPayment({
      ...values,
      type: Number(values.type),
    });
  };

  const onCreateCarerPayment = (values: any) => {
    return createCarerPayment({
      ...values,
    });
  };

  const onUpdateCarerPayment = (values: any) => {
    return updateCarerPayment({
      ...values,
    });
  };

  const handlePaymentModalVisible = (value: boolean) => {
    setPaymentModalVisible(value);
  };

  const handleCarerPaymentModalVisible = (value: boolean) => {
    setCarerPaymentModalVisible(value);
  };

  return (
    <PageContainer>
      <Card loading={!contract}>
        <div>
          <Space style={{ marginBottom: 16 }} />
          <ProCard
            tabs={{
              tabPosition: 'top',
              activeKey: tab,
              onChange: (key) => {
                history.push({
                  pathname: history.location.pathname,
                  query: {
                    tab: key,
                  },
                });

                setTab(key);
              },
            }}
          >
            <ProCard.TabPane
              key="info"
              tab={intl.formatMessage({
                id: 'pages.contract.info',
                defaultMessage: 'Detail Information',
              })}
            >
              <ProDescriptions
                title={intl.formatMessage({
                  id: 'pages.customers.title',
                  defaultMessage: 'Customer Information',
                })}
              >
                <ProDescriptions.Item
                  label={intl.formatMessage({
                    id: 'pages.customers.fullName',
                    defaultMessage: 'Full Name',
                  })}
                  valueType="text"
                >
                  <Link to={`/customer-view/${contract?.customer.id}`}>
                    <strong>{contract?.customer.fullName}</strong>
                  </Link>
                </ProDescriptions.Item>
                <ProDescriptions.Item
                  label={intl.formatMessage({
                    id: 'pages.customers.contactPhone',
                    defaultMessage: 'Phone Number',
                  })}
                  valueType="text"
                >
                  {contract?.customer.contactPhone}
                </ProDescriptions.Item>
                <ProDescriptions.Item
                  label={intl.formatMessage({
                    id: 'pages.customers.gender',
                    defaultMessage: 'Gender',
                  })}
                  valueType="text"
                >
                  {contract?.customer.gender === 1
                    ? intl.formatMessage({
                        id: 'pages.customers.gender.male',
                        defaultMessage: 'Male',
                      })
                    : intl.formatMessage({
                        id: 'pages.customers.gender.female',
                        defaultMessage: 'Female',
                      })}
                </ProDescriptions.Item>
                <ProDescriptions.Item
                  label={intl.formatMessage({
                    id: 'pages.customers.contractAddress',
                    defaultMessage: 'Contract Address',
                  })}
                  valueType="text"
                  plain
                >
                  {contract?.customer.street
                    ? `${contract?.customer.ward.name}, ${contract?.customer.district.name}, ${contract?.customer.province.name}`
                    : 'N/A'}
                </ProDescriptions.Item>
              </ProDescriptions>
              <ProDescriptions
                title={intl.formatMessage({
                  id: 'pages.contracts.title',
                  defaultMessage: 'Contract Information',
                })}
              >
                <ProDescriptions.Item
                  label={intl.formatMessage({
                    id: 'pages.contracts.effectFrom',
                    defaultMessage: 'From',
                  })}
                  valueType="text"
                >
                  {moment(contract?.effectFrom).format('L')}
                </ProDescriptions.Item>
                <ProDescriptions.Item
                  label={intl.formatMessage({
                    id: 'pages.contracts.effectTo',
                    defaultMessage: 'To',
                  })}
                  valueType="text"
                >
                  {moment(contract?.effectTo).format('L')}
                </ProDescriptions.Item>
                <ProDescriptions.Item
                  label={intl.formatMessage({
                    id: 'pages.contracts.signingBy',
                    defaultMessage: 'Signing By',
                  })}
                  valueType="text"
                >
                  {contract?.signingBy}
                </ProDescriptions.Item>
                <ProDescriptions.Item
                  label={intl.formatMessage({
                    id: 'pages.contracts.totalHours',
                    defaultMessage: 'Total Hours',
                  })}
                  valueType="text"
                >
                  {contract?.totalHours}
                </ProDescriptions.Item>
                <ProDescriptions.Item
                  label={intl.formatMessage({
                    id: 'pages.contracts.totalAmount',
                    defaultMessage: 'Total Amount',
                  })}
                  valueType="text"
                >
                  <NumberFormat value={contract?.totalAmount || 0} displayType="text" thousandSeparator="," suffix=" VND" />
                </ProDescriptions.Item>
                <ProDescriptions.Item
                  label={intl.formatMessage({
                    id: 'pages.contracts.totalServices',
                    defaultMessage: 'Total Services',
                  })}
                  valueType="text"
                >
                  {contract?.services.length}
                </ProDescriptions.Item>
                <ProDescriptions.Item
                  label={intl.formatMessage({
                    id: 'pages.contracts.paidAmount',
                    defaultMessage: 'Paid Amount',
                  })}
                  valueType="text"
                >
                  {/* {contract?.paidAmount || 'N/A'} */}
                  <NumberFormat value={contract?.paidAmount} displayType="text" thousandSeparator="," suffix=" VND" />
                </ProDescriptions.Item>
                <ProDescriptions.Item
                  label={intl.formatMessage({
                    id: 'pages.contracts.remainingAmount',
                    defaultMessage: 'Remaining Amount',
                  })}
                  valueType="text"
                >
                  {/* {contract?.remainingAmount || 'N/A'} */}
                  <NumberFormat value={contract?.remainingAmount} displayType="text" thousandSeparator="," suffix=" VND" />
                </ProDescriptions.Item>
                <ProDescriptions.Item
                  label={intl.formatMessage({
                    id: 'pages.contracts.status',
                    defaultMessage: 'Status',
                  })}
                  valueType="text"
                >
                  {renderStatus(contract?.status)}
                </ProDescriptions.Item>
                {renderStatusAction(contract?.status)}
              </ProDescriptions>
            </ProCard.TabPane>
            <ProCard.TabPane
              key="services"
              tab={intl.formatMessage({
                id: 'pages.services.title',
                defaultMessage: 'Services',
              })}
            >
              <ProTable<CONTRACT.ContractServiceDetailModel>
                columns={serviceColumns}
                rowKey="id"
                search={false}
                pagination={false}
                dateFormatter="string"
                dataSource={contract?.services}
                options={false}
                toolBarRender={() => [
                  <Button
                    type="primary"
                    key="primary"
                    onClick={() => {
                      onOpenCreateServiceModal();
                    }}
                  >
                    <PlusOutlined />{' '}
                    <FormattedMessage id="actions.create" defaultMessage="Create" />
                  </Button>,
                ]}
              />
            </ProCard.TabPane>
            <ProCard.TabPane
              key="payments"
              tab={intl.formatMessage({
                id: 'pages.payments.title',
                defaultMessage: 'Payments',
              })}
            >
              <ProTable<PAYMENT.PaymentItem>
                columns={paymentColumns}
                rowKey="id"
                search={false}
                pagination={false}
                dateFormatter="string"
                dataSource={contract?.payments}
                toolBarRender={() => [
                  <Button type="primary" key="primary" onClick={onOpenCreatePaymentModal}>
                    <PlusOutlined />{' '}
                    <FormattedMessage id="actions.create" defaultMessage="Create" />
                  </Button>,
                ]}
              />
            </ProCard.TabPane>
            <ProCard.TabPane
              key="care-payments"
              tab={intl.formatMessage({
                id: 'pages.carerPayments.title',
                defaultMessage: 'Patient Carer Payments',
              })}
            >
              <ProTable<PAYMENT.PaymentItem>
                columns={carerPaymentColumns}
                rowKey="id"
                search={false}
                pagination={false}
                dateFormatter="string"
                dataSource={contract?.patientCarerPayments}
                toolBarRender={() => [
                  <Button
                    type="primary"
                    key="primary"
                    onClick={() => onOpenCarerPaymentModal(null)}
                  >
                    <PlusOutlined />{' '}
                    <FormattedMessage id="actions.create" defaultMessage="Create" />
                  </Button>,
                ]}
              />
            </ProCard.TabPane>
            <ProCard.TabPane
              key="schedules"
              tab={intl.formatMessage({
                id: 'pages.schedules.title',
                defaultMessage: 'Schedules',
              })}
            >
              <ProTable<SCHEDULE.ContractScheduleModel>
                columns={scheduleColumns}
                rowKey="id"
                search={false}
                pagination={false}
                dateFormatter="string"
                dataSource={contract?.contractSchedules}
                toolBarRender={false}
              />
            </ProCard.TabPane>
          </ProCard>
        </div>
      </Card>
      <UpdateServiceForm
        modalVisible={serviceUpdateFormVisible}
        onCancel={() => setServiceUpdateFormVisible(false)}
        onFinish={onUpdateService}
        patientCarerOptions={[]}
        serviceOptions={[]}
        currentService={currentService}
      />
      <CreateServiceForm
        modalVisible={serviceFormVisible}
        onCancel={onServiceFormVisibleChange}
        patientCarerOptions={[]}
        serviceOptions={[]}
        onFinish={onCreateService}
      />
      <PaymentForm
        contractCode={contract?.id || ''}
        onFinish={currentPayment != null ? onUpdatePayment : onCreatePayment}
        visible={paymentModalVisible}
        onVisibleChange={handlePaymentModalVisible}
        currentPayment={currentPayment}
      />
      <CarerPaymentForm
        contractCode={contract?.id || ''}
        onFinish={currentCarerPayment != null ? onUpdateCarerPayment : onCreateCarerPayment}
        visible={carerPaymentModalVisible}
        onVisibleChange={handleCarerPaymentModalVisible}
        currentPayment={currentCarerPayment}
      />
    </PageContainer>
  );
};

export default ContractInformationView;
