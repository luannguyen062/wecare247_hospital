declare namespace CONTRACT {
  export interface ContractItem {
    id: string;
    signingDate?: Date;
    effectFrom?: Date;
    effectTo?: Date;
    totalDays: number;
    totalAmount: number;
    paymentAmount?: number;
    paidAmount?: number;
    remainingAmount?: number;
    actualDays?: number;
    signingBy: string;
    signingByName: string;
    customerId: string;
    customerName: string;
    contractSourceName: string;
    address: string;
    wardName: string;
    districtName: string;
    provinceName: string;
    note: string;
    status: number;
    statusValue: string;
  }

  export interface ContractListResponse {
    success: boolean;
    errors: any;
    data: API.PaginationData<ContractItem>;
  }

  export interface CreateContractModel {
    note: string;
    promotionId: string;
    contractLocationId: string;
    contractSourceId: string;
    locationDepartmentId: string;
    patient: CreatePatientModel;
    customer: CreateCustomerModel;
    services: CreateContractServiceModel[];
    requiredSkills: CreateContractRequireSkillModel[];
  }

  export interface CreatePatientModel {
    fullName: string;
    yearOfBirth: number;
    weight: number;
    gender: number;
  }

  export interface CreateCustomerModel {
    fullName: string;
    contactPhone: string;
    wardId: string;
    districtId: string;
    provinceId: string;
    street: string;
    avatar: string;
    note: string;
    gender: number;
  }

  export interface CreateContractServiceModel {
    serviceId: string;
    locationId: string;
    from: Date;
    to: Date;
    price?: number;
  }

  export interface ContractServiceModel {
    id: string;
    name: string;
    serviceId: string;
    locationId: string;
    from: Date;
    to: Date;
  }

  export interface CreateContractRequireSkillModel {
    id: string;
    value: boolean;
  }

  export interface ContractDetailModel {
    id: string;
    signingDate?: string;
    effectFrom?: string;
    effectTo?: string;
    totalHours?: string;
    totalAmount?: string;
    paymentAmount?: string;
    paidAmount?: string;
    remainingAmount?: string;
    signingBy: string;
    customerId: string;
    note: string;
    contractLocationId: string;
    status: number;
    customer: CUSTOMER.CustomerDetailModel;
    services: ContractServiceDetailModel[];
    requiredSkills: ContractRequireSkillDetailModel[];
    payments?: PAYMENT.PaymentItem[];
    patientCarerPayments?: PAYMENT.PaymentItem[];
    contractSchedules?: SCHEDULE.ContractScheduleModel[];
  }

  export interface ContractServiceDetailModel {
    id: string;
    contractId: string;
    serviceId: string;
    serviceName: string;
    customerId: string;
    patientCarerId?: string;
    patientCarerName?: string;
    from: Date;
    to: Date;
    created: Date;
    calendarHours: number;
    actualHours: number;
    totalPrice: number;
    rating?: number;
    status: number;
  }

  export interface ContractRequireSkillDetailModel {
    id: string;
    skillId: string;
    skillName: string;
    value: boolean;
  }

  export interface DetailServiceModel {
    id: string;
    name: string;
  }

  export interface ContractCreateResponse {
    success: boolean;
    errors?: any;
    data: string;
  }

  export interface ContractUpdateResponse {}

  export interface ContractDetailResponse {
    success: boolean;
    errors: any;
    data: DetailContractModel;
  }

  export interface CreateContractState {
    locations: any;
    skills: any;
    provinces: any;
    districts: any;
    wards: any;
    promotions: any;
    services: any;
    sources: any;
    departments: any;
  }
}
