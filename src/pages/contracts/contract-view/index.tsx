import React, { useState } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import { Button, Card, Tag } from 'antd';
import ProTable, { ProColumns } from '@ant-design/pro-table';
import { FormattedMessage, history, Link } from 'umi';
import { EyeOutlined, EditOutlined } from '@ant-design/icons';
import moment from 'moment';
import Field from '@ant-design/pro-field';
import { getContracts } from '../service';

const ContractView: React.FC<{}> = () => {
  // Columns
  const columns: ProColumns<CONTRACT.ContractItem>[] = [
    {
      title: <FormattedMessage id="pages.contracts.id" defaultMessage="Code" />,
      dataIndex: 'id',
      sorter: true,
      formItemProps: {
        name: 'search',
      },
      render: (_, record) => <Link to={`/contract-view/${record.id}`}>{record.id}</Link>,
    },
    {
      title: <FormattedMessage id="pages.contracts.customerName" defaultMessage="Customer" />,
      dataIndex: 'customerName',
      sorter: true,
      formItemProps: {
        name: 'search',
      },
      render: (_, record) => (
        <Link to={`/customer-view/${record.customerId}`}><strong>{record.customerName}</strong></Link>
      ),
    },
    {
      title: <FormattedMessage id="pages.contracts.statusValue" defaultMessage="Status" />,
      dataIndex: 'statusValue',
      formItemProps: {
        name: 'status',
      },
      filters: true,
      onFilter: true,
      valueType: 'select',
      valueEnum: {
        all: { text: 'Tất cả', status: '' },
        10: {
          text: 'Khởi Tạo',
          status: '10',
        },
        20: {
          text: 'Đang Hiệu Lực',
          status: '20',
        },
        100: {
          text: 'Hoàn Thành',
          status: '100',
        },
        99: {
          text: 'Đã Hủy',
          status: '99',
        },
        pend0ing: {
          text: 'Chờ Xác Nhận',
          status: '0',
          disabled: true,
        },
      },
      render: (_, record) => {
        if (record.status === 10) {
          return <Tag color="default">{record.statusValue}</Tag>;
        }

        if (record.status === 20) {
          return <Tag color="success">{record.statusValue}</Tag>;
        }

        if (record.status === 0) {
          return <Tag color="purple">{record.statusValue}</Tag>;
        }

        if (record.status === 100) {
          return <Tag color="cyan">{record.statusValue}</Tag>;
        }

        return <Tag color="error">{record.statusValue}</Tag>;
      },
    },
    {
      title: <FormattedMessage id="pages.contracts.contractSourceName" defaultMessage="Location" />,
      dataIndex: 'contractSourceName',
      hideInTable: true,
      search: false,
    },
    {
      title: <FormattedMessage id="pages.contracts.signingDate" defaultMessage="Signing Date" />,
      dataIndex: 'signingDate',
      search: false,
      render: (_, record) => {
        if (record.signingDate) {
          return moment(record.signingDate).format('L');
        }

        return 'N/A';
      },
    },
    {
      title: <FormattedMessage id="pages.contracts.totalAmount" defaultMessage="Payment" />,
      dataIndex: 'totalAmount',
      formItemProps: {
        name: 'paid',
      },
      filters: true,
      onFilter: true,
      valueType: 'select',
      valueEnum: {
        all: { text: 'Tất cả', status: '' },
        1: {
          text: 'Đã Có Phiếu Thu',
          paid: true,
        },
        0: {
          text: 'Chưa Có Phiếu Thu',
          paid: false,
        },
      },
      render: (_, record) => {
        if (record.totalAmount) {
          return <Field text={record.totalAmount} valueType="digit" mode="read" />;
        }

        return 'N/A';
      },
    },
    {
      title: <FormattedMessage id="pages.contracts.effectFrom" defaultMessage="Effect From" />,
      dataIndex: 'effectFrom',
      valueType: 'dateTime',
      formItemProps: {
        name: 'from',
      },
      render: (_, record) => moment(record.effectFrom).format('L'),
    },
    {
      title: <FormattedMessage id="pages.contracts.effectTo" defaultMessage="Effect To" />,
      dataIndex: 'effectTo',
      valueType: 'dateTime',
      formItemProps: {
        name: 'to',
      },
      render: (_, record) => moment(record.effectTo).format('L'),
    },
    {
      title: <FormattedMessage id="pages.contracts.paidAmount" defaultMessage="Paid" />,
      dataIndex: 'paidAmount',
      search: false,
      render: (_, record) => {
        if (record.paidAmount) {
          return <Field text={record.paidAmount} valueType="digit" mode="read" />;
        }

        return 'N/A';
      },
    },
    {
      title: <FormattedMessage id="pages.contracts.remainingAmount" defaultMessage="Remaining" />,
      dataIndex: 'remainingAmount',
      search: false,
      render: (_, record) => {
        if (record.remainingAmount) {
          return <Field text={record.remainingAmount} valueType="digit" mode="read" />;
        }

        return 'N/A';
      },
    },
    {
      title: 'Thao Tác',
      dataIndex: 'id',
      search: false,
      render: (_, record) => {
        return (
          <>
            <Button
              type="primary"
              color=""
              key="view-detail"
              onClick={() => history.push(`/contract-view/${record.id}`)}
              shape="circle"
              style={{ marginLeft: '10px' }}
            >
              <EyeOutlined />
            </Button>
          </>
        );
      },
    },
  ];

  // Multi lang
  // const intl = useIntl();
  const [contractList, setContractList] = useState<CONTRACT.ContractItem[]>([]);
  const [pagination, setPagination] = useState<API.PaginationInfo>();

  const onFetchContracts = async (params: any, sorter: any) => {
    const orderBy = Object.keys(sorter)[0];
    let desc = false;
    if (orderBy) {
      desc = sorter[Object.keys(sorter)[0]] === 'descend';
    }

    const queryParams = {
      page: params.current || 1,
      size: params.pageSize || 10,
      orderBy,
      desc,
      status: params.status ? Number(params.status) : null,
      paid: params.paid && params?.paid === '1',
      search: params?.search,
      from: params.from ? moment.utc(params.from).toDate() : null,
      to: params.to ? moment.utc(params.to).toDate() : null,
    };

    const response = await getContracts(queryParams);
    setContractList(response.data.items);
    setPagination({
      current: response.data.currentPage,
      total: response.data.total,
      pageSize: response.data.size,
      defaultPageSize: 10,
    });
    return {
      data: response.data.items,
      success: response.success,
    };
  };

  const onOpenCreateView = async () => {
    history.push('/contract-view/create');
  };

  return (
    <PageContainer>
      <Card>
        <ProTable<CONTRACT.ContractItem>
          // actionRef={actionRef}
          rowKey="id"
          // search={false}
          toolbar={{
            actions: [
              <Button
                type="primary"
                key="primary"
                onClick={() => {
                  onOpenCreateView();
                }}
              >
                <EditOutlined /> <FormattedMessage id="actions.create" defaultMessage="Create" />
              </Button>,
            ],
          }}
          request={(params, sorter) => onFetchContracts(params, sorter)}
          columns={columns}
          dateFormatter="string"
          dataSource={contractList}
          pagination={pagination}
        />
      </Card>
    </PageContainer>
  );
};

export default ContractView;
