import { apiConfig } from '@/configs/apis';
import moment from 'moment';
import request from '../../utils/request';

export async function getContracts(params: API.QueryParams) {
  return request<CONTRACT.ContractListResponse>(`${apiConfig.prefixUrl}/api/contracts`, { params });
}

export async function createContract(params: CONTRACT.CreateContractModel) {
  return request<CONTRACT.ContractCreateResponse>(`${apiConfig.prefixUrl}/api/contracts`, {
    method: 'POST',
    data: params,
  });
}

export async function getContract(id: string) {
  return request<CONTRACT.ContractDetailResponse>(`${apiConfig.prefixUrl}/api/contracts/${id}`);
}

export async function createContractService(params: any) {
  return request(`${apiConfig.prefixUrl}/api/contract-services`, {
    method: 'POST',
    data: params,
  });
}

export async function updateContractService(params: any) {
  return request(`${apiConfig.prefixUrl}/api/contract-services/${params.id}`, {
    method: 'PUT',
    data: params,
  });
}

export async function closeContract(id: any) {
  return request(`${apiConfig.prefixUrl}/api/contracts/${id}/complete`, {
    method: 'PUT',
    data: {
      status: 100,
    },
  });
}

export async function closeService(id: any) {
  return request(`${apiConfig.prefixUrl}/api/contract-services/${id}/complete`, {
    method: 'PUT',
  });
}

export async function cancelContract(id: any) {
  return request(`${apiConfig.prefixUrl}/api/contracts/${id}/cancel`, {
    method: 'PUT',
    data: {
      status: 99,
    },
  });
}

export async function getDepartmentSelectList(params?: any) {
  return request(`${apiConfig.prefixUrl}/api/select-list/location-departments`, { params });
}

export async function getCustomerContractForFeedback(params: any) {
  return request<any>(`${apiConfig.prefixUrl}/api/customer-feedbacks`, { params });
}

export async function createCustomerContractFeedback(params: any) {
  return request<any>(`${apiConfig.prefixUrl}/api/customer-feedbacks`, {
    method: 'POST',
    data: params,
  });
}

export default function parseCreateContractModel(values: any): CONTRACT.CreateContractModel {
  return {
    contractLocationId: values.contractLocationId,
    contractSourceId: values.contractSourceId,
    locationDepartmentId: values.locationDepartmentId,
    note: values.note,
    promotionId: values.promotionId,
    patient: {
      fullName: values.patientFullName,
      gender: values.patientGender,
      yearOfBirth: values.patientYearOfBirth,
      weight: values.patientWeight,
    },
    customer: {
      avatar: '',
      contactPhone: values.contactPhone,
      fullName: values.fullName,
      gender: Number(values.gender),
      note: values.note,
      districtId: values.districtId,
      provinceId: values.provinceId,
      wardId: values.wardId,
      street: values.street,
    },
    requiredSkills: values.skills.map((e: any) => ({
      id: e,
      value: true,
    })),
    services: [
      {
        from: moment.utc(values.fromTo[0]).toDate(),
        to: moment.utc(values.fromTo[1]).toDate(),
        locationId: values.contractLocationId,
        serviceId: values.serviceId,
        price: Number(values.servicePrice),
      },
    ],
  };
}
