import React, { useState, useEffect } from 'react';
import { message, Modal } from 'antd';
import { useIntl, FormattedMessage } from 'umi';
import ProForm, { ProFormDateRangePicker, ProFormDigit, ProFormSelect } from '@ant-design/pro-form';

import { getServiceSelectList } from '@/services/services';
import { getPatientCarerSelectList } from '@/pages/patient-carers/service';
import moment from 'moment';

interface CreateContractServiceFormProps {
  modalVisible: boolean;
  onCancel: () => void;
  serviceOptions: any;
  patientCarerOptions: any;
  onFinish: any;
}

const CreateServiceForm: React.FC<CreateContractServiceFormProps> = (props) => {
  const { modalVisible, onCancel, serviceOptions, onFinish, patientCarerOptions } = props;
  const intl = useIntl();
  const [customService, setCustomService] = useState(false);
  const [serviceSelectList, setServiceSelectList] = useState(serviceOptions);
  const [carerSelectList, setCarerSelectList] = useState(patientCarerOptions);

  const onServiceSelect = (value: string) => {
    const service = serviceSelectList.filter((e: any) => e.value === value)[0];
    if (service && service.extra.priceNegotiation) {
      setCustomService(true);
    } else {
      setCustomService(false);
    }
  };

  const onValuesChange = async (values: any) => {
    if (values.serviceTime && values.serviceTime.length === 2) {
      const carerResponse = await getPatientCarerSelectList({
        from: moment.utc(values.serviceTime[0]).toDate(),
        to: moment.utc(values.serviceTime[1]).toDate(),
      });

      setCarerSelectList(carerResponse.data);
    }
  };

  const onDisabledDate = (current: any) => {
    return current && current < moment().endOf('day');
  };

  useEffect(() => {
    async function fetchAsync() {
      if (serviceOptions.length === 0) {
        const serviceResponse = await getServiceSelectList();
        setServiceSelectList(serviceResponse.data);
      }
    }

    fetchAsync();
  }, []);
  return (
    <Modal
      destroyOnClose
      title={intl.formatMessage({
        id: 'pages.contracts.createService',
        defaultMessage: 'Create Contract Service',
      })}
      visible={modalVisible}
      onCancel={() => onCancel()}
      footer={null}
    >
      <ProForm
        onValuesChange={(changeValues) => onValuesChange(changeValues)}
        onFinish={async (values) => {
          const response = await onFinish(values);
          if (response.success) {
            message.success(
              intl.formatMessage({
                id: 'api.success',
                defaultMessage: 'Success',
              }),
            );

            setTimeout(() => {
              window.location.reload();
            }, 1000);
          }
        }}
      >
        <ProForm.Group>
          <ProFormSelect
            options={serviceSelectList}
            width="lg"
            name="serviceId"
            label={intl.formatMessage({
              id: 'pages.contracts.serviceId',
              defaultMessage: 'Service',
            })}
            fieldProps={{
              onSelect: onServiceSelect,
            }}
            rules={[
              {
                required: true,
                message: (
                  <FormattedMessage id="errors.required" defaultMessage="This field is required" />
                ),
              },
            ]}
          />
          <ProFormDigit
            name="price"
            label={intl.formatMessage({
              id: 'pages.contracts.servicePrice',
              defaultMessage: 'Service Price',
            })}
            width="lg"
            disabled={!customService}
          />
        </ProForm.Group>
        <ProForm.Group>
          <ProFormDateRangePicker
            name="serviceTime"
            label={intl.formatMessage({
              id: 'pages.contracts.serviceFromTo',
              defaultMessage: 'From - To',
            })}
            width="lg"
            rules={[
              {
                required: true,
                message: (
                  <FormattedMessage id="errors.required" defaultMessage="This field is required" />
                ),
              },
            ]}
            fieldProps={{
              disabledDate: (current) => onDisabledDate(current),
              showToday: false
            }}
          />
          <ProFormSelect
            options={carerSelectList}
            width="lg"
            name="patientCarerId"
            label={intl.formatMessage({
              id: 'pages.contracts.serviceCarer',
              defaultMessage: 'Patient Carer',
            })}
            rules={[
              {
                required: true,
                message: (
                  <FormattedMessage id="errors.required" defaultMessage="This field is required" />
                ),
              },
            ]}
          />
        </ProForm.Group>
      </ProForm>
    </Modal>
  );
};

export default CreateServiceForm;
