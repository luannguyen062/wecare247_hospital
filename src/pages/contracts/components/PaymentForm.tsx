import React, { useEffect } from 'react';
import ProForm, {
  ModalForm,
  ProFormText,
  ProFormSelect,
  ProFormDigit,
  ProFormTextArea,
} from '@ant-design/pro-form';
import { useIntl, FormattedMessage } from 'umi';
import { Form, message } from 'antd';

export interface PaymentFormProps {
  visible: boolean;
  onFinish: (values: any) => Promise<any>;
  onVisibleChange?: (visible: boolean) => void;
  currentPayment?: PAYMENT.ContractPaymentModel;
  contractCode: string;
}

const PaymentForm: React.FC<PaymentFormProps> = ({
  visible,
  onFinish,
  onVisibleChange,
  currentPayment,
  contractCode,
}) => {
  // Multi lang
  const intl = useIntl();
  const [form] = Form.useForm();

  useEffect(() => {
    if (form && visible) form.resetFields();
  }, [visible]);

  const updateMode = currentPayment != null;
  return (
    <ModalForm
      form={form}
      title={
        currentPayment == null
          ? intl.formatMessage({
              id: 'pages.payments.create.title',
              defaultMessage: 'Create New Payment',
            })
          : intl.formatMessage({
              id: 'pages.payments.update.title',
              defaultMessage: 'Update Payment',
            })
      }
      width="800px"
      visible={visible}
      onVisibleChange={onVisibleChange}
      onFinish={async (values: any) => {
        const response = await onFinish({
          ...values,
          id: currentPayment?.id,
        });
        if (response.success) {
          if (onVisibleChange) {
            onVisibleChange(false);
          }
          message.success(
            intl.formatMessage({
              id: 'api.success',
              defaultMessage: 'Success',
            }),
          );

          window.location.reload();
        }
      }}
    >
      <>
        <ProForm.Group>
          <ProFormText
            width="md"
            label={intl.formatMessage({
              id: 'pages.payments.contractCode',
              defaultMessage: 'Pay for Contract',
            })}
            rules={[
              {
                required: true,
                message: (
                  <FormattedMessage id="errors.required" defaultMessage="This field is required" />
                ),
              },
            ]}
            name="contractCode"
            initialValue={contractCode}
            disabled
          />
          <ProFormSelect
            width="md"
            name="type"
            label={intl.formatMessage({
              id: 'pages.payments.type',
              defaultMessage: 'Payment Type',
            })}
            options={[
              {
                value: 10,
                label: intl.formatMessage({
                  id: 'pages.payments.type.payForContract',
                  defaultMessage: 'Payment for Contract',
                }),
              },
              {
                value: 20,
                label: intl.formatMessage({
                  id: 'pages.payments.type.refund',
                  defaultMessage: 'Customer Refund',
                }),
              },
            ]}
            rules={[
              {
                required: true,
                message: intl.formatMessage({
                  id: 'errors.required',
                  defaultMessage: 'This field is required',
                }),
              },
            ]}
            // fieldProps={{
            //   defaultValue: currentPayment?.type,
            // }}
            initialValue={updateMode ? currentPayment?.type : undefined}
          />
          <ProFormDigit
            width="md"
            label={intl.formatMessage({
              id: 'pages.payments.amount',
              defaultMessage: 'Amount',
            })}
            rules={[
              {
                required: true,
                message: (
                  <FormattedMessage id="errors.required" defaultMessage="This field is required" />
                ),
              },
            ]}
            name="amount"
            initialValue={updateMode ? currentPayment?.amount : undefined}
          />
          <ProFormTextArea
            width="lg"
            name="note"
            label={intl.formatMessage({
              id: 'pages.payments.note',
              defaultMessage: 'Note',
            })}
            initialValue={updateMode ? currentPayment?.note : undefined}
          />
        </ProForm.Group>
      </>
    </ModalForm>
  );
};

export default PaymentForm;
