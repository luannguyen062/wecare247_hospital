import React, { useState, useEffect } from 'react';
import { message, Modal } from 'antd';
import { useIntl } from 'umi';
import ProForm, {
  ProFormDateRangePicker,
  ProFormDigit,
  ProFormSelect,
} from '@ant-design/pro-form';

import { getServiceSelectList } from '@/services/services';
import { getPatientCarerSelectList } from '@/pages/patient-carers/service';
import moment from 'moment';

interface UpdateContractServiceFormProps {
  modalVisible: boolean;
  onCancel: () => void;
  serviceOptions: any;
  patientCarerOptions: any;
  onFinish: any;
  currentService: CONTRACT.ContractServiceDetailModel;
}

const UpdateServiceForm: React.FC<UpdateContractServiceFormProps> = (props) => {
  const {
    modalVisible,
    onCancel,
    serviceOptions,
    onFinish,
    patientCarerOptions,
    currentService,
  } = props;

  const intl = useIntl();
  const [customService, setCustomService] = useState(false);
  const [serviceSelectList, setServiceSelectList] = useState(serviceOptions);
  const [carerSelectList, setCarerSelectList] = useState(patientCarerOptions);

  const onServiceSelect = (value: string) => {
    const service = serviceSelectList.filter((e: any) => e.value === value)[0];
    if (service && service.extra.priceNegotiation) {
      setCustomService(true);
    } else {
      setCustomService(false);
    }
  };

  const onValuesChange = async (values: any) => {
    if (values.serviceTime && values.serviceTime.length === 2) {
      const carerResponse = await getPatientCarerSelectList({
        from: moment.utc(values.serviceTime[0]).toDate(),
        to: moment.utc(values.serviceTime[1]).toDate(),
      });

      setCarerSelectList(carerResponse.data);
    }
  };

  useEffect(() => {
    async function fetchAsync() {
      if (currentService) {
        if (serviceOptions.length === 0) {
          const serviceResponse = await getServiceSelectList();
          setServiceSelectList(serviceResponse.data);
        }

        if (patientCarerOptions.length === 0) {
          const carerResponse = await getPatientCarerSelectList({
            from: moment.utc(currentService.from).toDate(),
            to: moment.utc(currentService.to).toDate(),
          });

          setCarerSelectList(carerResponse.data);
        }
      }
    }

    fetchAsync();
  }, [currentService]);
  return (
    <Modal
      destroyOnClose
      title={intl.formatMessage({
        id: 'pages.contracts.serviceUpdate',
        defaultMessage: 'Update Contract Service',
      })}
      visible={modalVisible}
      onCancel={() => onCancel()}
      footer={null}
    >
      {currentService && (
        <ProForm
          onValuesChange={(changeValues) => onValuesChange(changeValues)}
          onFinish={async (values) => {
            const response = await onFinish(values);
            if (response.success) {
              message.success(
                intl.formatMessage({
                  id: 'api.success',
                  defaultMessage: 'Success',
                }),
              );

              setTimeout(() => {
                window.location.reload();
              }, 1000);
            }
          }}
        >
          <ProForm.Group>
            <ProFormSelect
              options={serviceSelectList}
              width="lg"
              name="serviceId"
              label={intl.formatMessage({
                id: 'pages.contracts.serviceId',
                defaultMessage: 'Service',
              })}
              fieldProps={{
                onSelect: onServiceSelect,
              }}
              initialValue={currentService.serviceId}
            />
            <ProFormDigit
              name="price"
              label={intl.formatMessage({
                id: 'pages.contracts.servicePrice',
                defaultMessage: 'Service Price',
              })}
              width="lg"
              disabled={!customService}
              initialValue={currentService.totalPrice}
            />
          </ProForm.Group>
          <ProForm.Group>
            <ProFormDateRangePicker
              name="serviceTime"
              label={intl.formatMessage({
                id: 'pages.contracts.serviceFromTo',
                defaultMessage: 'From - To',
              })}
              width="lg"
              initialValue={[currentService.from, currentService.to]}
            />
            <ProFormSelect
              options={carerSelectList}
              width="lg"
              name="patientCarerId"
              label={intl.formatMessage({
                id: 'pages.contracts.servicePatientCarer',
                defaultMessage: 'Patient Carer',
              })}
              initialValue={currentService.patientCarerId}
            />
          </ProForm.Group>
        </ProForm>
      )}
    </Modal>
  );
};

export default UpdateServiceForm;
