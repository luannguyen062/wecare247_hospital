import React, { useEffect, useState } from 'react';
import ProForm, {
  ModalForm,
  ProFormText,
  ProFormSelect,
  ProFormDigit,
  ProFormTextArea,
} from '@ant-design/pro-form';
import { useIntl, FormattedMessage } from 'umi';
import { Form, message } from 'antd';
import { getContractCarerSelectList } from '@/pages/patient-carer-payments/service';

export interface CarerPaymentFormProps {
  contractCode: string;
  visible: boolean;
  currentPayment: PAYMENT.ContractPaymentModel;
  onFinish: (values: any) => Promise<PAYMENT.ContractPaymentListResponse>;
  onVisibleChange: (visible: boolean) => void;
}

const CarerPaymentForm: React.FC<CarerPaymentFormProps> = ({
  contractCode,
  visible,
  currentPayment,
  onFinish,
  onVisibleChange,
}) => {
  // Multi lang
  const intl = useIntl();
  const [form] = Form.useForm();
  const [patientCarers, setPatientCarers] = useState([]);

  useEffect(() => {
    if (form && visible) form.resetFields();
  }, [visible]);

  useEffect(() => {
    async function fetchAsync() {
      if (contractCode !== '') {
        const response = await getContractCarerSelectList({ contractCode });
        setPatientCarers(response.data);
      }
    }

    fetchAsync();
  }, [contractCode]);

  const updateMode = currentPayment != null;

  return (
    <ModalForm
      title={intl.formatMessage({
        id: 'pages.payments.create.title',
        defaultMessage: 'Patient Carer Payment',
      })}
      width="800px"
      visible={visible}
      onVisibleChange={onVisibleChange}
      onFinish={async (values: any) => {
        const response = await onFinish({
          ...values,
          id: currentPayment?.id,
        });
        if (response.success) {
          message.success(
            intl.formatMessage({
              id: 'api.success',
              defaultMessage: 'Success',
            }),
          );

          onVisibleChange(false);
          window.location.reload();
        }
      }}
      form={form}
    >
      <ProForm.Group>
        <ProFormText
          width="md"
          label={intl.formatMessage({
            id: 'pages.payments.contractCode',
            defaultMessage: 'Contract',
          })}
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage id="errors.required" defaultMessage="This field is required" />
              ),
            },
          ]}
          name="contractCode"
          initialValue={contractCode}
          disabled
        />
        <ProFormSelect
          width="md"
          name="patientCarerCode"
          label={intl.formatMessage({
            id: 'pages.payments.patientCarerCode',
            defaultMessage: 'Patient Carer',
          })}
          options={patientCarers}
          rules={[
            {
              required: true,
              message: intl.formatMessage({
                id: 'errors.required',
                defaultMessage: 'This field is required',
              }),
            },
          ]}
          initialValue={updateMode ? currentPayment?.patientCarerCode : undefined}
        />
        <ProFormDigit
          width="md"
          label={intl.formatMessage({
            id: 'pages.payments.amount',
            defaultMessage: 'Amount',
          })}
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage id="errors.required" defaultMessage="This field is required" />
              ),
            },
          ]}
          name="amount"
          initialValue={updateMode ? currentPayment?.amount : undefined}
        />
        <ProFormTextArea
          width="lg"
          name="note"
          label={intl.formatMessage({
            id: 'pages.payments.note',
            defaultMessage: 'Note',
          })}
          initialValue={updateMode ? currentPayment?.note : undefined}
        />
      </ProForm.Group>
    </ModalForm>
  );
};

export default CarerPaymentForm;
