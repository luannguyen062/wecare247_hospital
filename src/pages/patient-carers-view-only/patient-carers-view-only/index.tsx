/* eslint-disable @typescript-eslint/no-use-before-define */
import React, { useRef, useState, useEffect } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import { Card, Tag, Avatar, Divider } from 'antd';
import { useIntl, FormattedMessage, Link } from 'umi';
import ProTable, { ProColumns, ActionType } from '@ant-design/pro-table';
import {
  // LightFilter,
  // ProFormDatePicker,
  ProFormSelect,
  ProFormText,
  QueryFilter,
} from '@ant-design/pro-form';
import { getLevels } from '@/services/levels';
import { getLocations } from '@/services/locations';

import { PatientCarerItem } from '../data';
import {
  getPatientCarers,
} from '../service';

const PatientCarerViewOnly: React.FC<{}> = () => {
  // Columns
  const columns: ProColumns<PatientCarerItem>[] = [
    {
      title: <FormattedMessage id="pages.carers.id" defaultMessage="Patient Carer Code" />,
      dataIndex: 'id',
      sorter: true,
      render: (_, record) => <Link to={`/patient-carer-view/${record.id}`}>{record.id}</Link>,
    },
    {
      title: <FormattedMessage id="pages.carers.fullName" defaultMessage="Full Name" />,
      dataIndex: 'fullName',
      sorter: true,
      render: (_, record) => <Link to={`/patient-carer-view/${record.id}`}><strong>{record.fullName}</strong></Link>,
    },
    {
      title: <FormattedMessage id="pages.carers.cardNo" defaultMessage="Identity Card" />,
      dataIndex: 'cardNo',
      copyable: true
    },
    {
      title: <FormattedMessage id="pages.carers.contactPhone" defaultMessage="Contact Phone" />,
      dataIndex: 'contactPhone',
      copyable: true
    },
    {
      title: <FormattedMessage id="pages.carers.preferLocationId" defaultMessage="Work Location" />,
      dataIndex: 'preferLocationId',
      render: (_, record) => <Tag color="purple">{record.preferLocationName}</Tag>,
    },
    {
      title: <FormattedMessage id="pages.carers.levelId" defaultMessage="Level Code" />,
      dataIndex: 'levelId',
      sorter: true,
      render: (_, record) => <Tag color="green">{record.levelName}</Tag>,
    },
    {
      title: <FormattedMessage id="pages.carers.gender" defaultMessage="Gender" />,
      dataIndex: 'gender',
      render: (_, record) => {
        var gender;
        var color;
        switch(record.gender) {
          case 1:
            gender = intl.formatMessage({id: "male", defaultMessage: "Nam"});
            color = "blue";
            break;
          case 2:
            gender = intl.formatMessage({id: "female", defaultMessage: "Nữ"});
            color = "orange";
            break;
          default:
            gender = intl.formatMessage({id: "other", defaultMessage: "Khác"});
            color = "blue";
        }
        return <Tag color={color}>{gender}</Tag>;
      },
    },
    {
      title: <FormattedMessage id="pages.carers.avatar" defaultMessage="Avatar" />,
      dataIndex: 'avatar',
      render: (_, record) => {
        if (record.avatar) {
          return <Avatar src={record.avatar} />;
        }

        return 'N/A';
      },
    },
  ];

  // Multi lang
  const intl = useIntl();
  const [patientCarerList, setPatientCarerList] = useState<PatientCarerItem[]>([]);
  const [pagination, setPagination] = useState<API.PaginationInfo>();
  const [levelOptions, setLevelOptions] = useState<any>([]);
  const [locationOptions, setLocationOptions] = useState<any>([]);
  const actionRef = useRef<ActionType>();

  const onFetchPatientCarers = async (params: any, sorter: any, queryFilters?: any) => {
    const orderBy = Object.keys(sorter)[0];
    let desc = false;
    if (orderBy) {
      desc = sorter[Object.keys(sorter)[0]] === 'descend';
    }

    const queryParams = {
      page: params.current || 1,
      size: params.pageSize || 10,
      orderBy,
      desc,
      levelId: queryFilters?.levelId,
      preferLocationId: queryFilters?.preferLocationId,
      search: queryFilters?.search,
    };

    const response = await getPatientCarers(queryParams);
    setPatientCarerList(response.data.items);
    setPagination({
      current: response.data.currentPage,
      total: response.data.total,
      pageSize: response.data.size,
      defaultPageSize: 10,
    });

    return {
      data: response.data.items,
      success: response.success,
    };
  };

  useEffect(() => {
    async function callServicesAsync() {
      const levelResponse = await getLevels();
      setLevelOptions(
        levelResponse.data.map((e) => ({
          label: e.name,
          value: e.id,
        })),
      );

      const locationResponse = await getLocations();
      setLocationOptions(
        locationResponse.data.map((e) => ({
          label: e.name,
          value: e.id,
        })),
      );
    }

    callServicesAsync();
  }, []);

  return (
    <PageContainer>
      <Card>
        <div>
          <QueryFilter
            layout="vertical"
            onFinish={async (values) => {
              await onFetchPatientCarers({ current: 1, pageSize: 10 }, { id: 'descend' }, values);
            }}
          >
            <ProFormText
              name="search"
              label={intl.formatMessage({
                id: 'pages.carers.search',
                defaultMessage: 'Enter something to search ...',
              })}
              width="md"
            />
            <ProFormSelect
              name="levelId"
              label={intl.formatMessage({
                id: 'pages.carers.level',
                defaultMessage: 'Carer Levels',
              })}
              options={levelOptions}
              width="md"
            />
            <ProFormSelect
              name="preferLocationId"
              label={intl.formatMessage({
                id: 'pages.carers.preferLocation',
                defaultMessage: 'Prefer Locations',
              })}
              // mode="multiple"
              options={locationOptions}
            />
          </QueryFilter>
        </div>
        <Divider />
        <ProTable<PatientCarerItem>
          actionRef={actionRef}
          rowKey="id"
          search={false}
          request={(params, sorter) => onFetchPatientCarers(params, sorter)}
          columns={columns}
          dateFormatter="string"
          dataSource={patientCarerList}
          pagination={pagination}
          // options={{
          //   search: {
          //     name: 'search',
          //   },
          // }}
        />
        {/* <InformationDrawer
          visible={drawerVisible}
          onClose={onDrawerClose}
          currentCarer={currentUpdatedCarer}
          key={currentUpdatedCarer?.id}
        /> */}
      </Card>
    </PageContainer>
  );
};

export default PatientCarerViewOnly;
