export interface PatientCarerItem {
  id: string;
  fullName: string;
  birthday: Date;
  cardNo: string;
  cardDate: Date;
  cardIssuer: string;
  signingDate: Date;
  contactPhone: string;
  contactAddress: string;
  permanentAddress: string;
  preferLocationId: string;
  preferLocationName: string;
  avatar: string;
  levelId: string;
  levelName: string;
  gender: number;
}

export interface PatientCarerListResponse {
  success: boolean;
  errors: any;
  data: API.PaginationData<PatientCarerItem>;
}

export interface PatientCarerDetailResponse {
  success: boolean;
  errors: any;
  data: PatientCarerDetailModel;
}

export interface PatientCarerCreateResponse {
  success: boolean;
  errors: any;
  data: string;
}

export interface PatientCarerUpdateResponse {
  success: boolean;
  errors: any;
  data: string;
}

export interface PersonalSkill {
  id: string;
  value: boolean;
  skillId: string;
  name: string;
}

export interface Level {
  id: string;
  name: string;
}

export interface PreferLocation {
  id: string;
  name: string;
  description: string;
  streetAddress: string;
}

export interface PatientCarerModel {
  id: string;
  fullName: string;
  birthday: Date;
  cardNo: string;
  cardDate: Date;
  cardIssuer: string;
  signingDate: Date;
  contactPhone: string;
  contactAddress: string;
  permanentAddress: string;
  preferLocation: PreferLocation;
  avatar: string;
  level: Level;
  gender: number;
  personalSkills: PersonalSkill[];
}

export interface PatientCarerDetailModel {
  id: string;
  fullName: string;
  birthday: Date;
  cardNo: string;
  cardDate: Date;
  cardIssuer: string;
  signingDate: Date;
  contactPhone: string;
  contactAddress: string;
  permanentAddress: string;
  preferLocation: PreferLocation;
  avatar: string;
  level: Level;
  gender: number;
  personalSkills: PersonalSkill[];
  contracts?: any;
  contractServices?: any;
  contractSchedules?: any;
}
