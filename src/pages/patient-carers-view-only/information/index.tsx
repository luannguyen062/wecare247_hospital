import React, { useEffect, useState } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import { Card, Divider, Rate, Space, Tag } from 'antd';
import ProCard from '@ant-design/pro-card';
import { useIntl, history, FormattedMessage, Link } from 'umi';
import ContractListDetail from '@/components/WeCare247/ContractListDetail';
import { ProColumns } from '@ant-design/pro-table';
import moment from 'moment';
import { statusEnum, statusMap } from '@/services/constants';
import ProDescriptions from '@ant-design/pro-descriptions';
import { CheckCircleOutlined, HeartFilled } from '@ant-design/icons';
import PaymentListDetail from '../components/PaymentListDetail';
import { getPatientCarer } from '../service';

const statusRender = (status: any, statusValue: any) => {
  if (status === 10) {
    return <Tag color="default">{statusValue || statusMap[status]}</Tag>;
  }

  if (status === 20) {
    return <Tag color="success">{statusValue || statusMap[status]}</Tag>;
  }

  if (status === 0) {
    return <Tag color="warning">{statusValue || statusMap[status]}</Tag>;
  }

  if (status === 100) {
    return <Tag color="cyan">{statusValue || statusMap[status]}</Tag>;
  }

  return <Tag color="error">{statusValue || statusMap[status]}</Tag>;
};

const contractColumns: ProColumns<any>[] = [
  {
    title: <FormattedMessage id="pages.contracts.id" defaultMessage="Code" />,
    dataIndex: 'id',
    sorter: true,
    search: false,
    render: (_, record) => <Link to={`/contract-view/${record.id}`}>{record.id}</Link>,
  },
  {
    title: <FormattedMessage id="pages.contracts.customerName" defaultMessage="Customer" />,
    dataIndex: 'customerName',
    sorter: true,
    search: false,
    render: (_, record) => (
      <Link to={`/customer-view/${record.customerId}`}>
        <strong>{record.customerName}</strong>
      </Link>
    ),
  },
  {
    title: <FormattedMessage id="pages.contracts.statusValue" defaultMessage="Status" />,
    dataIndex: 'status',
    formItemProps: {
      name: 'status',
    },
    filters: true,
    onFilter: true,
    valueType: 'select',
    valueEnum: statusEnum,
    render: (_, record) => statusRender(record.status, record.statusValue),
  },
  {
    title: <FormattedMessage id="pages.contracts.contractSourceName" defaultMessage="Location" />,
    dataIndex: 'contractSourceName',
    hideInTable: true,
    search: false,
  },
  {
    title: <FormattedMessage id="pages.contracts.signingDate" defaultMessage="Signing Date" />,
    dataIndex: 'signingDate',
    search: false,
    sorter: true,
    render: (_, record) => {
      if (record.signingDate) {
        return moment(record.signingDate).format('L');
      }

      return 'N/A';
    },
  },
  {
    title: <FormattedMessage id="pages.contracts.effectFrom" defaultMessage="Effect From" />,
    dataIndex: 'effectFrom',
    valueType: 'dateTime',
    formItemProps: {
      name: 'from',
    },
    render: (_, record) => moment(record.effectFrom).format('L'),
  },
  {
    title: <FormattedMessage id="pages.contracts.effectTo" defaultMessage="Effect To" />,
    dataIndex: 'effectTo',
    valueType: 'dateTime',
    formItemProps: {
      name: 'to',
    },
    render: (_, record) => moment(record.effectTo).format('L'),
  },
];

const serviceColumns: ProColumns<any>[] = [
  {
    dataIndex: 'id',
    hideInTable: true,
  },
  {
    title: <FormattedMessage id="pages.contracts.serviceName" defaultMessage="Service Name" />,
    dataIndex: 'serviceId',
    render: (_, innerRecord) => innerRecord.service.name,
  },
  {
    title: <FormattedMessage id="pages.contracts.serviceFrom" defaultMessage="From" />,
    dataIndex: 'from',
    render: (_, innerRecord) => moment(innerRecord.from).format('L'),
  },
  {
    title: <FormattedMessage id="pages.contracts.serviceTo" defaultMessage="To" />,
    dataIndex: 'to',
    render: (_, innerRecord) => moment(innerRecord.to).format('L'),
  },
  {
    title: <FormattedMessage id="pages.contracts.serviceHours" defaultMessage="Actual Hours" />,
    dataIndex: 'actualHours',
    valueType: 'digit',
  },
  {
    title: <FormattedMessage id="pages.contracts.servicePrice" defaultMessage="Price" />,
    dataIndex: 'totalPrice',
    valueType: 'digit',
  },
  {
    title: <FormattedMessage id="pages.contracts.serviceStatus" defaultMessage="Status" />,
    dataIndex: 'status',
    formItemProps: {
      name: 'status',
    },
    filters: true,
    onFilter: true,
    valueType: 'select',
    valueEnum: statusEnum,
    render: (_, record) => statusRender(record.status, record.statusValue),
  },
  {
    title: <FormattedMessage id="pages.contracts.serviceRating" defaultMessage="Rating" />,
    dataIndex: 'rating',
    render: (_, innerRecord) => {
      if (!innerRecord.rating) {
        return (
          <FormattedMessage
            id="pages.contracts.serviceRating.NotYet"
            defaultMessage="Not Rated Yet"
          />
        );
      }

      return (
        <Rate disabled character={<HeartFilled />} allowHalf defaultValue={innerRecord.rating} />
      );
    },
  },
];

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const PatientCarerInformationView: React.FC<{}> = (props: any) => {
  const intl = useIntl();
  const [tab, setTab] = useState('info');
  const [patientCarer, setPatientCarer] = useState<any>(undefined);

  useEffect(() => {
    const patientCarerId = props.match.params.id;
    const activeTab = props.location.query.tab;
    setTab(activeTab || 'info');

    async function fetchAsync() {
      const response = await getPatientCarer(patientCarerId);
      setPatientCarer(response.data);
    }

    fetchAsync();
  }, []);

  return (
    <PageContainer>
      <Card loading={!patientCarer}>
        {patientCarer && (
          <div>
            <Space style={{ marginBottom: 16 }} />
            <ProCard
              tabs={{
                tabPosition: 'top',
                type: 'card',
                activeKey: tab,
                onChange: (key) => {
                  history.push({
                    pathname: history.location.pathname,
                    query: {
                      tab: key,
                    },
                  });

                  setTab(key);
                },
              }}
            >
              <ProCard.TabPane
                key="info"
                tab={intl.formatMessage({
                  id: 'pages.carers.detail',
                  defaultMessage: 'Personal Information',
                })}
              >
                <ProDescriptions column={2} style={{ fontSize: '15px', fontWeight: 'bold' }}>
                  <ProDescriptions.Item label="Họ Tên" valueType="text">
                    {patientCarer.fullName}
                  </ProDescriptions.Item>
                  <ProDescriptions.Item label="Ngày Sinh" valueType="text">
                    {moment(patientCarer.birthday).format('L')}
                  </ProDescriptions.Item>
                  <ProDescriptions.Item label="Giới Tính" valueType="text">
                    {patientCarer.gender === 1 ? 'Nam' : 'Nữ'}
                  </ProDescriptions.Item>
                  <ProDescriptions.Item label="Ngày Cấp" valueType="text">
                    {moment(patientCarer.cardDate).format('L')}
                  </ProDescriptions.Item>
                  <ProDescriptions.Item label="Nơi Cấp" valueType="text">
                    {patientCarer.cardIssuer}
                  </ProDescriptions.Item>
                  <ProDescriptions.Item label="CMND/CCCD" valueType="text">
                    {patientCarer.cardNo}
                  </ProDescriptions.Item>
                </ProDescriptions>
                <Divider />
                <ProDescriptions column={2} style={{ fontSize: '15px', fontWeight: 'bold' }}>
                  <ProDescriptions.Item label="SĐT Liên Hệ" valueType="text">
                    {patientCarer.contactPhone}
                  </ProDescriptions.Item>
                  <ProDescriptions.Item label="Trình Độ/Cấp Bậc" valueType="text">
                    {patientCarer.level.name}
                  </ProDescriptions.Item>
                  <ProDescriptions.Item label="Địa Chỉ" valueType="text">
                    {patientCarer.contactAddress}
                  </ProDescriptions.Item>
                  <ProDescriptions.Item label="Thường Trú" valueType="text">
                    {patientCarer.permanentAddress}
                  </ProDescriptions.Item>
                  <ProDescriptions.Item label="Nơi Làm Việc" valueType="text">
                    {patientCarer.preferLocation.name}
                  </ProDescriptions.Item>
                </ProDescriptions>
                <Divider />
                <>
                  {patientCarer.personalSkills.map((e: any) => (
                    <Tag
                      icon={<CheckCircleOutlined />}
                      color="success"
                      style={{ marginBottom: '10px', fontSize: '14px' }}
                    >
                      {e.name}
                    </Tag>
                  ))}
                </>
              </ProCard.TabPane>
              <ProCard.TabPane
                key="contracts"
                tab={intl.formatMessage({
                  id: 'pages.carers.contracts',
                  defaultMessage: 'Contracts',
                })}
              >
                <ContractListDetail
                  headerTitle={intl.formatMessage({
                    id: 'pages.customers.detail.services.title',
                    defaultMessage: 'Services',
                  })}
                  contracts={patientCarer.contracts || []}
                  services={patientCarer.contractServices}
                  dateFormatter="string"
                  displayContractSummary
                  contractColumns={contractColumns}
                  serviceColumns={serviceColumns}
                />
              </ProCard.TabPane>
              <ProCard.TabPane
                key="payments"
                tab={intl.formatMessage({
                  id: 'pages.carers.payments',
                  defaultMessage: 'Payments',
                })}
              >
                <PaymentListDetail
                  data={patientCarer.payments}
                  services={patientCarer.contractServices}
                />
              </ProCard.TabPane>
              <ProCard.TabPane
                key="feedbacks"
                tab={intl.formatMessage({
                  id: 'pages.carers.feedbacks',
                  defaultMessage: 'Feedbacks',
                })}
              >
                {intl.formatMessage({
                  id: 'actions.comingSoon',
                  defaultMessage: 'Coming soon',
                })}
              </ProCard.TabPane>
              <ProCard.TabPane
                key="training"
                tab={intl.formatMessage({
                  id: 'pages.carers.training',
                  defaultMessage: 'Training',
                })}
              >
                {intl.formatMessage({
                  id: 'actions.comingSoon',
                  defaultMessage: 'Coming soon',
                })}
              </ProCard.TabPane>
            </ProCard>
          </div>
        )}
      </Card>
    </PageContainer>
  );
};

export default PatientCarerInformationView;
