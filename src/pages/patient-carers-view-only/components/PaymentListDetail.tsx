import ProCard from '@ant-design/pro-card';
import ProTable, { ProColumns } from '@ant-design/pro-table';
import { Space, Tag } from 'antd';
import moment from 'moment';
import React from 'react';
import NumberFormat from 'react-number-format';
import { FormattedMessage, Link, useIntl } from 'umi';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
type PaymentListDetailProps = {
  data: any;
  services: any;
};

const PaymentListDetail: React.FC<PaymentListDetailProps> = (props) => {
  const { data, services } = props;
  const intl = useIntl();

  const sum = (array: any, key: string, percent?: number) => {
    let sumValue = array.reduce((a: any, b: any) => a + (b[key] || 0), 0);
    if (percent) {
      sumValue = (sumValue * percent) / 100;
    }

    return <NumberFormat value={sumValue} displayType="text" thousandSeparator="," suffix=" VND" />;
  };

  const sumCurrentMonth = () => {
    const startOfMonth = moment().clone().startOf('month');
    const matchedData = data.filter((e: any) => moment(e.created) > startOfMonth);
    return sum(matchedData, 'amount');
  };

  const columns: ProColumns<PAYMENT.ContractPaymentItem>[] = [
    {
      title: <FormattedMessage id="pages.payments.id" defaultMessage="Payment Ref" />,
      dataIndex: 'id',
      search: false,
      hideInTable: true,
    },
    {
      title: <FormattedMessage id="pages.payments.contractCode" defaultMessage="Contract" />,
      dataIndex: 'contractCode',
      formItemProps: {
        name: 'search',
        rules: [
          {
            required: true,
            message: 'Vui lỏng điền mã HĐDV',
          },
        ],
      },
      sorter: (a, b) => a.contractCode.length - b.contractCode.length,
      render: (_, record) => (
        <Link to={`/contract-view/${record.contractCode}`}>{record.contractCode}</Link>
      ),
    },
    {
      title: <FormattedMessage id="pages.payments.type" defaultMessage="Type" />,
      dataIndex: 'type',
      sorter: true,
      render: () => {
        return (
          <Tag color="green">
            <FormattedMessage
              id="pages.payments.type.payForCarer"
              defaultMessage="Pay for Patient Carer"
            />
          </Tag>
        );
      },
    },
    {
      title: <FormattedMessage id="pages.payments.amount" defaultMessage="Amount" />,
      dataIndex: 'amount',
      search: false,
      valueType: 'digit',
      sorter: (a, b) => a.amount - b.amount,
    },
    {
      title: <FormattedMessage id="pages.payments.note" defaultMessage="Note" />,
      dataIndex: 'note',
      search: false,
    },
    {
      title: <FormattedMessage id="pages.payments.created" defaultMessage="Date" />,
      dataIndex: 'created',
      render: (_, record) => moment(record.created).format('DD/MM/YYYY LT A'),
      search: false,
      sorter: (a, b) => {
        if(a.created > b.created) {
          return 0;
        }

        return -1;
      },
    },
    {
      title: <FormattedMessage id="pages.payments.createdBy" defaultMessage="Employee" />,
      dataIndex: 'createdBy',
      search: false,
    },
  ];

  return (
    <>
      <ProCard>
        <ProCard split="horizontal">
          <ProCard split="horizontal">
            <ProCard split="vertical">
              <ProCard
                title={intl.formatMessage({
                  id: 'pages.carers.contracts.totalServices',
                  defaultMessage: 'Total Services',
                })}
              >
                {services.length}
              </ProCard>
              <ProCard
                title={intl.formatMessage({
                  id: 'pages.carers.payments.paidAmount',
                  defaultMessage: 'Paid Amount',
                })}
              >
                {sum(data, 'amount')}
              </ProCard>
              <ProCard
                title={intl.formatMessage({
                  id: 'pages.carers.payments.estimateAmount',
                  defaultMessage: 'Services Amount',
                })}
              >
                {sum(services, 'totalPrice', 85)}
              </ProCard>
              <ProCard
                title={intl.formatMessage({
                  id: 'pages.carers.payments.currentMonthEstimateAmount',
                  defaultMessage: 'Current Month',
                })}
              >
                {sumCurrentMonth()}
              </ProCard>
            </ProCard>
          </ProCard>
        </ProCard>
      </ProCard>
      <Space style={{ marginBottom: 16 }} />
      <ProTable<any>
        columns={columns}
        request={(params, sorter, filter) => {
          // eslint-disable-next-line no-console
          console.log(params, sorter, filter);
          return Promise.resolve({
            data,
            success: true,
          });
        }}
        rowKey="id"
        search={false}
        pagination={{
          pageSize: 5,
        }}
        dateFormatter="string"
        options={false}
      />
    </>
  );
};

export default PaymentListDetail;
