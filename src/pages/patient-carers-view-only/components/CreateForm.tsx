import React from 'react';
import ProForm, {
  ModalForm,
  ProFormText,
  ProFormDatePicker,
  // ProFormUploadButton,
  ProFormSelect,
  // ProFormUploadButton,
} from '@ant-design/pro-form';
import { useIntl, FormattedMessage } from 'umi';
import { message } from 'antd';
import { PatientCarerCreateResponse } from '../data';

export interface CreateFormProps {
  visible: boolean;
  onFinish: (values: any) => Promise<PatientCarerCreateResponse>;
  onVisibleChange?: (visible: boolean) => void;
  handleCreateModalVisible: (visible: boolean) => void;
  skillOptions: any;
  levelOptions: any;
  locationOptions: any;
  actionRef: any;
}

const CreateForm: React.FC<CreateFormProps> = ({
  visible,
  onFinish,
  onVisibleChange,
  skillOptions,
  levelOptions,
  locationOptions,
  actionRef,
  handleCreateModalVisible
}) => {
  // Multi lang
  const intl = useIntl();
  return (
    <ModalForm
      title={intl.formatMessage({
        id: 'pages.carers.create.title',
        defaultMessage: 'Create New Carer',
      })}
      width="800px"
      visible={visible}
      onVisibleChange={onVisibleChange}
      onFinish={async (values: any) => {
        const response = await onFinish(values);
        if (response.success) {
          handleCreateModalVisible(false);
          if (actionRef.current) {
            actionRef.current.reload();
          }
          message.success(
            intl.formatMessage({
              id: 'api.create.success',
              defaultMessage: 'Success',
            }),
          );
        }
      }}
    >
      <ProForm.Group
        title={intl.formatMessage({
          id: 'pages.carers.info',
          defaultMessage: 'Personal Information',
        })}
      >
        <ProFormText
          width="md"
          label={intl.formatMessage({
            id: 'pages.carers.fullName',
            defaultMessage: 'Full Name',
          })}
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage id="errors.required" defaultMessage="This field is required" />
              ),
            },
          ]}
          name="fullName"
        />
        <ProFormDatePicker
          width="md"
          name="birthday"
          label={intl.formatMessage({
            id: 'pages.carers.birthday',
            defaultMessage: 'Birthday',
          })}
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage id="errors.required" defaultMessage="This field is required" />
              ),
            },
          ]}
        />
        <ProFormText
          width="md"
          label={intl.formatMessage({
            id: 'pages.carers.cardNo',
            defaultMessage: 'CCCD/CMND',
          })}
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage id="errors.required" defaultMessage="This field is required" />
              ),
            },
          ]}
          name="cardNo"
        />
        <ProFormText
          width="md"
          label={intl.formatMessage({
            id: 'pages.carers.cardIssuer',
            defaultMessage: 'Nơi Cấp',
          })}
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage id="errors.required" defaultMessage="This field is required" />
              ),
            },
          ]}
          name="cardIssuer"
        />
        <ProFormDatePicker
          width="md"
          name="cardDate"
          label={intl.formatMessage({
            id: 'pages.carers.cardDate',
            defaultMessage: 'Ngày Cấp',
          })}
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage id="errors.required" defaultMessage="This field is required" />
              ),
            },
          ]}
        />
        <ProFormSelect
          width="md"
          name="gender"
          label={intl.formatMessage({
            id: 'pages.carers.gender',
            defaultMessage: 'Giới Tính',
          })}
          valueEnum={{
            1: intl.formatMessage({
              id: 'pages.carers.gender.male',
              defaultMessage: 'Male',
            }),
            2: intl.formatMessage({
              id: 'pages.carers.gender.female',
              defaultMessage: 'Female',
            }),
          }}
          rules={[
            {
              required: true,
              message: intl.formatMessage({
                id: 'errors.required',
                defaultMessage: 'This field is required',
              }),
            },
          ]}
        />
        {/* <ProFormUploadButton name="avatar" label="Ảnh cá nhân" max={1} action="/api/files/aws-s3" title="Chọn hình ảnh" /> */}
      </ProForm.Group>
      <ProForm.Group
        title={intl.formatMessage({
          id: 'pages.carers.contactInfo',
          defaultMessage: 'Contract Information',
        })}
      >
        <ProFormText
          width="md"
          label={intl.formatMessage({
            id: 'pages.carers.contactPhone',
            defaultMessage: 'Contact Phone',
          })}
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage id="errors.required" defaultMessage="This field is required" />
              ),
            },
          ]}
          name="contactPhone"
        />
        <ProFormText
          width="md"
          label={intl.formatMessage({
            id: 'pages.carers.permanentAddress',
            defaultMessage: 'Thường Trú',
          })}
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage id="errors.required" defaultMessage="This field is required" />
              ),
            },
          ]}
          name="permanentAddress"
        />
        <ProFormText
          width="md"
          label={intl.formatMessage({
            id: 'pages.carers.contactAddress',
            defaultMessage: 'Nơi Ở Hiện Tại',
          })}
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage id="errors.required" defaultMessage="This field is required" />
              ),
            },
          ]}
          name="contactAddress"
        />
      </ProForm.Group>
      <ProForm.Group
        title={intl.formatMessage({
          id: 'pages.carers.skillInfo',
          defaultMessage: 'Work Skills',
        })}
      >
        <ProFormSelect
          width="md"
          name="levelId"
          label={intl.formatMessage({
            id: 'pages.carers.levelId',
            defaultMessage: 'Current Level',
          })}
          options={levelOptions}
          rules={[
            {
              required: true,
              message: intl.formatMessage({
                id: 'errors.required',
                defaultMessage: 'This field is required',
              }),
            },
          ]}
        />
        <ProFormSelect
          width="md"
          name="preferLocationId"
          label={intl.formatMessage({
            id: 'pages.carers.preferLocationId',
            defaultMessage: 'Prefer Location',
          })}
          options={locationOptions}
          rules={[
            {
              required: true,
              message: intl.formatMessage({
                id: 'errors.required',
                defaultMessage: 'This field is required',
              }),
            },
          ]}
        />
        <ProFormSelect
          width="xl"
          name="skills"
          label={intl.formatMessage({
            id: 'pages.carers.skills',
            defaultMessage: 'Work Skills',
          })}
          options={skillOptions}
          mode="tags"
          rules={[
            {
              required: true,
              message: intl.formatMessage({
                id: 'errors.required',
                defaultMessage: 'This field is required',
              }),
            },
          ]}
        />
      </ProForm.Group>
    </ModalForm>
  );
};

export default CreateForm;
