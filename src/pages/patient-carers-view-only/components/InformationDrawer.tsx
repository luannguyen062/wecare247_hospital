import React from 'react';
// import { useIntl, FormattedMessage } from 'umi';
import { Divider, Drawer, Skeleton, Tag } from 'antd';
import ProCard from '@ant-design/pro-card';
import moment from 'moment';
import ProDescriptions from '@ant-design/pro-descriptions';
import { CheckCircleOutlined } from '@ant-design/icons';
import { PatientCarerModel } from '../data';

export interface InformationDrawerProps {
  visible: boolean;
  onClose: () => void;
  currentCarer: PatientCarerModel | undefined;
}

const InformationDrawer: React.FC<InformationDrawerProps> = ({
  visible,
  onClose,
  currentCarer,
}) => {
  // Multi lang
  // const intl = useIntl();
  return (
    <Drawer width={720} placement="right" closable={false} onClose={onClose} visible={visible}>
      {!currentCarer ? (
        <Skeleton />
      ) : (
        <>
          <ProCard
            tabs={{
              type: 'card',
            }}
          >
            <ProCard.TabPane key="info" tab="Thông Tin Cá Nhân">
              <ProDescriptions column={2} style={{ fontSize: '15px', fontWeight: 'bold' }}>
                <ProDescriptions.Item label="Họ Tên" valueType="text">
                  {currentCarer.fullName}
                </ProDescriptions.Item>
                <ProDescriptions.Item label="Ngày Sinh" valueType="text">
                  {moment(currentCarer.birthday).format('L')}
                </ProDescriptions.Item>
                <ProDescriptions.Item label="Giới Tính" valueType="text">
                  {currentCarer.gender === 1 ? 'Name' : 'Nữ'}
                </ProDescriptions.Item>
                <ProDescriptions.Item label="Ngày Cấp" valueType="text">
                  {moment(currentCarer.cardDate).format('L')}
                </ProDescriptions.Item>
                <ProDescriptions.Item label="Nơi Cấp" valueType="text">
                  {currentCarer.cardIssuer}
                </ProDescriptions.Item>
                <ProDescriptions.Item label="CMND/CCCD" valueType="text">
                  {currentCarer.cardNo}
                </ProDescriptions.Item>
              </ProDescriptions>
              <Divider />
              <ProDescriptions column={2} style={{ fontSize: '15px', fontWeight: 'bold' }}>
                <ProDescriptions.Item label="SĐT Liên Hệ" valueType="text">
                  {currentCarer.contactPhone}
                </ProDescriptions.Item>
                <ProDescriptions.Item label="Trình Độ/Cấp Bậc" valueType="text">
                  {currentCarer.level.name}
                </ProDescriptions.Item>
                <ProDescriptions.Item label="Địa Chỉ" valueType="text">
                  {currentCarer.contactAddress}
                </ProDescriptions.Item>
                <ProDescriptions.Item label="Thường Trú" valueType="text">
                  {currentCarer.permanentAddress}
                </ProDescriptions.Item>
                <ProDescriptions.Item label="Nơi Làm Việc" valueType="text">
                  {currentCarer.preferLocation.name}
                </ProDescriptions.Item>
              </ProDescriptions>
            </ProCard.TabPane>
            <ProCard.TabPane key="skills" tab="Thông Tin Kỹ Năng">
              {currentCarer.personalSkills.map((e) => (
                <Tag
                  icon={<CheckCircleOutlined />}
                  color="success"
                  style={{ marginBottom: '10px', fontSize: '16px' }}
                >
                  {e.name}
                </Tag>
              ))}
            </ProCard.TabPane>
          </ProCard>
        </>
      )}
    </Drawer>
  );
};

export default InformationDrawer;
