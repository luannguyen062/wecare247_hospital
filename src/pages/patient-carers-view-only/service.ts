import { apiConfig } from '@/configs/apis';
import request from '../../utils/request';
import type {
  PatientCarerCreateResponse,
  PatientCarerDetailResponse,
  PatientCarerListResponse,
} from './data';

export async function getPatientCarers(params: API.QueryParams) {
  return request<PatientCarerListResponse>(`${apiConfig.prefixUrl}/api/patient-carers`, { params });
}

export async function getPatientCarer(id: string) {
  return request<PatientCarerDetailResponse>(`${apiConfig.prefixUrl}/api/patient-carers/${id}`);
}

export async function createPatientCarer(params: any) {
  return request<PatientCarerCreateResponse>(`${apiConfig.prefixUrl}/api/patient-carers`, {
    method: 'POST',
    data: params,
  });
}

export async function updatePatientCarer(params: any) {
  return request<PatientCarerCreateResponse>(
    `${apiConfig.prefixUrl}/api/patient-carers/${params.id}`,
    {
      method: 'PUT',
      data: params,
    },
  );
}

export async function getPatientCarerSelectList(params: any) {
  return request(`${apiConfig.prefixUrl}/api/select-list/patient-carers`, { params });
}
