/* eslint-disable @typescript-eslint/no-use-before-define */
import React, { useRef, useState, useEffect } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import { Card, Button, Tag, Avatar, Divider } from 'antd';
import { PlusOutlined, EditOutlined, EyeOutlined } from '@ant-design/icons';
import { useIntl, FormattedMessage, Link } from 'umi';
import ProTable, { ProColumns, ActionType } from '@ant-design/pro-table';
import {
  // LightFilter,
  // ProFormDatePicker,
  ProFormSelect,
  ProFormText,
  QueryFilter,
} from '@ant-design/pro-form';
import { getLevels } from '@/services/levels';
import { getLocations } from '@/services/locations';
import moment from 'moment';

import { PatientCarerItem, PatientCarerModel } from '../data.d';
import {
  getPatientCarers,
  getPatientCarer,
  createPatientCarer,
  updatePatientCarer,
} from '../service';
import { getRequireSkills } from '../../../services/skills';
import CreateForm from '../components/CreateForm';
// import ModalForm from '../create';
import UpdateForm from '../components/UpdateForm';
import InformationDrawer from '../components/InformationDrawer';

const PatientCarerView: React.FC<{}> = () => {
  // Columns
  const columns: ProColumns<PatientCarerItem>[] = [
    {
      title: <FormattedMessage id="pages.carers.id" defaultMessage="Patient Carer Code" />,
      dataIndex: 'id',
      sorter: true,
      render: (_, record) => <Link to={`/patient-carer-view/${record.id}`}>{record.id}</Link>,
    },
    {
      title: <FormattedMessage id="pages.carers.fullName" defaultMessage="Full Name" />,
      dataIndex: 'fullName',
      sorter: true,
      render: (_, record) => <Link to={`/patient-carer-view/${record.id}`}><strong>{record.fullName}</strong></Link>,
    },
    {
      title: <FormattedMessage id="pages.carers.cardNo" defaultMessage="Identity Card" />,
      dataIndex: 'cardNo',
      copyable: true
    },
    {
      title: <FormattedMessage id="pages.carers.contactPhone" defaultMessage="Contact Phone" />,
      dataIndex: 'contactPhone',
      copyable: true
    },
    {
      title: <FormattedMessage id="pages.carers.preferLocationId" defaultMessage="Work Location" />,
      dataIndex: 'preferLocationId',
      render: (_, record) => <Tag color="purple">{record.preferLocationName}</Tag>,
    },
    {
      title: <FormattedMessage id="pages.carers.levelId" defaultMessage="Level Code" />,
      dataIndex: 'levelId',
      sorter: true,
      render: (_, record) => <Tag color="green">{record.levelName}</Tag>,
    },
    {
      title: <FormattedMessage id="pages.carers.gender" defaultMessage="Gender" />,
      dataIndex: 'gender',
      render: (_, record) => {
        if (record.gender === 1) {
          return <Tag color="blue">Name</Tag>;
        }
        if (record.gender === 2) {
          return <Tag color="orange">Nữ</Tag>;
        }
        return <Tag color="blue">Khác</Tag>;
      },
    },
    {
      title: <FormattedMessage id="pages.carers.avatar" defaultMessage="Avatar" />,
      dataIndex: 'avatar',
      render: (_, record) => {
        if (record.avatar) {
          return <Avatar src={record.avatar} />;
        }

        return 'N/A';
      },
    },
    {
      title: 'Thao Tác',
      dataIndex: 'id',
      render: (_, record) => {
        return (
          <>
            <Button
              type="primary"
              key="update"
              onClick={() => onOpenUpdateForm(record.id)}
              shape="circle"
            >
              <EditOutlined />
            </Button>
            <Button
              type="primary"
              color=""
              key="view-detail"
              onClick={() => onOpenDrawer(record.id)}
              shape="circle"
              style={{ marginLeft: '10px' }}
            >
              <EyeOutlined />
            </Button>
          </>
        );
      },
    },
  ];

  // Multi lang
  const intl = useIntl();
  const [patientCarerList, setPatientCarerList] = useState<PatientCarerItem[]>([]);
  const [pagination, setPagination] = useState<API.PaginationInfo>();
  const [createModalVisible, setCreateModalVisible] = useState<boolean>(false);
  const [skillOptions, setSkillOptions] = useState<any>([]);
  const [levelOptions, setLevelOptions] = useState<any>([]);
  const [locationOptions, setLocationOptions] = useState<any>([]);
  const [updateModalVisible, setUpdateModalVisible] = useState<boolean>(false);
  const [drawerVisible, setDrawerVisible] = useState<boolean>(false);
  const [currentUpdatedCarer, setCurrentUpdatedCarer] = useState<PatientCarerModel>();
  const actionRef = useRef<ActionType>();

  const onFetchPatientCarers = async (params: any, sorter: any, queryFilters?: any) => {
    const orderBy = Object.keys(sorter)[0];
    let desc = false;
    if (orderBy) {
      desc = sorter[Object.keys(sorter)[0]] === 'descend';
    }

    const queryParams = {
      page: params.current || 1,
      size: params.pageSize || 10,
      orderBy,
      desc,
      levelId: queryFilters?.levelId,
      preferLocationId: queryFilters?.preferLocationId,
      search: queryFilters?.search,
    };

    const response = await getPatientCarers(queryParams);
    setPatientCarerList(response.data.items);
    setPagination({
      current: response.data.currentPage,
      total: response.data.total,
      pageSize: response.data.size,
      defaultPageSize: 10,
    });

    return {
      data: response.data.items,
      success: response.success,
    };
  };

  const onOpenUpdateForm = async (id: string) => {
    const response = await getPatientCarer(id);
    setCurrentUpdatedCarer(response.data);
    setUpdateModalVisible(true);
  };

  const onDrawerClose = () => {
    setDrawerVisible(false);
  };

  const onOpenDrawer = async (id: string) => {
    setDrawerVisible(true);
    const response = await getPatientCarer(id);
    setCurrentUpdatedCarer(response.data);
  };

  const onCreatePatientCarer = async (values: any) => {
    return createPatientCarer({
      ...values,
      skills: values.skills.map((e: string) => ({
        id: e,
        value: true,
      })),
      gender: Number(values.gender),
    });
  };

  const onUpdatePatientCarer = async (id: string, values: any) => {
    return updatePatientCarer({
      ...values,
      id,
      birthday: moment(values.birthday, ['MM-DD-YYYY', 'YYYY-MM-DD', 'DD/MM/YYYY']).format(),
      cardDate: moment(values.cardDate, ['MM-DD-YYYY', 'YYYY-MM-DD', 'DD/MM/YYYY']).format(),
      skills: values.skills.map((e: string) => ({
        id: e,
        value: true,
      })),
      gender: Number(values.gender),
    });
  };

  useEffect(() => {
    async function callServicesAsync() {
      const skillResponse = await getRequireSkills();
      setSkillOptions(
        skillResponse.data.map((e) => ({
          label: e.name,
          value: e.id,
        })),
      );

      const levelResponse = await getLevels();
      setLevelOptions(
        levelResponse.data.map((e) => ({
          label: e.name,
          value: e.id,
        })),
      );

      const locationResponse = await getLocations();
      setLocationOptions(
        locationResponse.data.map((e) => ({
          label: e.name,
          value: e.id,
        })),
      );
    }

    callServicesAsync();
  }, []);

  const handleCreateModalVisible = (value: boolean) => {
    setCreateModalVisible(value);
  };

  const handleUpdateModalVisible = (value: boolean) => {
    setUpdateModalVisible(value);
  };

  return (
    <PageContainer>
      <Card>
        <div>
          <QueryFilter
            layout="vertical"
            onFinish={async (values) => {
              await onFetchPatientCarers({ current: 1, pageSize: 10 }, { id: 'descend' }, values);
            }}
          >
            <ProFormText
              name="search"
              label={intl.formatMessage({
                id: 'pages.carers.search',
                defaultMessage: 'Enter something to search ...',
              })}
              width="md"
            />
            <ProFormSelect
              name="levelId"
              label={intl.formatMessage({
                id: 'pages.carers.level',
                defaultMessage: 'Carer Levels',
              })}
              // mode="multiple"
              options={levelOptions}
              width="md"
            />
            <ProFormSelect
              name="preferLocationId"
              label={intl.formatMessage({
                id: 'pages.carers.preferLocation',
                defaultMessage: 'Prefer Locations',
              })}
              // mode="multiple"
              options={locationOptions}
            />
          </QueryFilter>
        </div>
        <Divider />
        <ProTable<PatientCarerItem>
          actionRef={actionRef}
          rowKey="id"
          search={false}
          toolbar={{
            // filter: (
            //   <LightFilter onFinish={async (values: any) => console.log(values)}>
            //     <ProFormSelect
            //       name="levelId"
            //       label={
            //         <strong>
            //           {intl.formatMessage({
            //             id: 'pages.carers.level',
            //             defaultMessage: 'Carer Levels',
            //           })}
            //         </strong>
            //       }
            //       // mode="multiple"
            //       options={levelOptions}
            //       width="md"
            //     />
            //     <ProFormSelect
            //       name="preferLocationId"
            //       label={
            //         <strong>
            //           {intl.formatMessage({
            //             id: 'pages.carers.preferLocation',
            //             defaultMessage: 'Prefer Locations',
            //           })}
            //         </strong>
            //       }
            //       // mode="multiple"
            //       options={locationOptions}
            //     />
            //   </LightFilter>
            // ),
            actions: [
              <Button
                type="primary"
                key="primary"
                onClick={() => {
                  setCreateModalVisible(true);
                }}
              >
                <PlusOutlined />{' '}
                <FormattedMessage id="actions.create" defaultMessage="Create New" />
              </Button>,
            ],
          }}
          request={(params, sorter) => onFetchPatientCarers(params, sorter)}
          columns={columns}
          dateFormatter="string"
          dataSource={patientCarerList}
          pagination={pagination}
          // options={{
          //   search: {
          //     name: 'search',
          //   },
          // }}
        />
        <CreateForm
          visible={createModalVisible}
          onVisibleChange={setCreateModalVisible}
          onFinish={onCreatePatientCarer}
          skillOptions={skillOptions}
          levelOptions={levelOptions}
          locationOptions={locationOptions}
          actionRef={actionRef}
          handleCreateModalVisible={handleCreateModalVisible}
        />
        <UpdateForm
          visible={updateModalVisible}
          onVisibleChange={setUpdateModalVisible}
          onFinish={onUpdatePatientCarer}
          skillOptions={skillOptions}
          currentUpdatedCarer={currentUpdatedCarer}
          currentUpdatedCarerId={currentUpdatedCarer?.id || ''}
          levelOptions={levelOptions}
          locationOptions={locationOptions}
          actionRef={actionRef}
          handleUpdateModalVisible={handleUpdateModalVisible}
        />
        <InformationDrawer
          visible={drawerVisible}
          onClose={onDrawerClose}
          currentCarer={currentUpdatedCarer}
          key={currentUpdatedCarer?.id}
        />
      </Card>
    </PageContainer>
  );
};

export default PatientCarerView;
