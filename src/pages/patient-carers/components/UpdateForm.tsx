import React, { useEffect } from 'react';
import ProForm, {
  ModalForm,
  ProFormText,
  ProFormDatePicker,
  // ProFormUploadButton,
  ProFormSelect,
} from '@ant-design/pro-form';
import { Form, Skeleton } from 'antd';
import { useIntl, FormattedMessage } from 'umi';
import moment from 'moment';
import { PatientCarerCreateResponse, PatientCarerModel } from '../data';

export interface UpdateFormProps {
  visible: boolean;
  onFinish: (id: string, values: any) => Promise<PatientCarerCreateResponse>;
  onVisibleChange: (visible: boolean) => void;
  handleUpdateModalVisible: (visible: boolean) => void;
  skillOptions: any;
  levelOptions: any;
  locationOptions: any;
  actionRef: any;
  currentUpdatedCarer: PatientCarerModel | undefined;
  currentUpdatedCarerId: string;
}

const UpdateForm: React.FC<UpdateFormProps> = ({
  visible,
  onFinish,
  onVisibleChange,
  handleUpdateModalVisible,
  skillOptions,
  levelOptions,
  locationOptions,
  actionRef,
  currentUpdatedCarer,
  currentUpdatedCarerId,
}) => {
  // Multi lang
  const intl = useIntl();
  const [form] = Form.useForm();

  useEffect(() => {
    if (form && visible) form.resetFields();
  }, [visible]);

  return (
    <ModalForm
      title={intl.formatMessage({
        id: 'pages.carers.create.title',
        defaultMessage: 'Update Patient Carer',
      })}
      width="910px"
      visible={visible}
      onVisibleChange={onVisibleChange}
      onFinish={async (values: any) => {
        const response = await onFinish(currentUpdatedCarerId, values);
        if (response.success) {
          handleUpdateModalVisible(false);
          if (actionRef.current) {
            actionRef.current.reload();
          }
        }
      }}
      form={form}
      // initialValues={initialValues()}
    >
      {!currentUpdatedCarer ? (
        <Skeleton />
      ) : (
        <>
          <ProForm.Group
            title={intl.formatMessage({
              id: 'pages.carers.info',
              defaultMessage: 'Personal Information',
            })}
          >
            <ProFormText
              width="md"
              label={intl.formatMessage({
                id: 'pages.carers.fullName',
                defaultMessage: 'Full Name',
              })}
              rules={[
                {
                  required: true,
                  message: (
                    <FormattedMessage
                      id="errors.required"
                      defaultMessage="This field is required"
                    />
                  ),
                },
              ]}
              name="fullName"
              initialValue={currentUpdatedCarer.fullName}
            />
            <ProFormDatePicker
              width="md"
              name="birthday"
              label={intl.formatMessage({
                id: 'pages.carers.birthday',
                defaultMessage: 'Birthday',
              })}
              rules={[
                {
                  required: true,
                  message: (
                    <FormattedMessage
                      id="errors.required"
                      defaultMessage="This field is required"
                    />
                  ),
                },
              ]}
              initialValue={
                currentUpdatedCarer?.birthday != null
                  ? moment(currentUpdatedCarer.birthday)
                  : undefined
              }
            />
            <ProFormText
              width="md"
              label={intl.formatMessage({
                id: 'pages.carers.cardNo',
                defaultMessage: 'Identity Card',
              })}
              rules={[
                {
                  required: true,
                  message: (
                    <FormattedMessage
                      id="errors.required"
                      defaultMessage="This field is required"
                    />
                  ),
                },
              ]}
              name="cardNo"
              initialValue={currentUpdatedCarer.cardNo}
            />
            <ProFormText
              width="md"
              label={intl.formatMessage({
                id: 'pages.carers.cardIssuer',
                defaultMessage: 'Issue Place',
              })}
              rules={[
                {
                  required: true,
                  message: (
                    <FormattedMessage
                      id="errors.required"
                      defaultMessage="This field is required"
                    />
                  ),
                },
              ]}
              name="cardIssuer"
              initialValue={currentUpdatedCarer.cardIssuer}
            />
            <ProFormDatePicker
              width="md"
              name="cardDate"
              initialValue={moment(currentUpdatedCarer.cardDate)}
              label={intl.formatMessage({
                id: 'pages.carers.cardDate',
                defaultMessage: 'Issue Date',
              })}
              rules={[
                {
                  required: true,
                  message: (
                    <FormattedMessage
                      id="errors.required"
                      defaultMessage="This field is required"
                    />
                  ),
                },
              ]}
            />
            <ProFormSelect
              width="md"
              name="gender"
              label={intl.formatMessage({
                id: 'pages.carers.gender',
                defaultMessage: 'Gender',
              })}
              options={[
                {
                  value: 1,
                  label: intl.formatMessage({
                    id: 'pages.carers.gender.male',
                    defaultMessage: 'Male',
                  }),
                },
                {
                  value: 2,
                  label: intl.formatMessage({
                    id: 'pages.carers.gender.female',
                    defaultMessage: 'Female',
                  }),
                },
                {
                  value: 0,
                  label: intl.formatMessage({
                    id: 'pages.carers.gender.others',
                    defaultMessage: 'Others',
                  }),
                },
              ]}
              rules={[
                {
                  required: true,
                  message: intl.formatMessage({
                    id: 'errors.required',
                    defaultMessage: 'This field is required',
                  }),
                },
              ]}
              fieldProps={{
                defaultValue: currentUpdatedCarer.gender,
              }}
              initialValue={currentUpdatedCarer.gender}
            />
            {/* <ProFormUploadButton name="avatar" label="Ảnh cá nhân" max={1} action="/api/files/aws-s3" title="Chọn hình ảnh" /> */}
          </ProForm.Group>
          <ProForm.Group
            title={intl.formatMessage({
              id: 'pages.carers.contactInfo',
              defaultMessage: 'Contract Information',
            })}
          >
            <ProFormText
              width="md"
              label={intl.formatMessage({
                id: 'pages.carers.contactPhone',
                defaultMessage: 'Contact Phone',
              })}
              rules={[
                {
                  required: true,
                  message: (
                    <FormattedMessage
                      id="errors.required"
                      defaultMessage="This field is required"
                    />
                  ),
                },
              ]}
              name="contactPhone"
              initialValue={currentUpdatedCarer.contactPhone}
            />
            <ProFormText
              width="md"
              label={intl.formatMessage({
                id: 'pages.carers.permanentAddress',
                defaultMessage: 'Thường Trú',
              })}
              rules={[
                {
                  required: true,
                  message: (
                    <FormattedMessage
                      id="errors.required"
                      defaultMessage="This field is required"
                    />
                  ),
                },
              ]}
              name="permanentAddress"
              initialValue={currentUpdatedCarer.permanentAddress}
            />
            <ProFormText
              width="md"
              label={intl.formatMessage({
                id: 'pages.carers.contactAddress',
                defaultMessage: 'Contact Address',
              })}
              rules={[
                {
                  required: true,
                  message: (
                    <FormattedMessage
                      id="errors.required"
                      defaultMessage="This field is required"
                    />
                  ),
                },
              ]}
              name="contactAddress"
              initialValue={currentUpdatedCarer.contactAddress}
            />
          </ProForm.Group>
          <ProForm.Group
            title={intl.formatMessage({
              id: 'pages.carers.skillInfo',
              defaultMessage: 'Work Skills',
            })}
          >
            <ProFormSelect
              width="md"
              name="levelId"
              label={intl.formatMessage({
                id: 'pages.carers.levelId',
                defaultMessage: 'Current Level',
              })}
              options={levelOptions}
              rules={[
                {
                  required: true,
                  message: intl.formatMessage({
                    id: 'errors.required',
                    defaultMessage: 'This field is required',
                  }),
                },
              ]}
              fieldProps={{
                defaultValue: currentUpdatedCarer.level.id,
              }}
              initialValue={currentUpdatedCarer.level.id}
            />
            <ProFormSelect
              width="md"
              name="preferLocationId"
              label={intl.formatMessage({
                id: 'pages.carers.preferLocationId',
                defaultMessage: 'Prefer Location',
              })}
              options={locationOptions}
              rules={[
                {
                  required: true,
                  message: intl.formatMessage({
                    id: 'errors.required',
                    defaultMessage: 'This field is required',
                  }),
                },
              ]}
              fieldProps={{
                defaultValue: currentUpdatedCarer.preferLocation.id,
              }}
              initialValue={currentUpdatedCarer.preferLocation.id}
            />
            <ProFormSelect
              width="xl"
              name="skills"
              label={intl.formatMessage({
                id: 'pages.carers.skills',
                defaultMessage: 'Work Skills',
              })}
              options={skillOptions}
              mode="tags"
              rules={[
                {
                  required: true,
                  message: intl.formatMessage({
                    id: 'errors.required',
                    defaultMessage: 'This field is required',
                  }),
                },
              ]}
              initialValue={currentUpdatedCarer.personalSkills.map((e) => e.skillId)}
              fieldProps={{
                defaultValue: currentUpdatedCarer.personalSkills.map((e) => e.skillId),
              }}
            />
          </ProForm.Group>
        </>
      )}
    </ModalForm>
  );
};

export default UpdateForm;
