declare namespace SCHEDULE {
  export interface ContractScheduleModel {
    id: string;
    serviceName: string;
    patientCarerName: string;
    patientCarerId: string;
    from: Date;
    to: Date;
    created: Date;
    status: number;
  }
}
