import { apiConfig } from '@/configs/apis';
import request from '../../utils/request';

export async function getContractPayments(params: any) {
  return request<PAYMENT.ContractPaymentListResponse>(
    `${apiConfig.prefixUrl}/api/contract-payments`,
    { params },
  );
}

export async function getContract(id: string) {
  return request<PAYMENT.ContractPaymentListResponse>(
    `${apiConfig.prefixUrl}/api/contract-payments/${id}`,
  );
}

export async function createContractPayment(params: PAYMENT.ContractPaymentModel) {
  return request<any>(`${apiConfig.prefixUrl}/api/contract-payments`, {
    data: params,
    method: 'POST',
  });
}

export async function createMultipleContractPayment(params: any) {
  return request<any>(`${apiConfig.prefixUrl}/api/contract-payments/bulk`, {
    data: params,
    method: 'POST',
  });
}

export async function updateContractPayment(params: PAYMENT.ContractPaymentModel) {
  return request<any>(`${apiConfig.prefixUrl}/api/contract-payments/${params.id}`, {
    data: params,
    method: 'PUT',
  });
}

export async function getContractSelectList(params: any) {
  return request(`${apiConfig.prefixUrl}/api/select-list/contracts`, { params });
}
