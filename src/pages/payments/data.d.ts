declare namespace PAYMENT {
  export interface ContractPaymentItem {
    id: string;
    contractCode: string;
    type: number;
    note: string;
    amount: number;
    created: Date;
  }

  export interface ContractPaymentModel {
    id: string;
    contractCode: string;
    amount: number;
    type: number;
    note: Date;
    patientCarerCode: string;
    patientCarerName: string
  }

  export interface ContractPaymentListResponse {
    success: boolean;
    error: any;
    data: API.PaginationData<ContractPaymentItem>;
  }

  export interface ContractPaymentQueryParams extends API.QueryParams {
    contractId: string;
    type: number;
  }
}
