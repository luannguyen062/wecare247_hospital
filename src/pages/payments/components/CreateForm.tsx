import React from 'react';
import ProForm, {
  ModalForm,
  ProFormText,
  // ProFormUploadButton,
  ProFormSelect,
  ProFormDigit,
  ProFormTextArea,
  // ProFormUploadButton,
} from '@ant-design/pro-form';
import { useIntl, FormattedMessage } from 'umi';
import { message } from 'antd';
// import { message } from 'antd';
// import { PatientCarerCreateResponse } from '../data';

export interface CreateFormProps {
  visible: boolean;
  onFinish: (values: any) => Promise<PAYMENT.ContractPaymentListResponse>;
  onVisibleChange?: (visible: boolean) => void;
  handleCreateModalVisible: (visible: boolean) => void;
  // skillOptions: any;
  // levelOptions: any;
  // locationOptions: any;
  actionRef: any;
}

const CreateForm: React.FC<CreateFormProps> = ({
  visible,
  onFinish,
  onVisibleChange,
  // skillOptions,
  // levelOptions,
  // locationOptions,
  actionRef,
  handleCreateModalVisible,
}) => {
  // Multi lang
  const intl = useIntl();
  return (
    <ModalForm
      title={intl.formatMessage({
        id: 'pages.payments.create.title',
        defaultMessage: 'Create New Payment',
      })}
      width="800px"
      visible={visible}
      onVisibleChange={onVisibleChange}
      onFinish={async (values: any) => {
        const response = await onFinish(values);
        if (response.success) {
          handleCreateModalVisible(false);
          if (actionRef.current) {
            actionRef.current.reload();
          }
          message.success(
            intl.formatMessage({
              id: 'api.create.success',
              defaultMessage: 'Success',
            }),
          );
        }
      }}
    >
      <ProForm.Group>
        <ProFormText
          width="md"
          label={intl.formatMessage({
            id: 'pages.payments.contractCode',
            defaultMessage: 'Pay for Contract',
          })}
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage id="errors.required" defaultMessage="This field is required" />
              ),
            },
          ]}
          name="contractCode"
        />
        <ProFormSelect
          width="md"
          name="type"
          label={intl.formatMessage({
            id: 'pages.payments.type',
            defaultMessage: 'Loại Thanh Toán',
          })}
          valueEnum={{
            10: intl.formatMessage({
              id: 'pages.payments.type.payForContract',
              defaultMessage: 'Thanh Toán HĐDV',
            }),
            20: intl.formatMessage({
              id: 'pages.payments.type.refund',
              defaultMessage: 'Hoàn Trả Khách Hàng',
            }),
            100: intl.formatMessage({
              id: 'pages.payments.type.ref',
              defaultMessage: 'Chi Huê Hồng',
            }),
          }}
          rules={[
            {
              required: true,
              message: intl.formatMessage({
                id: 'errors.required',
                defaultMessage: 'This field is required',
              }),
            },
          ]}
        />
        <ProFormDigit
          width="md"
          label={intl.formatMessage({
            id: 'pages.payments.amount',
            defaultMessage: 'Amount',
          })}
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage id="errors.required" defaultMessage="This field is required" />
              ),
            },
          ]}
          name="amount"
        />
        <ProFormTextArea
          width="lg"
          name="note"
          label={intl.formatMessage({
            id: 'pages.payments.note',
            defaultMessage: 'Mô tả khác',
          })}
        />
      </ProForm.Group>
    </ModalForm>
  );
};

export default CreateForm;
