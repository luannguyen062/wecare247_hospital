import React, { useEffect } from 'react';
import ProForm, {
  ModalForm,
  ProFormText,
  ProFormSelect,
  ProFormDigit,
  ProFormTextArea,
} from '@ant-design/pro-form';
import { useIntl, FormattedMessage } from 'umi';
import { Form, message, Skeleton } from 'antd';

export interface UpdateFormProps {
  visible: boolean;
  onFinish: (values: any) => Promise<PAYMENT.ContractPaymentListResponse>;
  onVisibleChange: (visible: boolean) => void;
  currentPayment: PAYMENT.ContractPaymentModel;
  actionRef: any;
}

const UpdateForm: React.FC<UpdateFormProps> = ({
  visible,
  onFinish,
  onVisibleChange,
  currentPayment,
  actionRef,
}) => {
  // Multi lang
  const intl = useIntl();
  const [form] = Form.useForm();
  useEffect(() => {
    if (form && visible) form.resetFields();
  }, [visible]);

  return (
    <ModalForm
      title={intl.formatMessage({
        id: 'pages.payments.create.title',
        defaultMessage: 'Create New Payment',
      })}
      width="800px"
      form={form}
      visible={visible}
      onVisibleChange={onVisibleChange}
      onFinish={async (values: any) => {
        const response = await onFinish({
          ...values,
          id: currentPayment?.id
        });
        if (response.success) {
          onVisibleChange(false);
          if (actionRef.current) {
            actionRef.current.reload();
          }
          message.success(
            intl.formatMessage({
              id: 'api.create.success',
              defaultMessage: 'Success',
            }),
          );
        }
      }}
    >
      {!currentPayment ? (
        <Skeleton />
      ) : (
        <>
          <ProForm.Group>
            <ProFormText
              width="md"
              label={intl.formatMessage({
                id: 'pages.payments',
                defaultMessage: 'Pay for Contract',
              })}
              rules={[
                {
                  required: true,
                  message: (
                    <FormattedMessage
                      id="errors.required"
                      defaultMessage="This field is required"
                    />
                  ),
                },
              ]}
              name="contractCode"
              initialValue={currentPayment != null ? currentPayment.contractCode : undefined}
              disabled
            />
            <ProFormSelect
              width="md"
              name="type"
              label={intl.formatMessage({
                id: 'pages.payments.type',
                defaultMessage: 'Loại Thanh Toán',
              })}
              options={[
                {
                  value: 10,
                  label: intl.formatMessage({
                    id: 'pages.payments.type.payForContract',
                    defaultMessage: 'Paid',
                  }),
                },
                {
                  value: 20,
                  label: intl.formatMessage({
                    id: 'pages.payments.type.refund',
                    defaultMessage: 'Refund',
                  }),
                },
              ]}
              rules={[
                {
                  required: true,
                  message: intl.formatMessage({
                    id: 'errors.required',
                    defaultMessage: 'This field is required',
                  }),
                },
              ]}
              initialValue={currentPayment != null ? currentPayment.type : undefined}
            />
            <ProFormDigit
              width="md"
              label={intl.formatMessage({
                id: 'pages.payments.amount',
                defaultMessage: 'Amount',
              })}
              rules={[
                {
                  required: true,
                  message: (
                    <FormattedMessage
                      id="errors.required"
                      defaultMessage="This field is required"
                    />
                  ),
                },
              ]}
              name="amount"
              initialValue={currentPayment != null ? currentPayment.amount : undefined}
            />
            <ProFormTextArea
              width="lg"
              name="note"
              label={intl.formatMessage({
                id: 'pages.payments.note',
                defaultMessage: 'Mô tả khác',
              })}
              initialValue={currentPayment != null ? currentPayment.note : undefined}
            />
          </ProForm.Group>
        </>
      )}
    </ModalForm>
  );
};

export default UpdateForm;
