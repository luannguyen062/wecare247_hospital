/* eslint-disable @typescript-eslint/no-use-before-define */
import React, { useRef, useState, useEffect } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import { Button, Card, Modal, Tag } from 'antd';
import { FormattedMessage, Link, useIntl } from 'umi';
import ProTable, { ActionType, ProColumns } from '@ant-design/pro-table';
import { EditOutlined, PlusOutlined } from '@ant-design/icons';
import moment from 'moment';
import MultiRowForm from '@/components/WeCare247/Forms/MultiRowForm';
import {
  createContractPayment,
  createMultipleContractPayment,
  getContractPayments,
  getContractSelectList,
  updateContractPayment,
} from '../service';
import CreateForm from '../components/CreateForm';
import UpdateForm from '../components/UpdateForm';

const ReceiptView: React.FC<{}> = () => {
  // Columns
  const intl = useIntl();
  const columns: ProColumns<PAYMENT.ContractPaymentItem>[] = [
    {
      title: <FormattedMessage id="pages.payments.id" defaultMessage="Payment Ref" />,
      dataIndex: 'id',
      search: false,
    },
    {
      title: <FormattedMessage id="pages.payments.contractCode" defaultMessage="Contract" />,
      dataIndex: 'contractCode',
      sorter: true,
      formItemProps: {
        name: 'search',
        rules: [
          {
            required: true,
            message: 'Vui lỏng điền mã HĐDV',
          },
        ],
      },
      render: (_, record) => (
        <Link to={`/contract-view/${record.contractCode}`}>{record.contractCode}</Link>
      ),
    },
    {
      title: <FormattedMessage id="pages.payments.type" defaultMessage="Type" />,
      dataIndex: 'type',
      sorter: true,
      filters: true,
      valueType: 'select',
      valueEnum: {
        10: { text: 'Paid', type: '10' },
        20: { text: 'Refund', type: '20' },
      },
      render: (_, record) => {
        if (record.type === 10) {
          return (
            <Tag color="green">
              <FormattedMessage id="pages.payments.type.paid" defaultMessage="Paid" />
            </Tag>
          );
        }

        return (
          <Tag color="orange">
            <FormattedMessage id="pages.payments.type.refund" defaultMessage="Refund" />
          </Tag>
        );
      },
    },
    {
      title: <FormattedMessage id="pages.payments.amount" defaultMessage="Amount" />,
      dataIndex: 'amount',
      search: false,
      valueType: 'digit',
    },
    {
      title: <FormattedMessage id="pages.payments.note" defaultMessage="Note" />,
      dataIndex: 'note',
      search: false,
    },
    {
      title: <FormattedMessage id="pages.payments.created" defaultMessage="Date" />,
      dataIndex: 'created',
      render: (_, record) => moment(record.created).format('DD/MM/YYYY LT A'),
      sorter: true,
      search: false,
    },
    {
      title: <FormattedMessage id="pages.payments.createdBy" defaultMessage="Employee" />,
      dataIndex: 'createdBy',
      search: false,
    },
    {
      title: 'Thao Tác',
      dataIndex: 'id',
      search: false,
      render: (_, record) => {
        return (
          <>
            <Button
              type="primary"
              key="update"
              onClick={() => onOpenUpdateForm(record)}
              shape="circle"
            >
              <EditOutlined />
            </Button>
          </>
        );
      },
    },
  ];

  const actionRef = useRef<ActionType>();
  const [paymentList, setPaymentList] = useState<PAYMENT.ContractPaymentItem[]>();
  const [pagination, setPagination] = useState<any>();
  const [createModalVisible, setCreateModalVisible] = useState<boolean>(false);
  const [createMultipleModalVisible, setCreateMultipleModalVisible] = useState<boolean>(false);
  const [updateModalVisible, setUpdateModalVisible] = useState<boolean>(false);
  const [currentPayment, setCurrentPayment] = useState<any>(null);
  const [contracts, setContracts] = useState<any>();

  const onOpenUpdateForm = (record: any) => {
    setUpdateModalVisible(true);
    setCurrentPayment(record);
  };

  const onFetchPayments = async (params: any, sorter: any, queryFilters?: any) => {
    const orderBy = Object.keys(sorter)[0];
    let desc = false;
    if (orderBy) {
      desc = sorter[Object.keys(sorter)[0]] === 'descend';
    }

    const queryParams = {
      page: params.current || 1,
      size: params.pageSize || 10,
      orderBy,
      desc,
      levelId: queryFilters?.levelId,
      preferLocationId: queryFilters?.preferLocationId,
      search: queryFilters?.search,
    };

    const response = await getContractPayments(queryParams);
    setPaymentList(response.data.items);
    setPagination({
      current: response.data.currentPage,
      total: response.data.total,
      pageSize: response.data.size,
      defaultPageSize: 10,
    });

    return {
      data: response.data.items,
      success: response.success,
    };
  };

  const onCreateContractPayment = async (values: any) => {
    return createContractPayment({
      ...values,
      type: Number(values.type),
    });
  };

  const onUpdateContractPayment = async (values: any) => {
    return updateContractPayment({
      ...values,
      type: Number(values.type),
    });
  };

  const handleCreateModalVisible = (value: boolean) => {
    setCreateModalVisible(value);
  };

  useEffect(() => {
    async function getContractsAsync() {
      const response = await getContractSelectList({});
      setContracts(response.data);
    }

    getContractsAsync();
  }, []);

  const onCreateMultipleContractPayment = async (values: any) => {
    return createMultipleContractPayment({
      payments: values.payments.map((item: any) => ({
        contractCode: item.contractCode,
        amount: Number(item.amount),
        note: item.node,
      })),
      exportExcel: false,
    });
  };
  return (
    <PageContainer>
      <Card>
        <ProTable<PAYMENT.ContractPaymentItem>
          actionRef={actionRef}
          rowKey="id"
          toolbar={{
            actions: [
              // <Button
              //   type="primary"
              //   key="primary"
              //   onClick={() => {
              //     setCreateModalVisible(true);
              //   }}
              // >
              //   <PlusOutlined />
              //   <FormattedMessage id="actions.create" defaultMessage="Create New" />
              // </Button>,
              <Button
                type="primary"
                key="primary"
                onClick={() => {
                  setCreateMultipleModalVisible(true);
                }}
              >
                <PlusOutlined />{' '}
                <FormattedMessage id="actions.create" defaultMessage="Create New" />
              </Button>,
            ],
          }}
          request={(params, sorter, filter) => onFetchPayments(params, sorter, filter)}
          columns={columns}
          dateFormatter="string"
          dataSource={paymentList}
          pagination={pagination}
          search={{
            labelWidth: 120,
          }}
        />
        <CreateForm
          visible={createModalVisible}
          onVisibleChange={setCreateModalVisible}
          onFinish={onCreateContractPayment}
          actionRef={actionRef}
          handleCreateModalVisible={handleCreateModalVisible}
        />
        <UpdateForm
          visible={updateModalVisible}
          onVisibleChange={setUpdateModalVisible}
          onFinish={onUpdateContractPayment}
          actionRef={actionRef}
          currentPayment={currentPayment}
        />
        <Modal
          visible={createMultipleModalVisible}
          title={intl.formatMessage({
            id: 'pages.payments.create.multiple',
            defaultMessage: 'Create Multiple Payments',
          })}
          onCancel={() => setCreateMultipleModalVisible(false)}
          footer={null}
          width={800}
        >
          <MultiRowForm
            addMoreRowTitle={intl.formatMessage({
              id: 'pages.payments.create.multiple.more',
              defaultMessage: 'Add More',
            })}
            contracts={contracts || []}
            visible={createMultipleModalVisible}
            onFinish={onCreateMultipleContractPayment}
            closeForm={() => setCreateMultipleModalVisible(false)}
            actionRef={actionRef}
          />
        </Modal>
      </Card>
    </PageContainer>
  );
};

export default ReceiptView;
// MultiRowForm
