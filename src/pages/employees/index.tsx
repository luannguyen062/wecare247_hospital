import React, { useRef, useState, useEffect } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import ProTable, { ActionType, ProColumns } from '@ant-design/pro-table';
import { Button, Card, Tooltip } from 'antd';
import { PlusOutlined, CheckCircleOutlined } from '@ant-design/icons';
import { useIntl } from 'umi';
import ProForm, {
  // ProFormCheckbox,
  ProFormDatePicker,
  ProFormSelect,
  ProFormText,
} from '@ant-design/pro-form';
import {
  createEmployee,
  getEmployeePermissions,
  getEmployees,
  getFeaturePermissions,
  getRoleSelectList,
  updateEmployeePermissions,
} from './service';

import CreateForm from './components/CreateForm';
import PermissionModal from './components/PermissionModal';

const EmployeeView: React.FC<{}> = () => {
  const actionRef = useRef<ActionType>();
  const intl = useIntl();

  const [createModalVisible, setCreateModalVisible] = useState(false);
  const [roleOptions, setRoleOptions] = useState([]);
  const [features, setFeatures] = useState([]);
  const [permissionModalVisible, setPermissionModalVisible] = useState(false);
  const [currentUser, setCurrentUser] = useState<any>();
  const [existedPermissions, setExistedPermissions] = useState<any>();

  const openPermissionModal = async (record: any) => {
    const response = await getEmployeePermissions({ id: record.id });
    setExistedPermissions(response.data);
    setPermissionModalVisible(true);
    setCurrentUser(record);
  };

  const columns: ProColumns<EMPLOYEE.EmployeeItem>[] = [
    {
      title: intl.formatMessage({ id: 'pages.employees.code', defaultMessage: 'Full Name' }),
      dataIndex: 'fullName',
      formItemProps: {
        name: 'search',
      },
      sorter: true,
    },
    {
      title: intl.formatMessage({ id: 'pages.employees.email', defaultMessage: 'Email Address' }),
      dataIndex: 'email',
      search: false,
      sorter: true,
    },
    {
      title: intl.formatMessage({
        id: 'pages.employees.contactPhone',
        defaultMessage: 'Phone No#',
      }),
      dataIndex: 'contactPhone',
      search: false,
      sorter: true,
    },
    {
      title: intl.formatMessage({ id: 'pages.employees.gender', defaultMessage: 'Gender' }),
      dataIndex: 'gender',
      render: (_, record) => {
        if (record.gender === 1) {
          return (
            <p>
              {intl.formatMessage({
                id: 'pages.users.gender.male',
                defaultMessage: 'Male',
              })}
            </p>
          );
        }

        if (record.gender === 2) {
          return (
            <p>
              {intl.formatMessage({
                id: 'pages.users.gender.female',
                defaultMessage: 'Female',
              })}
            </p>
          );
        }

        return 'N/A';
      },
      search: false,
    },
    {
      title: intl.formatMessage({ id: 'pages.employees.roleId', defaultMessage: 'Role' }),
      dataIndex: 'role',
      sorter: true,
      search: false,
    },
    {
      title: 'Thao Tác',
      dataIndex: 'id',
      render: (_, record) => {
        return (
          <>
            <Tooltip
              title={intl.formatMessage({
                id: 'actions.permission',
                defaultMessage: 'Add Permissions',
              })}
            >
              <Button
                type="primary"
                key="update"
                onClick={() => openPermissionModal(record)}
                shape="circle"
              >
                <CheckCircleOutlined />
              </Button>
            </Tooltip>
          </>
        );
      },
    },
  ];

  useEffect(() => {
    async function fetchAsync() {
      const roles = await getRoleSelectList();
      setRoleOptions(roles.data);

      const featurePermissions = await getFeaturePermissions();
      setFeatures(featurePermissions.data);
    }

    fetchAsync();
  }, []);

  const openCreateModalForm = () => {
    setCreateModalVisible(true);
  };

  const onCreateEmployee = (values: any) => {
    return createEmployee(values);
  };

  const onPermissionModalVisibleChange = (value: boolean) => {
    setPermissionModalVisible(value);
    if (!value) {
      setCurrentUser(null);
      setExistedPermissions([]);
    }
  };

  const onUpdatePermissions = async (values: any) => {
    const codes = Object.values(values) as string[];
    // eslint-disable-next-line prefer-spread
    const permissions = [''].concat.apply([], codes);
    return updateEmployeePermissions({
      id: currentUser.id,
      permissions,
    });
  };

  return (
    <PageContainer>
      <Card>
        <ProTable<EMPLOYEE.EmployeeItem>
          actionRef={actionRef}
          rowKey="id"
          search={{
            labelWidth: 120,
          }}
          request={(params, sorter, filter) => getEmployees({ ...params, sorter, filter })}
          columns={columns}
          toolBarRender={() => [
            <Button type="primary" key="primary" onClick={openCreateModalForm}>
              <PlusOutlined />
              {intl.formatMessage({
                id: 'btn.create',
                defaultMessage: 'Create',
              })}
            </Button>,
          ]}
        />
        <CreateForm modalVisible={createModalVisible} onCancel={() => setCreateModalVisible(false)}>
          <ProForm
            onFinish={async (values: any) => {
              const success = await onCreateEmployee(values);
              if (success) {
                setCreateModalVisible(false);
                if (actionRef.current) {
                  actionRef.current.reload();
                }
              }
            }}
          >
            <ProForm.Group>
              <ProFormText
                width="sm"
                name="code"
                label={intl.formatMessage({
                  id: 'pages.employees.code',
                  defaultMessage: 'Employee Code',
                })}
                rules={[
                  {
                    required: true,
                    message: intl.formatMessage({
                      id: 'errors.required',
                      defaultMessage: 'This field is required!',
                    }),
                  },
                ]}
              />
              <ProFormText
                width="sm"
                name="fullName"
                label={intl.formatMessage({
                  id: 'pages.employees.fullName',
                  defaultMessage: 'Full Name',
                })}
                rules={[
                  {
                    required: true,
                    message: intl.formatMessage({
                      id: 'errors.required',
                      defaultMessage: 'This field is required!',
                    }),
                  },
                ]}
              />
            </ProForm.Group>
            <ProForm.Group>
              <ProFormText
                width="sm"
                name="email"
                label={intl.formatMessage({
                  id: 'pages.employees.email',
                  defaultMessage: 'Login Email',
                })}
                rules={[
                  {
                    required: true,
                    message: intl.formatMessage({
                      id: 'errors.required',
                      defaultMessage: 'This field is required!',
                    }),
                  },
                ]}
              />
              <ProFormText.Password
                width="sm"
                name="password"
                label={intl.formatMessage({
                  id: 'pages.employees.password',
                  defaultMessage: 'Password',
                })}
                rules={[
                  {
                    required: true,
                    message: intl.formatMessage({
                      id: 'errors.required',
                      defaultMessage: 'This field is required!',
                    }),
                  },
                ]}
              />
            </ProForm.Group>
            <ProForm.Group>
              <ProFormSelect
                width="sm"
                name="gender"
                label={intl.formatMessage({
                  id: 'pages.employees.gender',
                  defaultMessage: 'Gender',
                })}
                rules={[
                  {
                    required: true,
                    message: intl.formatMessage({
                      id: 'errors.required',
                      defaultMessage: 'This field is required!',
                    }),
                  },
                ]}
                options={[
                  {
                    label: intl.formatMessage({
                      id: 'pages.users.gender.male',
                      defaultMessage: 'Male',
                    }),
                    value: 1,
                  },
                  {
                    label: intl.formatMessage({
                      id: 'pages.users.gender.female',
                      defaultMessage: 'Female',
                    }),
                    value: 2,
                  },
                ]}
              />
              <ProFormDatePicker
                width="sm"
                name="birthday"
                label={intl.formatMessage({
                  id: 'pages.employees.birthday',
                  defaultMessage: 'Birthday',
                })}
                rules={[
                  {
                    required: true,
                    message: intl.formatMessage({
                      id: 'errors.required',
                      defaultMessage: 'This field is required!',
                    }),
                  },
                ]}
              />
            </ProForm.Group>
            <ProForm.Group>
              <ProFormText
                width="sm"
                name="contactPhone"
                label={intl.formatMessage({
                  id: 'pages.employees.contactPhone',
                  defaultMessage: 'Contact Phone',
                })}
                rules={[
                  {
                    required: true,
                    message: intl.formatMessage({
                      id: 'errors.required',
                      defaultMessage: 'This field is required!',
                    }),
                  },
                ]}
              />
              <ProFormSelect
                width="sm"
                name="roleId"
                label={intl.formatMessage({
                  id: 'pages.employees.roleId',
                  defaultMessage: 'Role',
                })}
                rules={[
                  {
                    required: true,
                    message: intl.formatMessage({
                      id: 'errors.required',
                      defaultMessage: 'This field is required!',
                    }),
                  },
                ]}
                options={roleOptions}
              />
            </ProForm.Group>
          </ProForm>
        </CreateForm>
        <PermissionModal
          onFinish={onUpdatePermissions}
          features={features}
          visible={permissionModalVisible}
          onVisibleChange={onPermissionModalVisibleChange}
          actionRef={actionRef}
          userId={currentUser?.id}
          existedPermissions={existedPermissions}
        />
      </Card>
    </PageContainer>
  );
};

export default EmployeeView;
