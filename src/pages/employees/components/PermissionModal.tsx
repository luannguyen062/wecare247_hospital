import React, { useEffect, useState } from 'react';
import { useIntl } from 'umi';
import ProForm, { ModalForm, ProFormCheckbox } from '@ant-design/pro-form';
import { Checkbox, Form, message } from 'antd';

interface PermissionModalProps {
  visible: boolean;
  onVisibleChange: any;
  onFinish: any;
  userId: string;
  features: any | [];
  actionRef: any;
  existedPermissions: any | [];
}

const PermissionModal: React.FC<PermissionModalProps> = (props: PermissionModalProps) => {
  const intl = useIntl();
  const [formState, setFormState] = useState<any>({ initialValues: {} });
  // const [existedPermissions, setExistedPermissions] = useState<any>([]);

  const [form] = Form.useForm();

  useEffect(() => {
    if (props.visible && props.features) {
      const obj = {};
      props.features.forEach((feature: any) => {
        if (props.existedPermissions.length === 0) {
          obj[feature.id] = feature.permissions.map((p: any) => p.code);
        } else {
          obj[feature.id] = feature.permissions
            .filter((e: any) =>
              props.existedPermissions.some(
                (currentPermission: any) => currentPermission.code === e.code,
              ),
            )
            .map((p: any) => p.code);
        }
      });

      setFormState({
        initialValues: obj,
      });
    }
  }, [props.userId]);

  useEffect(() => {
    if (form && props.visible) form.resetFields();
  }, [props.visible]);

  useEffect(() => {
    form.resetFields();
  }, [formState]);

  const getPermissionCodes = (id: any) => {
    const array = props.features.filter((e: any) => e.id === id)[0];
    if (!array) return [];
    return array.permissions.map((p: any) => p.code);
  };

  const onCheckAllChange = (e: any, id: string) => {
    // Check all
    const currentValues = formState.initialValues;
    if (e.target.checked) {
      currentValues[id] = getPermissionCodes(id);
      setFormState({
        initialValues: currentValues,
      });
    } else {
      currentValues[id] = [];
      setFormState({
        initialValues: currentValues,
      });
    }
  };

  return (
    <ModalForm
      title={intl.formatMessage({
        id: 'pages.employee.permissions.title',
        defaultMessage: 'Employee Permissions',
      })}
      initialValues={{ ...formState.initialValues }}
      form={form}
      visible={props.visible}
      onVisibleChange={props.onVisibleChange}
      onFinish={async (values: any) => {
        const response = await props.onFinish(values);
        if (response.success) {
          props.onVisibleChange(false);
          if (props.actionRef.current) {
            props.actionRef.current.reload();
          }
          message.success(
            intl.formatMessage({
              id: 'api.success',
              defaultMessage: 'Success',
            }),
          );
        }
      }}
    >
      {props.features.map((feature: any) => (
        <ProForm.Group
          title={feature.name}
          key={`group-${feature.id}`}
          extra={
            <Checkbox onChange={(e) => onCheckAllChange(e, feature.id)}>
              {intl.formatMessage({
                id: 'actions.checkAll',
                defaultMessage: 'Check All',
              })}
            </Checkbox>
          }
        >
          <ProFormCheckbox.Group
            name={`${feature.id}`}
            options={feature.permissions.map((p: any) => ({
              label: p.name,
              value: p.code,
            }))}
            // initialValue={() => getInitialValue(feature.id)}
          />
        </ProForm.Group>
      ))}
    </ModalForm>
  );
};

export default PermissionModal;
