import { apiConfig } from '@/configs/apis';
import request from '../../utils/request';

export async function getEmployees(params?: any) {
  return request(`${apiConfig.prefixUrl}/api/employees`, {
    params,
  });
}

export async function createEmployee(params: EMPLOYEE.CreateEmployeeModel) {
  return request(`${apiConfig.prefixUrl}/api/employees`, {
    method: 'POST',
    data: params,
  });
}

export async function getRoleSelectList() {
  return request(`${apiConfig.prefixUrl}/api/select-list/roles`);
}

export async function getFeaturePermissions() {
  return request(`${apiConfig.prefixUrl}/api/features`);
}

export async function updateEmployeePermissions(params: any) {
  return request(`${apiConfig.prefixUrl}/api/employees/${params.id}/permissions`, {
    method: 'PUT',
    data: {
      permissions: params.permissions,
    },
  });
}

export async function getEmployeePermissions(params: any) {
  return request(`${apiConfig.prefixUrl}/api/employees/${params.id}/permissions`);
}
