declare namespace EMPLOYEE {
  export interface EmployeeItem {
    id: string;
    email: string;
    fullName: string;
    birthday?: Date;
    code: string;
    contactPhone: string;
    gender: Number;
    role: string;
  }

  export interface CreateEmployeeModel {
    email: string;
    password: string;
    fullName: string;
    birthday?: Date;
    code: string;
    contactPhone: string;
    gender: Number;
    roleId: string;
  }
}
