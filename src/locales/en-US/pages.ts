export default {
  'pages.layouts.userLayout.title':
    'Ant Design is the most influential web design specification in Xihu district',
  'pages.login.accountLogin.tab': 'Account Login',
  'pages.login.accountLogin.errorMessage': 'email/password is incorrect',
  'pages.login.username.placeholder': 'Email',
  'pages.login.username.required': 'This field is required',
  'pages.login.username.email': 'Invalid email address',
  'pages.login.password.placeholder': 'Password',
  'pages.login.password.required': 'This field is required',
  'pages.login.phoneLogin.tab': 'Phone Login',
  'pages.login.phoneLogin.errorMessage': 'Verification Code Error',
  'pages.login.phoneNumber.placeholder': 'Phone Number',
  'pages.login.phoneNumber.required': 'Please input your phone number!',
  'pages.login.phoneNumber.invalid': 'Phone number is invalid!',
  'pages.login.captcha.placeholder': 'Verification Code',
  'pages.login.captcha.required': 'Please input verification code!',
  'pages.login.phoneLogin.getVerificationCode': 'Get Code',
  'pages.getCaptchaSecondText': 'sec(s)',
  'pages.login.rememberMe': 'Remember me',
  'pages.login.forgotPassword': 'Forgot Password ?',
  'pages.login.submit': 'Đăng Nhập',
  'pages.login.loginWith': 'Login with :',
  'pages.login.registerAccount': 'Register Account',
  'pages.contracts.create.customerInfo': 'Customer Information',
  'pages.contracts.create.customer.fullName': 'Full Name',
  'pages.contracts.create.customer.contactPhone': 'Phone Number',
  'pages.contracts.create.customer.gender': 'Gender',
  'pages.contracts.create.customer.contact': 'Contact Address',
  'pages.contracts.create.customer.street': 'Street',
  'pages.contracts.create.customer.wardId': 'Ward',
  'pages.contracts.create.customer.districtId': 'District',
  'pages.contracts.create.customer.provinceId': 'Province/City',
  'pages.contracts.create.info': 'Contract Information',
  'pages.contracts.create.required-skills': 'Contract Required Skills',
  'pages.contracts.create.skills': 'Select Skill',
  'pages.contracts.create.services': 'Customer Services',
  'pages.contracts.create.serviceId': 'Choose service',
  'pages.contracts.create.from': 'From',
  'pages.contracts.create.to': 'To',
  'pages.contracts.create.note': 'Other description',

  'pages.users.gender.male': 'Male',
  'pages.users.gender.female': 'Female',
  'pages.users.gender.others': 'Other',
  'pages.employees.create.title': 'Create Account',
  'pages.employees.roleId': 'Role',
  'pages.employees.contactPhone': 'Phone',
  'pages.employees.birthday': 'Birthday',
  'pages.employees.gender': 'Gender',
  'pages.employees.password': 'Password',
  'pages.employees.email': 'Email Address',
  'pages.employees.fullName': 'Full Name',
  'pages.employees.code': 'Employee Code',
  'dashboard.contract': 'Contract Statistic',
  'dashboard.payment': 'Payment Statistic',
  'dashboard.customer': 'Customer Statistic',
  'dashboard.customer.type.title': 'Filter By',
  'dashboard.customer.type.province': 'Province/City',
  'dashboard.customer.type.gender': 'Gender',
  'dashboard.customer.type.location': 'Contract Location'
};
