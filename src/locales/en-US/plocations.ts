export default {
  'pages.locations.id': 'Hospital Code',
  'pages.locations.search': 'Search ...',
  'pages.location.create': 'Create New',
  'pages.locations.name': 'Name',
  'pages.locations.description': 'Description',
  'pages.locations.streetAddress': 'Street Address',
  'pages.locations.province': 'Province/City',
  'pages.locations.district': 'District',
  'pages.locations.ward': 'Ward',
  'pages.locations.note': 'Description',
  'pages.locations.actions': 'Actions',
  'pages.locations.create.title' : 'Create New'
}
