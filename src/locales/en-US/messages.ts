export default {
  'api.login.success': 'Login success',
  'api.success': 'Success',
  'api.login.error': 'Login fail',
  'api.error': 'Fail',
  'api.loading': 'Requesting ...',
};
