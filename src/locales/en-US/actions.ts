export default {
  'btn.create': 'Create New',
  'btn.update': 'Update',
  'btn.view': 'View',
  'input.search': 'Search ...',
  'actions.logout': 'Logout',
  'actions.create': 'Create New',
  'actions.update': 'Update',
  'actions.title': 'Actions'
};
