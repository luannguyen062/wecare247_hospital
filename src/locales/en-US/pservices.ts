export default {
  'pages.services.id': 'Service Code',
  'pages.services.name': 'Service Name',
  'pages.services.description': 'Description',
  'pages.services.hours': 'Hours',
  'pages.services.price': 'Price',
  'pages.services.priceNegotiation': 'Custom Price',
};
