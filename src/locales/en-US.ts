import component from './en-US/component';
import globalHeader from './en-US/globalHeader';
import menu from './en-US/menu';
import pwa from './en-US/pwa';
import settingDrawer from './en-US/settingDrawer';
import settings from './en-US/settings';
import pages from './en-US/pages';
import pcarers from './en-US/pcarers';
import plocations from './en-US/plocations';
import messages from './en-US/messages';
import errors from './en-US/errors';
import actions from './en-US/actions';
import pcustomers from './en-US/pcustomers';
import pcontracts from './en-US/pcontracts';
import ppayments from './en-US/ppayments';
import pschedules from './en-US/pschedules';
import pservices from './en-US/pservices';
import genders from './en-US/genders';

export default {
  'navBar.lang': 'Ngôn ngữ',
  'layout.user.link.help': 'Trợ giúp',
  'layout.user.link.privacy': 'Riêng tư',
  'layout.user.link.terms': 'Terms',
  'app.preview.down.block': 'Download this page to your local project',
  'app.welcome.link.fetch-blocks': 'Get all block',
  'app.welcome.link.block-list': 'Quickly build standard, pages based on `block` development',
  ...globalHeader,
  ...menu,
  ...settingDrawer,
  ...settings,
  ...pwa,
  ...component,
  ...pages,
  ...messages,
  ...pcarers,
  ...errors,
  ...plocations,
  ...actions,
  ...pcustomers,
  ...pcontracts,
  ...ppayments,
  ...pschedules,
  ...pservices,
  ...genders
};
