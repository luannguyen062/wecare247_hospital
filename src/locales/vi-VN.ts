import component from './vi-VN/component';
import globalHeader from './vi-VN/globalHeader';
import menu from './vi-VN/menu';
import pwa from './vi-VN/pwa';
import settingDrawer from './vi-VN/settingDrawer';
import settings from './vi-VN/settings';
import pages from './vi-VN/pages';
import pcarers from './vi-VN/pcarers';
import plocations from './vi-VN/plocations';
import messages from './vi-VN/messages';
import errors from './vi-VN/errors';
import actions from './vi-VN/actions';
import pcustomers from './vi-VN/pcustomers';
import pcontracts from './vi-VN/pcontracts';
import ppayments from './vi-VN/ppayments';
import pschedules from './vi-VN/pschedules';
import pservices from './vi-VN/pservices';
import genders from './vi-VN/genders';

export default {
  'navBar.lang': 'Ngôn ngữ',
  'layout.user.link.help': 'Trợ giúp',
  'layout.user.link.privacy': 'Riêng tư',
  'layout.user.link.terms': 'Terms',
  'app.preview.down.block': 'Download this page to your local project',
  'app.welcome.link.fetch-blocks': 'Get all block',
  'app.welcome.link.block-list': 'Quickly build standard, pages based on `block` development',
  ...globalHeader,
  ...menu,
  ...settingDrawer,
  ...settings,
  ...pwa,
  ...component,
  ...pages,
  ...messages,
  ...pcarers,
  ...errors,
  ...plocations,
  ...actions,
  ...pcustomers,
  ...pcontracts,
  ...ppayments,
  ...pschedules,
  ...pservices,
  ...genders
};
