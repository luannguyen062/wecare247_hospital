export default {
  'pages.contracts.id': 'Mã HĐ',
  'pages.contracts.customerName': 'Khách Hàng',
  'pages.contracts.statusValue': 'Trạng Thái',
  'pages.contracts.totalAmount': 'Giá',
  'pages.contracts.effectFrom': 'Từ Ngày',
  'pages.contracts.effectTo': 'Đến Ngày',
  'pages.contracts.paidAmount': 'Thanh Toán',
  'pages.contracts.remainingAmount': 'Còn Lại',
  'pages.contracts.signingDate': 'Ngày Ký',
  'pages.contracts.serviceName': 'Dịch Vụ',
  'pages.contracts.serviceFrom': 'Từ Ngày',
  'pages.contracts.serviceTo': 'Đến Ngày',
  'pages.contracts.serviceCarer': 'Người Thực Hiện',
  'pages.contracts.serviceHours': 'Tổng Giờ',
  'pages.contracts.servicePrice': 'Giá',
  'pages.contracts.serviceStatus': 'Trạng Thái',
  'pages.contracts.actions': 'Thao Tác',
  'pages.contracts.title': 'Thông Tin Chi Tiết',
  'pages.contracts.signingBy': 'Người Ký',
  'pages.contracts.totalHours': 'Tổng Giờ',
  'pages.contracts.status': 'Trạng Thái',
  'pages.services.title': 'Danh Sách Dịch Vụ',
  'pages.contracts.serviceUpdate': 'Cập Nhật Dịch Vụ',
  'pages.contracts.createService': 'Thêm Dịch Vụ',
  'pages.contracts.serviceId': 'Dịch Vụ',
  'pages.contracts.serviceFromTo': 'Từ Ngày - Đến Ngày',
  'pages.contracts.totalServices': 'Số Lượng Dịch Vụ',
  'pages.contracts.status.new': 'Khởi Tạo',
  'pages.contracts.status.active': 'Đang Hiệu Lực',
  'pages.contracts.status.cancelled': 'Đã Hủy',
  'pages.contracts.status.completed': 'Hoàn Thành',
  'pages.contract.info': 'Thông Tin Hợp Đồng',
  'pages.contract.actions.complete.confirm': 'Xác nhận đóng HĐDV?',
  'pages.contract.actions.cancel.confirm': 'Xác nhận hủy HĐDV?',
  'pages.contract.actions.complete': 'Đóng',
  'pages.contract.actions.cancel': 'Hủy',
  'pages.contracts.create.contractLocationId': 'Nơi Ký HĐDV',
  'pages.contracts.create.servicePrice': 'Giá Dịch Vụ',
  'pages.contracts.create.fromTo': 'Từ Ngày - Đến Ngày',
  'pages.contracts.patientCarer': 'Người Thực Hiện',
  'pages.contracts.serviceRating': 'Đánh Giá',
  'pages.contracts.serviceRating.NotYet': 'Chưa Đánh Giá',
  'pages.contracts.sources.create': 'Thêm Mới',
  'pages.contracts.sources.name': 'Hiển Thị',
  'pages.contracts.sources.description': 'Mô Tả',
  'pages.contracts.create.customer.secondaryPhone': 'Số Bổ Sung',
  'pages.contracts.patient.title': 'Thông Tin Bệnh Nhân',
  'pages.contracts.create.patient.fullName': 'Tên Bệnh Nhân',
  'pages.contracts.create.patient.patientYearOfBirth': 'Năm Sinh',
  'pages.contracts.create.patient.weight': 'Cân Nặng',
  'pages.contracts.create.locationDepartmentId': 'Nơi Thực Hiện',
  'pages.contracts.create.contractSourceId': 'Nguồn Hợp Đồng'
}
