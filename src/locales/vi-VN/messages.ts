export default {
  'api.login.success': 'Đăng nhập thành công',
  'api.success': 'Thao tác thành công',
  'api.login.error': 'Đăng nhập không thành công',
  'api.error': 'Thao tác không thành công',
  'api.loading': 'Đang tải ...',
};
