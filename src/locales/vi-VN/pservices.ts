export default {
  'pages.services.id': 'Mã Dịch Vụ',
  'pages.services.name': 'Tên',
  'pages.services.description': 'Mô Tả',
  'pages.services.hours': 'Số Giờ',
  'pages.services.price': 'Giá Dịch Vụ',
  'pages.services.priceNegotiation': 'Dịch Vụ Thỏa Thuận',
};
