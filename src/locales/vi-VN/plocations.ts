export default {
  'pages.locations.id': 'Mã BV',
  'pages.locations.search': 'Tìm Kiếm',
  'pages.location.create': 'Tạo Nguồn HĐ',
  'pages.locations.name': 'Bệnh Viện',
  'pages.locations.description': 'Tìm Kiếm',
  'pages.locations.streetAddress': 'Địa Chỉ',
  'pages.locations.province': 'Tỉnh/Thành Phố',
  'pages.locations.district': 'Quận/Huyện',
  'pages.locations.ward': 'Phường/Xã',
  'pages.locations.note': 'Mô Tả',
  'pages.locations.actions': 'Thao Tác',
  'pages.locations.create.title' : 'Tạo Nguồn HĐ'
}
