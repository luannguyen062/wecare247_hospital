export default {
  'btn.create': 'Thêm Mới',
  'btn.update': 'Cập Nhật',
  'btn.view': 'Xem',
  'input.search': 'Tìm kiếm',
  'actions.logout': 'Thoát',
  'actions.create': 'Thêm Mới',
  'actions.update': 'Cập Nhật',
  'actions.title': 'Thao Tác'
};
