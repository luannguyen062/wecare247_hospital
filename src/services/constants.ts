export const statusOptions = [
  {
    value: 10,
    label: 'Khởi Tạo',
  },
  {
    value: 20,
    label: 'Đang Thực Hiện',
  },
  {
    value: 100,
    label: 'Hoàn Thành',
  },
  {
    value: 99,
    label: 'Đã Hủy',
  },
  {
    value: 0,
    label: 'Chờ Xác Nhận',
  },
];

export const statusEnum = {
  10: {
    text: 'Khởi Tạo',
    status: '10',
  },
  20: {
    text: 'Đang Thực Hiện',
    status: '20',
  },
  100: {
    text: 'Hoàn Thành',
    status: '100',
  },
  99: {
    text: 'Đã Hủy',
    status: '99',
  },
  0: {
    text: 'Chờ Xác Nhận',
    status: '0',
    disabled: true,
  },
};

export const statusMap = {
  10: 'Khởi Tạo',
  20: 'Đang Thực Hiện',
  100: 'Hoàn Thành',
  99: 'Đã Hủy',
  0: 'Chờ Xác Nhận',
};

export const paymentTypeMap = {
  10: 'Thanh Toán HĐDV',
  20: 'Hoàn Tiền Khách Hàng',
  30: 'Chi Cho NNB',
  100: 'Chi Hoa Hồng',
};
