import jwt_decode from 'jwt-decode';

export function getCurrentUser(): any | undefined {
  const accessToken = localStorage.getItem('logged-user');

  if (accessToken) {
    const userInfo = jwt_decode<any>(accessToken);

    return {
      ...userInfo,
    };
  }

  return undefined;
}
