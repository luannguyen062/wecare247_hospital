declare namespace API {
  export interface LoginStateType {
    status?: 'ok' | 'error';
    type?: string;
  }

  export interface QueryParams {
    page: number;
    size: number;
    search?: string;
    orderBy?: string;
    desc?: boolean;
  }

  export interface PaginationData<T> {
    currentPage: number;
    hasNext: boolean;
    hasPrevious: boolean;
    items: T[];
    pages: number;
    size: number;
    total: number;
  }

  export interface PaginationInfo {
    current: number;
    pageSize: number;
    total: number;
    defaultPageSize: number;
  }

  export interface CreateResponse {
    success: boolean;
    error: any;
    data: string;
  }

  export interface UpdateResponse {
    success: boolean;
    error: any;
    data: string;
  }
}
