import { apiConfig } from '@/configs/apis';
import request from '../utils/request';

export interface Ward {
  id: string;
  name: string;
  provinceId: string;
  districtId: string;
}

export interface WardResponse {
  success: boolean;
  errors: any;
  data: Ward[];
}

export async function getWards(params: any) {
  return request<WardResponse>(`${apiConfig.prefixUrl}/api/wards`, {
    params,
  });
}
