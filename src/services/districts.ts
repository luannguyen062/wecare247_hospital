import { apiConfig } from '@/configs/apis';
import request from '../utils/request';

export interface District {
  id: string;
  name: string;
  provinceId: string;
}

export interface DistrictResponse {
  success: boolean;
  errors: any;
  data: District[];
}

export async function getDistricts(params: any) {
  return request<DistrictResponse>(`${apiConfig.prefixUrl}/api/districts`, {
    params
  });
}
