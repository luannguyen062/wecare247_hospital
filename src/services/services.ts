import { apiConfig } from '@/configs/apis';
import request from '../utils/request';

export interface ServiceModel {
  id: string;
  name: string;
  description: string;
  price: number;
  pricePerHour: number;
}

export interface ServiceResponse {
  success: boolean;
  errors: any;
  data: ServiceModel[];
}

export async function getServices() {
  return request<ServiceResponse>(`${apiConfig.prefixUrl}/api/services`);
}

export async function getServiceSelectList() {
  return request(`${apiConfig.prefixUrl}/api/select-list/services`);
}
