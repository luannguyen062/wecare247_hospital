import { apiConfig } from '@/configs/apis';
import request from '../utils/request';

export interface Level {
  id: string;
  name: string;
}

export interface LevelResponse {
  success: boolean;
  errors: any;
  data: Level[];
}

export async function getLevels() {
  return request<LevelResponse>(`${apiConfig.prefixUrl}/api/patient-carer-levels`);
}
