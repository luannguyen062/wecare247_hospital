import request from '@/utils/request';
import { apiConfig } from '@/configs/apis';

export interface LoginParamsType {
  username: string;
  password: string;
  mobile: string;
  captcha: string;
  type: string;
}

export interface LoginParams {
  username: string;
  password: string;
}

export interface TokenResponse {
  accessToken: string;
  refreshToken: string;
  expiredIn: number;
}

export interface LoginResponse {
  errors?: any;
  data?: TokenResponse;
  success?: boolean;
}

export async function login(params: LoginParams) {
  return request<LoginResponse>(`${apiConfig.prefixUrl}/api/oauth/token`, {
    method: 'POST',
    data: params,
  });
}
