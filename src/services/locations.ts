import { apiConfig } from '@/configs/apis';
import request from '../utils/request';

export interface Location {
  id: string;
  name: string;
}

export interface LocationResponse {
  success: boolean;
  errors: any;
  data: Location[];
}

export async function getLocations() {
  return request<LocationResponse>(`${apiConfig.prefixUrl}/api/locations`);
}
