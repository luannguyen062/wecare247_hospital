import { apiConfig } from '@/configs/apis';
import request from '../utils/request';

export interface Province {
  id: string;
  name: string;
}

export interface ProvinceResponse {
  success: boolean;
  errors: any;
  data: Province[];
}

export async function getProvinces() {
  return request<ProvinceResponse>(`${apiConfig.prefixUrl}/api/provinces`);
}
