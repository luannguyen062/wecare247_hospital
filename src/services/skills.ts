import { apiConfig } from '@/configs/apis';
import request from '../utils/request';

export interface RequireSkill {
  id: string;
  name: string;
  description: string;
  groupId: string;
}

export interface RequireSkillResponse {
  success: boolean;
  errors: any;
  data: RequireSkill[];
}

export async function getRequireSkills() {
  return request<RequireSkillResponse>(`${apiConfig.prefixUrl}/api/require-skills`);
}
