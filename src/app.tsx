import React from 'react';
import { Settings as LayoutSettings, PageLoading } from '@ant-design/pro-layout';
// import { notification } from 'antd';
import { history, RunTimeLayoutConfig } from 'umi';
import RightContent from '@/components/RightContent';
import Footer from '@/components/Footer';
// import { ResponseError } from 'umi-request';
import { getCurrentUser } from './services/user';
import defaultSettings from '../config/defaultSettings';
import './app.css';

export const initialStateConfig = {
  loading: <PageLoading />,
};

export async function getInitialState(): Promise<{
  settings?: LayoutSettings;
  currentUser?: any;
  fetchUserInfo?: () => Promise<any | undefined>;
}> {
  const fetchUserInfo = () => {
    try {
      const currentUser = getCurrentUser();
      return currentUser;
    } catch (error) {
      history.push('/user/login');
    }
    return undefined;
  };

  if (history.location.pathname !== '/user/login') {
    const currentUser = fetchUserInfo();
    return {
      fetchUserInfo,
      currentUser,
      settings: defaultSettings,
    };
  }
  return {
    fetchUserInfo,
    settings: defaultSettings,
  };
}

export const layout: RunTimeLayoutConfig = ({ initialState }) => {
  return {
    rightContentRender: () => <RightContent />,
    disableContentMargin: false,
    footerRender: () => <Footer />,
    onPageChange: () => {
      const { location } = history;
      if (!initialState?.currentUser && location.pathname !== '/user/login') {
        history.push('/user/login');
      }
    },
    menuHeaderRender: undefined,
    ...initialState?.settings,
  };
};
