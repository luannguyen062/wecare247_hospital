import { MinusCircleOutlined, PlusOutlined } from '@ant-design/icons';
import { AutoComplete, Button, Form, Input, message, Space } from 'antd';
import React, { useEffect } from 'react';
import { useIntl } from 'umi';

type MultiRowFormProps = {
  onFinish?: any;
  addMoreRowTitle: string;
  contracts: any;
  visible: boolean;
  actionRef?: any;
  closeForm?: any;
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const MultiRowForm: React.FC<MultiRowFormProps> = (props: any) => {
  const [form] = Form.useForm();
  const intl = useIntl();
  const { contracts, addMoreRowTitle, visible, onFinish, actionRef, closeForm } = props;
  // const [hasData, setHasData] = useState(false);

  const reset = () => {
    form.setFieldsValue({ payments: [] });
  };

  useEffect(() => {
    form.setFieldsValue({ payments: [] });
  }, [visible]);

  return (
    <Form
      form={form}
      name="dynamic_form_nest_item"
      onFinish={async (values: any) => {
        const response = await onFinish(values);
        if (response.success) {
          if (closeForm) {
            closeForm();
          }
          if (actionRef.current) {
            actionRef.current.reload();
          }
          message.success(
            intl.formatMessage({
              id: 'api.create.success',
              defaultMessage: 'Success',
            }),
          );
        }
      }}
      autoComplete="off"
    >
      <Form.List
        name="payments"
        rules={[
          {
            validator: async (_, payments) => {
              if (!payments || payments.length < 1) {
                // setHasData(true);
                return Promise.reject(new Error('At least 1 payment'));
              }

              return Promise.resolve(true);
            },
          },
        ]}
      >
        {(fields, { add, remove }) => (
          <>
            {fields.map((field) => (
              <Space key={field.key} align="baseline">
                <Form.Item
                  {...field}
                  label={intl.formatMessage({
                    id: 'pages.payments.contractCode',
                    defaultMessage: 'Contract Code',
                  })}
                  name={[field.name, 'contractCode']}
                  fieldKey={[field.fieldKey, 'contractCode']}
                  rules={[
                    {
                      required: true,
                      message: intl.formatMessage({
                        id: 'errors.required',
                        defaultMessage: 'This field is required',
                      }),
                    },
                  ]}
                >
                  <AutoComplete
                    style={{ width: 200 }}
                    options={contracts}
                    filterOption={(inputValue, option) =>
                      option!.value.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1
                    }
                  />
                </Form.Item>
                <Form.Item
                  {...field}
                  label={intl.formatMessage({
                    id: 'pages.payments.amount',
                    defaultMessage: 'Amount',
                  })}
                  name={[field.name, 'amount']}
                  fieldKey={[field.fieldKey, 'amount']}
                  rules={[
                    {
                      required: true,
                      message: intl.formatMessage({
                        id: 'errors.required',
                        defaultMessage: 'This field is required',
                      }),
                    },
                    {
                      pattern: new RegExp('^[0-9]*$'),
                      message: intl.formatMessage({
                        id: 'errors.number',
                        defaultMessage: 'This is not a valid number',
                      }),
                    },
                  ]}
                >
                  <Input style={{ width: 200 }} />
                </Form.Item>
                <Form.Item
                  {...field}
                  label={intl.formatMessage({
                    id: 'pages.payments.note',
                    defaultMessage: 'Payment note',
                  })}
                  name={[field.name, 'note']}
                  fieldKey={[field.fieldKey, 'note']}
                  rules={[
                    {
                      required: true,
                      message: intl.formatMessage({
                        id: 'errors.required',
                        defaultMessage: 'This field is required',
                      }),
                    },
                  ]}
                >
                  <Input style={{ width: 200 }} />
                </Form.Item>
                <MinusCircleOutlined onClick={() => remove(field.name)} />
              </Space>
            ))}

            <Form.Item>
              <Button type="dashed" onClick={() => add()} block icon={<PlusOutlined />}>
                {addMoreRowTitle}
              </Button>
            </Form.Item>
          </>
        )}
      </Form.List>
      <Form.Item>
        <div style={{ float: 'right' }}>
          <Button type="default" onClick={reset} style={{ marginRight: 10 }}>
            {intl.formatMessage({
              id: 'pages.buttons.reset',
              defaultMessage: 'Reset',
            })}
          </Button>
          <Button type="primary" htmlType="submit">
            {intl.formatMessage({
              id: 'pages.buttons.submit',
              defaultMessage: 'Submit',
            })}
          </Button>
        </div>
      </Form.Item>
    </Form>
  );
};

export default MultiRowForm;
