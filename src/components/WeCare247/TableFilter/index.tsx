import { LightFilter } from '@ant-design/pro-form';
import React from 'react';

type TableFilterProps = {
  onFinish: any;
  initialValues: any;
};

const TableFilter: React.FC<TableFilterProps> = (props) => {
  return (
    <LightFilter onFinish={props.onFinish} initialValues={props.initialValues}>
      {props.children}
    </LightFilter>
  );
};

export default TableFilter;
