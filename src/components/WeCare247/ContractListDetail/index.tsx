import { HeartFilled } from '@ant-design/icons';
import ProCard from '@ant-design/pro-card';
import ProTable, { ProColumns } from '@ant-design/pro-table';
import { Rate, Space } from 'antd';
import React from 'react';
import { isMobile } from 'react-device-detect';
import { useIntl } from 'umi';

export interface ContractListDetailProps {
  services?: any | [];
  contracts?: any | [];
  headerTitle?: string;
  dateFormatter?: 'string' | 'number' | false;
  displayContractSummary?: boolean;
  serviceColumns: ProColumns<any>[];
  contractColumns: ProColumns<any>[];
}

const ContractListDetail: React.FC<ContractListDetailProps> = (props) => {
  const intl = useIntl();
  const { services, contracts, serviceColumns, contractColumns, displayContractSummary } = props;

  const expandedRowRender = (record: any) => {
    const contractServices = services.filter((item: any) => item.contractId === record.id);
    return (
      <ProTable
        rowKey="id"
        columns={serviceColumns}
        headerTitle={false}
        search={false}
        options={false}
        pagination={false}
        request={() => {
          return Promise.resolve({
            data: contractServices,
            success: true,
          });
        }}
      />
    );
  };

  const average = (array: any, key: string) => {
    const sumValue = array.reduce((a: any, b: any) => a + (b[key] || 0), 0);
    if (sumValue > 0) {
      return sumValue / array.length;
    }

    return sumValue;
  };

  const countServices = (status?: number[]) => {
    if (!status || status.length === 0) {
      return services.length;
    }

    const filteredServices = services.filter((item: any) => status.indexOf(item.status) > -1);
    return filteredServices.length;
  };

  const averageServiceRating = () => {
    const filteredServices = services.filter((item: any) => item.rating);
    return average(filteredServices, 'rating');
  };

  return (
    <>
      {displayContractSummary && (
        <ProCard>
          <ProCard
            layout="center"
            colSpan={{
              xs: '100%',
              sm: '50%',
              md: '30%',
              lg: '20%',
              xl: '20%',
            }}
            title={intl.formatMessage({
              id: 'pages.carers.contracts.total',
              defaultMessage: 'Total Contracts',
            })}
          >
            {contracts.length}
          </ProCard>
          <ProCard
            colSpan={{
              xs: '100%',
              sm: '50%',
              md: '30%',
              lg: '20%',
              xl: '20%',
            }}
            title={intl.formatMessage({
              id: 'pages.carers.contracts.totalServices',
              defaultMessage: 'Total Services',
            })}
          >
            {countServices()}
          </ProCard>
          <ProCard
            colSpan={{
              xs: '100%',
              sm: '50%',
              md: '30%',
              lg: '20%',
              xl: '20%',
            }}
            title={intl.formatMessage({
              id: 'pages.carers.contracts.completedServices',
              defaultMessage: 'Completed Services',
            })}
          >
            {countServices([100])}
          </ProCard>
          <ProCard
            colSpan={{
              xs: '100%',
              sm: '50%',
              md: '30%',
              lg: '20%',
              xl: '20%',
            }}
            title={intl.formatMessage({
              id: 'pages.carers.contracts.activeServices',
              defaultMessage: 'Active Services',
            })}
          >
            {countServices([10, 20])}
          </ProCard>
          <ProCard
            colSpan={{
              xs: '100%',
              sm: '50%',
              md: '30%',
              lg: '20%',
              xl: '20%',
            }}
            title={intl.formatMessage({
              id: 'pages.carers.contracts.averageRating',
              defaultMessage: 'Average Rating',
            })}
          >
            <Rate
              disabled
              character={<HeartFilled />}
              allowHalf
              defaultValue={averageServiceRating()}
            />
          </ProCard>
        </ProCard>
      )}
      <Space style={{ marginBottom: 16 }} />
      <ProTable<any>
        columns={contractColumns}
        request={() => {
          return Promise.resolve({
            data: contracts,
            success: true,
          });
        }}
        rowKey="id"
        pagination={{
          showQuickJumper: true,
        }}
        expandable={{ expandedRowRender }}
        search={false}
        dateFormatter={props.dateFormatter}
        headerTitle={props.headerTitle}
        options={false}
        style={isMobile ? { paddingLeft: 0, paddingTop: 0 } : undefined}
      />
    </>
  );
};

export default ContractListDetail;
