import React from 'react';
import ProTable, { ProColumns } from '@ant-design/pro-table';
import { useIntl } from 'umi';
import { Col, Row, Statistic } from 'antd';
import NumberFormat from 'react-number-format';

export interface SimpleTableProps {
  data: any;
  valueSuffix?: string;
  title: string;
}

const SimpleTable: React.FC<SimpleTableProps> = (props) => {
  const intl = useIntl();
  const { data, valueSuffix, title } = props;
  const columns: ProColumns<any>[] = [
    {
      title: intl.formatMessage({
        id: 'pages.simpleTable.text',
        defaultMessage: 'Text',
      }),
      sorter: (a, b) => a.text.length - b.text.length,
      dataIndex: 'text',
    },
    {
      title: intl.formatMessage({
        id: 'pages.simpleTable.value',
        defaultMessage: 'Value',
      }),
      sorter: (a, b) => a.value - b.value,
      dataIndex: 'value',
      render: (_, record) => <NumberFormat value={record.value} displayType="text" thousandSeparator="," suffix={` ${valueSuffix}`} />,
    },
  ];

  let total = 0;
  data.forEach((item: any) => {
    total += item.value;
  });

  return (
    <>
      <h3>{title}</h3>
      <Row gutter={16}>
        <Col span={8}>
          <Statistic
            title={intl.formatMessage({
              id: 'pages.simpleTable.total',
              defaultMessage: 'Total',
            })}
            value={total}
          />
        </Col>
      </Row>
      <ProTable<any>
        style={{ marginTop: 25 }}
        columns={columns}
        dataSource={data}
        request={(params, sorter, filter) => {
          // eslint-disable-next-line no-console
          console.log(params, sorter, filter);
          return Promise.resolve({
            data,
            success: true,
          });
        }}
        rowKey="text"
        pagination={{
          showQuickJumper: true,
          pageSize: 5,
        }}
        search={false}
        options={false}
      />
    </>
  );
};

export default SimpleTable;
