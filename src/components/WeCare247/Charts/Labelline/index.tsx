import React from 'react';
import { Chart, Tooltip, Axis, Coordinate, Interval } from 'bizcharts';

export interface LabelLineProps {
  data: any;
  showTooltipTitle?: boolean;
  style?: any;
  labelContentFormat?: any;
  title: string;
}

const LabelLine: React.FC<LabelLineProps> = (props) => {
  const cols = {
    percent: {
      formatter: (val: any) => {
        return `${val * 100}%`;
      },
    },
  };

  const { data, showTooltipTitle, style, labelContentFormat, title } = props;
  return (
    <>
      <h3 style={{ textAlign: 'center' }}>{title}</h3>
      <Chart height={400} data={data} scale={cols} autoFit>
        <Coordinate type="theta" radius={0.75} />
        <Tooltip showTitle={showTooltipTitle || false} />
        <Axis visible={false} />
        <Interval
          position="extra"
          adjust="stack"
          color="text"
          style={
            style || {
              lineWidth: 1,
              stroke: '#fff',
            }
          }
          label={[
            '*',
            {
              content: (labelData: any) => {
                if (labelContentFormat) {
                  return labelContentFormat(labelData);
                }

                return `${labelData.text}: ${labelData.extra * 100}%`;
              },
            },
          ]}
        />
      </Chart>
    </>
  );
};

export default LabelLine;
