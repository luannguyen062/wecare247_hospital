import React from 'react';
// import { GithubOutlined } from '@ant-design/icons';
import { DefaultFooter } from '@ant-design/pro-layout';

export default () => (
  <DefaultFooter
    copyright="2020 WeCare247"
    links={[
      {
        key: 'CRM WeCare247',
        title: 'CRM WeCare247',
        href: 'https://nguoinuoibenh.com',
        blankTarget: true,
      },
    ]}
  />
);
