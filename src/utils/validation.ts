import { message } from 'antd';

export const renderFormValidations = (form: any, errors: any, values: any): void => {
  if (errors.length === 1 && errors[0]._) {
    message.error({
      message: `${errors[0]._}`,
    });

    return;
  }

  for (let i = 0; i < errors.length; i + 1) {
    Object.keys(
      errors[i].map((key: any) => {
        return form.setFields({
          key: {
            value: values[key],
            errors: [new Error(errors[i][key])],
          },
        });
      }),
    );
  }
};
