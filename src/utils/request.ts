import { extend } from 'umi-request';
import { notification } from 'antd';
import { history } from 'umi';
import { readData } from './utils';

const codeMessage = {
  400: 'Dữ liệu có sai sót',
  401: 'Vui lòng đăng nhập để thực hiện thao tác',
  403: 'Không có quyền truy cấp',
  404: 'Không tìm thấy',
};

const errorHandler = (error: { response: Response }): Response => {
  const { response } = error;
  // eslint-disable-next-line no-console
  console.log(response, 'Error response');

  if (response && response.status) {
    const errorText = codeMessage[response.status] || response.statusText;
    const { status, url } = response;
    if (status === 401) {
      history.push('/user/login');
      return response;
    }

    if (status === 403) {
      history.push('/403');
      return response;
    }

    if (status === 404) {
      history.push('/404');
      return response;
    }

    if (status === 500) {
      history.push('/500');
      return response;
    }

    notification.error({
      message: `${status}: ${url}`,
      description: errorText,
    });
  }

  return response;
};

const request = extend({
  errorHandler,
  credentials: 'include',
});

request.interceptors.request.use(async (url, options) => {
  const token = await readData('logged-user', null, null);
  if (token) {
    const headers = {
      authorization: `Bearer ${token}`,
    };

    return {
      url,
      options: { ...options, headers },
    };
  }

  return {
    url,
    options: { ...options },
  };
});

export default request;
