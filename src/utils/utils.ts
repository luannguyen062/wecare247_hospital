import AsyncLocalStorage from '@createnextapp/async-local-storage';

const reg = /(((^https?:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+(?::\d+)?|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)$/;

export const isUrl = (path: string): boolean => reg.test(path);

export const isAntDesignPro = (): boolean => {
  if (ANT_DESIGN_PRO_ONLY_DO_NOT_USE_IN_YOUR_PRODUCTION === 'site') {
    return true;
  }
  return window.location.hostname === 'preview.pro.ant.design';
};

export const isAntDesignProOrDev = (): boolean => {
  const { NODE_ENV } = process.env;
  if (NODE_ENV === 'development') {
    return true;
  }
  return isAntDesignPro();
};

export const getToken = () => {
  return localStorage.getItem('logged-user');
};

export const mapToSelectList = (list: any) =>
  list.map((e: any) => ({ label: e.name, value: e.id }));

export const storeData = async (key: string, data: any, callback: any, errorHandle: any) => {
  try {
    await AsyncLocalStorage.setItem(key, data, callback);
  } catch (e) {
    if (errorHandle) {
      errorHandle();
    }
  }
};

export const readData = async (key: string, callback: any, errorHandle: any) => {
  let data;

  try {
    data = await AsyncLocalStorage.getItem(key, callback);
  } catch (e) {
    if (errorHandle) {
      errorHandle();
    }
  }

  return data;
};

export const cleanData = async (key: string, callback: any, errorHandle: any) => {
  try {
    await AsyncLocalStorage.removeItem(key);
    if (callback) {
      callback();
    }
  } catch (e) {
    if (errorHandle) {
      errorHandle();
    }
  }
};
