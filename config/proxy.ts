export default {
  dev: {
    '/api/': {
      target: 'https://apps.nguoinuoibenh.com',
      changeOrigin: true,
      pathRewrite: { '^': '' },
    },
  },
  test: {
    '/api/': {
      target: 'https://preview.pro.ant.design',
      changeOrigin: true,
      pathRewrite: { '^': '' },
    },
  },
  pre: {
    '/api/': {
      target: 'https://apps.nguoinuoibenh.com',
      changeOrigin: true,
      pathRewrite: { '^': '' },
    },
  },
};
